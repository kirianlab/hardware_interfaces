#!/bin/bash

# Example of how to start the Shimadzu HPLC pump interface.
# Right now, we use the vendor's interface, but we hope to write a python interface based on the example scripts from EuXFEL.
#
# FIXME: Update with native python interface.

iexplore="C:\Program Files\Internet Explorer\iexplore.exe"
start iexplore "192.168.200.99"
