#!/bin/bash


# date="20210721"
# nozzle_id="test"

# # from xfel1 to analysis laptop
# SOURCEDIR="/data/microjets/2021/./$date/$nozzle_id"
# DESTDIR="/home/kkarpos/rawdata_temp/microjets"

# while [ true ]; do
#     rsync -avzh --stats --progress raklab@10.206.48.2:$SOURCEDIR $DESTDIR
#     echo "No new files, waiting 10 seconds"
#     sleep 10
# done


# from testing station to xfel1
# SOURCEDIR="C:\\Users\\raklab\\Desktop\\NanoscribeNozzles\\2021\\.\\$date\\$nozzle_id"
SOURCEDIR="/c/Users/raklab/Desktop/NanoscribeNozzles/2021/./20210902"
SOURCEDIR="/c/Users/raklab/Desktop/Nanoscribe-Nozzles/2021/./20210902"
DESTDIR="/data/rawdata/microjets/2021"

while [ true ]; do
    rsync -avzh --stats --progress $SOURCEDIR raklab@10.206.48.2:$DESTDIR
    echo "No new files, waiting 3 seconds"
    sleep 3
done
