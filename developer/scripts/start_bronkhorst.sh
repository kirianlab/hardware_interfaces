#!/bin/bash

# Example of how to start the Bronkhorst sensor interface, with logging.
# Right now, this runs the sensor in Odysseus, but we will fix that soon.
#
# FIXME: Remove the Odysseus depenency of this script.

/c/Users/raklab/Miniconda3/python /d/sahba/repos/odysseus/odysseus/athena/bronkhorst/main_sensor_widget_bronkhrost.py
