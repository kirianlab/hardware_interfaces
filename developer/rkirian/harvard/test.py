import time
from hardware import harvard
pump = harvard.Pump(com_port='/dev/ttyACM0') #com_port="COM3")  # Port syntax differs from linux and Windows
pump.set_syringe_diameter(4)
pump.set_syringe_volume(1000, None)
pump.set_infuse_rate(20, None)
pump.set_target_volume(100, None)
pump.start_infuse('p1')
time.sleep(1)
# pump.stop_pump('p1')  # Manual stop.  The pump will stop when the Pump instance is destroyed.
