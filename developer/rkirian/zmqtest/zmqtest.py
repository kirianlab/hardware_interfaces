import sys
import time
import pickle
import logging
import threading
import daemon3x
import zmq
import numpy as np
import pyqtgraph as pg
from hardware.sensirion import SensirionDevice
from hardware.shimadzu import hplc

debug = True

sensirion_config = {
    'device_class': 'Sensirion',  # This is the class that should be instantiated for this device.
    'serial_number': 'FTSTT5NA',  # All devices should have some identifier that is unique to the specific device.
    'calibration': 0,  # Sensirion "calibration" for low (0) or high (1) flow rates
    'resolution': 16,  # Measurement resolution (bits)
    'polling_frequency_hz': 100,  # Specific to this type of device
    'average_n_readings': 100,  # Specific to this type of device
    'zmq_client_address': 'tcp://localhost:5555',  # Make sure the port is unique to this device.
    'zmq_server_address': 'tcp://*:5555',  # Make sure this port is matching the above.
    'zmq_daemon_pid_file': '/tmp/zmq_daemon_5555.pid'  # This is for creating a daemon server, if desired.
}

shimadzu_config = {
    'device_class': 'hplc',
    'ip_address': '192.168.200.99',
    'user': 'raklab',
    'password': 'sky2Blue',
    'polling_frequency_hz': 1,
    'buffer_length': 100000,
    'zmq_client_address': 'tcp://localhost:5556',
    'zmq_server_address': 'tcp://*:5556',
    'zmq_daemon_pid_file': '/tmp/zmq_daemon_5556.pid'
}

# for emulation only
global flow_rate_setpoint
flow_rate_setpoint = 0
global sensirion_connected
sensirion_connected = False


def dbgmsg(*args, **kwargs):
    r""" Simple debug message that can be silenced if desired. """
    if debug:
        print(*args, **kwargs)


# As a general rule, all device classes (serial, tcp, etc.) should be initialized using a single config dictionary as
# the argument.  Optionally, additional keyword arguments may be used to override the config dictionary.

class SensirionSerialConnection:
    r""" Emulate a serial connection to a flowmeter.  This should do all the dirty work of maintaining a connection to
    the device, and should aim to provide a sensible, intuitive interface. The problem with using this class directly
    to communicate with the device is that you can have only one instance.  If you have more than one process that
    needs to communicate with the device (e.g. a live monitor and a script that is making changes) then you should
    wrap this in a Server class. """

    def __init__(self, config=None):
        global sensirion_connected
        if sensirion_connected:
            raise Exception('Sensirion connection is occupied')
        sensirion_connected = True
        self.config = config
        self.serial_number = config['serial_number']
        self.device_id = 'MySensirion'
        dbgmsg('Serial: Connecting to device', self.device_id, self.serial_number)
        self.frequency_setpoint = 0.5
        self.noise_level = 0.1
        time.sleep(1)

    def get_flow_rate_reading(self):
        global flow_rate_setpoint  # The emulated HPLC pump should be setting this value
        t = time.time()
        y = flow_rate_setpoint + (np.sin(time.time() * self.frequency_setpoint) + np.random.rand()) * self.noise_level
        return t, y

    def get_all(self):
        return {'device_id': self.device_id,
                'serial_number': self.serial_number,
                'flow_rate_reading': self.get_flow_rate_reading()}

    def get_info(self):
        return {'device_id': self.device_id,
                'serial_number': self.serial_number}

    def disconnect(self):
        global sensirion_connected
        sensirion_connected = False


class ShimadzuTCPConnection:
    r""" Emulate an HPLC pump TCP connection.  This should do all the dirty work of maintaining a connection to
    the device, and should aim to provide a sensible, intuitive interface. The problem with using this class directly
    to communicate with the device is that you can have only one instance.  If you have more than one process that
    needs to communicate with the device (e.g. a live monitor and a script that is making changes) then you should
    wrap this in a Server class. """

    def __init__(self, config=None):
        self.config = config
        self.serial_number = 'V0001'
        self.device_id = 'MyHPLC'
        dbgmsg('Serial: Connecting to device', self.device_id, self.serial_number)
        self.flow_rate_setpoint = 0
        self.pump_is_on = True
        time.sleep(1)

    def set_flow_rate(self, value):
        dbgmsg('Shimadzu: setting liquid flow rate to', value)
        global flow_rate_setpoint
        flow_rate_setpoint = value
        self.flow_rate_setpoint = value

    def get_pressure_reading(self):
        return self.flow_rate_setpoint * 10 + np.random.rand()

    def get_flow_rate_setpoint(self):
        return self.flow_rate_setpoint

    def get_all(self):
        return {'device_id': self.device_id,
                'serial_number': self.serial_number,
                'config': self.config,
                'flow_rate_setpoint': self.get_flow_rate_setpoint(),
                'pressure_reading': self.get_pressure_reading(),
                'pump_is_on': self.pump_is_on}

    def get_info(self):
        return {'device_id': self.device_id,
                'serial_number': self.serial_number,
                'config': self.config}

    def disconnect(self):
        return None


# 3 levels: device (serial), server (wraps around device), clients (server communication i.e., ody, ipython, etc.), daemon (not implemented)

class GenericZMQClient:
    r""" A client that can request data from device such as flowmeter, or set parameters such as flow rate.  Multiple
    clients should be allowed, but it is presently dangerous to do that unless you have thought hard about what
    happens when a device is overloaded with multiple or conflicting requests at the same time.  This class is rather
    generic in the sense that you interact with a device class instance by sending the method name along with positional
    arguments and keyword arguments.  The server will execute the method, and return the results. """

    def __init__(self, config, start_server=False):
        r"""
        Arguments:
        """
        self.server = None
        if start_server:
            dbgmsg('Client: Attempting to start server.')
        try:
            self.server = GenericZMQServer(config=config)
            self.server.start()
        except zmq.error.ZMQError as e:
            # FIXME: Need to check if a server is already running instead of simply trying and failing...
            dbgmsg('Client: Attempted to start server but failed.', e)
        address = config['zmq_client_address']
        dbgmsg('Creating client context.')
        context = zmq.Context()
        dbgmsg('Creating client socket.')
        socket = context.socket(zmq.REQ)
        socket.setsockopt(zmq.RCVTIMEO, 2000)
        socket.connect(address)
        dbgmsg('Done.')
        self.config = config
        self.address = address
        self.context = context
        self.socket = socket
        # Check if the device server is running.  If not, attempt to start one.
        # test = self.get('get_info')
        # if test is None and start_server:
        #     dbgmsg('Client: Cannot find running server.  Starting one now.')
        #     server = GenericZMQServer(config=config)
        #     server.start()
        #     try:
        #         test = self.get('get_info')
        #         if test is None:
        #             raise Exception('Client: Could not start server.')
        #     except:
        #         raise Exception('Client: Could not start server.')
        # else:
        #     dbgmsg('GUI: Found a running server.')

    def get(self, method, args=[], kwargs={}):
        r""" Request that a method be called on the device.
        Arguments:
            method (str): The method name
            args (list): Positional arguments
            kwargs (dict): Keyword arguments
        """
        dbgmsg('Client: sending request to server.')
        try:
            self.socket.send(pickle.dumps({'method': method, 'args': args, 'kwargs': kwargs}))
            data = pickle.loads(self.socket.recv())
            dbgmsg('Client: received data from server.')
        except:
            dbgmsg('Client: cannot get data from server.  Is it running?')
            data = None
        return data


class GenericZMQServer:
    r""" A server that maintains instance of serial connection to a device via e.g. serial connection.  Only one server per
    device.  At present, you can communicate with the server by making a pickle of a dictionary that contains
    the keys 'method', 'args', and 'kwargs'.  The server then calls that method on your device instance with those
    arguments and sends back a pickle of the output.  Whatever data your device class produces must be serializable
    in order to create the pickle.  Multiple clients can communicate with the server, but there is no protection
    against race conditions, etc."""

    def __init__(self, config=None):
        r"""
        Arguments:
            config (dict): The configuration dictionary with the following keys:
        """
        self.dbgstr = 'GenericZMQServer:' + config['device_class'] + ':'
        dbgmsg('GenericZMQServer.__init__')
        address = config['zmq_server_address']
        dbgmsg(self.dbgstr, 'Creating server context.')
        context = zmq.Context()
        dbgmsg(self.dbgstr, 'Creating server socket.')
        socket = context.socket(zmq.REP)
        poller = zmq.Poller()
        poller.register(socket, zmq.POLLIN)
        # socket.RCVTIMEO = 1000
        dbgmsg(self.dbgstr, 'Connecting to address', address)
        socket.bind(address)
        dbgmsg('Server: Initialized')
        self.config = config
        self.address = address
        self.context = context
        self.socket = socket
        self.poller = poller
        # self.device is an instance of the class that communicates with the hardware
        self.device = globals()[config['device_class']](config=config)
        self.thread = None
        self.do_listen = True

    def start(self, threaded=True):
        r""" Start the server. """
        dbgmsg(self.dbgstr, 'Start.')
        # this is the where the server is waiting for a client to ask it something
        def listen():
            while self.do_listen:
                dbgmsg(self.dbgstr, 'Listening...')
                if not self.poller.poll(1000):
                    continue
                message = pickle.loads(self.socket.recv()) # if it gets here, it has received data from client
                method = message['method']
                args = message['args']
                kwargs = message['kwargs']
                dbgmsg(self.dbgstr, 'Request received:', method, args, kwargs)
                out = getattr(self.device, method)(*args, **kwargs)
                dbgmsg(self.dbgstr, 'Method called:', method, args, kwargs)
                self.socket.send(pickle.dumps(out)) # only thing that matters is that the output is pickled

        if threaded:
            if self.thread is None:
                dbgmsg(self.dbgstr, 'Starting thread.')
                self.thread = threading.Thread(target=listen)
                self.thread.start()
        else:
            listen()

    def stop(self):
        r""" Stop the server.  Kill the thread if it is threaded. """
        dbgmsg(self.dbgstr, 'Stopping', self.address)
        self.do_listen = False
        if self.thread is not None:
            dbgmsg('Server: joining thread', self.address)
            self.thread.join()
        dbgmsg(self.dbgstr, 'Closing socket', self.address)
        # self.socket.disconnect()
        self.socket.close()
        dbgmsg(self.dbgstr, 'Disconnecting device')
        self.device.disconnect()


class GenericZMQDaemon(daemon3x.DaemonABC):
    r""" A daemon that starts a server as a background process so that it persists after your program has exited.
    This might be useful if more than one client will be fetching data."""

    def __init__(self, pidfile=None, device_class=None, args=[], kwargs={}):
        r"""
        Arguments:
            pidfile (str): Path to process ID file.
            server (class): The type of server you want to initialize.
            args (list): Positional arguments passed to server upon instantiation.
            kwargs (dict): Keyword arguments passed to server upon instantiation.
        """
        super().__init__(pidfile)
        self.pidfile = pidfile
        self.server = None
        self.device_class = device_class
        self.args = args
        self.kwargs = kwargs

    def run(self):
        r""" Start the server. """
        if self.server is None:
            self.server = GenericZMQServer(device_class=self.device_class, args=self.args, kwargs=self.kwargs)
            self.server.start()


class ClientGui:
    r""" A client graphical interface for monitoring and controlling device such as a flowmeter.  It should be able to
    launch a daemon if it doesn't already exist. """

    def __init__(self):
        r"""
        Arguments:
            address (str): The IP address of the client.
            buffer_length (int): The length of the data buffer (how long the history should run)
            update_interval (float): How long to wait (in seconds) before updating the buffer.
        """

        dbgmsg('GUI: initializing.')

        self.buffer_idx = 0  # Count how many times a datapoint has been added to ring buffer
        self.buffer_length = 1000
        self.buffer_update_interval = 0.1
        self.plot_update_interval = 0.1
        self.is_initialized = False
        self.is_paused = False
        self.do_update_plot = True

        # FIXME: The ring buffer needs to be managed by the serial/TCP class to avoid multiple clients attempting
        # FIXME: to create their own data buffers.
        self.ring_buffer_y = np.zeros(self.buffer_length)
        self.ring_buffer_t = np.zeros(self.buffer_length)

        self._setup_device_interfaces()
        self._setup_interface()

    def _setup_device_interfaces(self):

        self.sensirion_client = GenericZMQClient(config=sensirion_config, start_server=True)
        self.hplc_client = GenericZMQClient(config=shimadzu_config, start_server=True)

    def _setup_interface(self):

        app = pg.mkQApp()
        app.aboutToQuit.connect(self.quit)

        main = pg.QtGui.QWidget()
        main_layout = pg.QtGui.QVBoxLayout()

        # Setup plot window
        plt_layout = pg.QtGui.QHBoxLayout() #QGridLayout() for grid layout, or QSlider() for slider window thing
        plot = pg.PlotWidget()
        # plot2 = pg.PlotWidget()
        pg.setConfigOptions(antialias=True)
        curve = plot.plot(pen=None, symbol='o', symbolPen=None, symbolBrush='g')
        # curve2 = plot.plot(pen=None, symbol='o', symbolPen=None, symbolBrush='r')
        plt_layout.addWidget(plot)
        # plt_layout.addWidget(plot2)
        main_layout.addItem(plt_layout)
        # 

        # Setup device status widget (row of widgets)
        btn_layout = pg.QtGui.QHBoxLayout()
        # Flow rate status
        btn_layout.addWidget(pg.QtGui.QLabel('Flow rate:'))
        self.flow_label = pg.QtGui.QLabel('0')
        btn_layout.addWidget(self.flow_label)
        # Setpoint status
        btn_layout.addWidget(pg.QtGui.QLabel('Flow rate setpoint:'))
        self.flow_setpoint_label = pg.QtGui.QLabel('0')
        btn_layout.addWidget(self.flow_setpoint_label)
        main_layout.addItem(btn_layout)

        # Setup control widget (row of widgets)
        ctl_layout = pg.QtGui.QHBoxLayout()
        self.set_flow_setpoint_btn = pg.QtGui.QPushButton('Set flow rate')
        self.set_flow_setpoint_btn.clicked.connect(self._set_flow_rate)
        ctl_layout.addWidget(self.set_flow_setpoint_btn)
        ctl_layout.addSpacing(20)
        self.flow_setpoint_lineedit = pg.QtGui.QLineEdit()
        self.flow_setpoint_lineedit.setText('20')
        self.flow_setpoint_lineedit.returnPressed.connect(self._set_flow_rate)
        ctl_layout.addWidget(self.flow_setpoint_lineedit)
        main_layout.addItem(ctl_layout)

        # Setup stop flow widget
        ctl_layout = pg.QtGui.QHBoxLayout()
        self.stop_flow_btn = pg.QtGui.QPushButton('STOP FLOW NOW!')
        self.stop_flow_btn.clicked.connect(self._stop_flow)
        ctl_layout.addWidget(self.stop_flow_btn)
        main_layout.addItem(ctl_layout)

        # Timers for auto-updating things
        buffer_update_timer = pg.QtCore.QTimer()
        buffer_update_timer.timeout.connect(self._update_buffer)
        plot_update_timer = pg.QtCore.QTimer()
        plot_update_timer.timeout.connect(self._update_plot)

        main.setLayout(main_layout)
        self.app = app
        self.main = main
        self.layout = main_layout
        self.buffer_update_timer = buffer_update_timer
        self.plot_update_timer = plot_update_timer
        self.curve = curve
        # self.curve2 = curve2
        self.plot = plot

    def start(self):
        r""" Start the GUI, make it visible. """
        dbgmsg('GUI: Starting application.')
        self.main.show()
        self.buffer_update_timer.start(int(self.buffer_update_interval * 1e3))
        self.plot_update_timer.start(int(self.plot_update_interval * 1e3))
        self.app.exec_()

    def _update_buffer(self):
        r""" Fetch data and update the internal memory buffer. """
        i = self.buffer_idx % self.buffer_length
        data = self.sensirion_client.get(method='get_flow_rate')
        if data is None:
            dbgmsg('GUI: Requested data is None.')
            return
        self.ring_buffer_t[i] = data[0]
        self.ring_buffer_y[i] = data[1]
        self.buffer_idx += 1
        self.flow_label.setText('%3.2f ul/min' % data[1])
        d = self.hplc_client.get(method='get_all')
        sp = d['flow'] * 1e3
        self.flow_setpoint_label.setText('%3.2f ul/min' % sp)

    def _update_plot(self):
        r""" Update the plot. """
        if self.buffer_idx < 10:
            return
        t = self.ring_buffer_t
        y = self.ring_buffer_y
        if self.buffer_idx < self.buffer_length:  # Don't show garbage data values, or zeros.
            t = t[:self.buffer_idx]
            y = y[:self.buffer_idx]
        self.curve.setData(t - np.max(t), y)
        # self.curve2.setData()...
        if not self.is_initialized:
            self.plot.enableAutoRange('x', False)  # Stop auto-scaling after the first data set is plotted.
            self.plot.setXRange(-self.buffer_length * self.buffer_update_interval, 0)
            self.is_initialized = True

    def _set_flow_rate(self):
        try:
            dbgmsg('GUI: setting flow rate...')
            flow = float(self.flow_setpoint_lineedit.text())
            dbgmsg('GUI: set flow rate to', flow)
        except:
            dbgmsg('Flow setpoint is not a float value.')
            return
        self.hplc_client.get(method='set_flow', args=[flow])
        self.hplc_client.get(method='start')

    def _stop_flow(self):
        self.hplc_client.get(method='set_flow', args=[0])
        self.hplc_client.get(method='stop')

    def quit(self):
        if self.sensirion_client.server is not None:
            self.sensirion_client.server.stop()
        if self.hplc_client.server is not None:
            self.hplc_client.get(method='stop')
            self.hplc_client.server.stop()


if __name__ == '__main__':

    # At our lowest level, we have some kind of protocol for communicating with devices.  For example, serial, or TCP.
    # We make a "device class" that handles this nasty stuff and provides a clean interface.  The interface is
    # specific to the device; for example, 'set_flow_rate'.  In this example we emulate such classes for demonstration
    # purposes.  We have a Sensirion liquid flowmeter device class (which pretends to use a serial communication) and
    # Shimadzu HPLC pump device class (which pretends to use TCP communication).
    #
    # Wherever appropriate, a device class should have a threads that continuously gather data and store it in
    # ring buffers.  Corresponding timestamps should always be maintained alongside the buffered data.  For example,
    # the sensirion flowmeter might internally gather data at 1000, but it transports data to the device class in chunks
    # of 100 samples and stores the average values in a ring buffer at 10 Hz.  These details would be configuration
    # upon creating the device class instance.  Having this capability goes along with the goal of making device classes
    # that provide clean, convenient interfaces, with the nasty stuff done internally.  In this case, the end user could
    # easily request all the flow readings recorded in the past 5 seconds.
    #
    # Often, we can only have one class that communicates directly with a device, as is the case for a serial
    # connection.  Therefore, if more than one process needs to communicate with the device, the need arises for a
    # "server class" to maintain communications with the device.  The server communicates with "client
    # class" instances.  In this example, we have created a single server class, using ZeroMQ, which provides a very
    # thin wrapper around any device class.  It is not clear that it is the best scheme; we might instead consider
    # creating a server class that is specific to each device.  In that case, the generic server class can be
    # sub-classed.
    #
    # Sometimes we want servers to run in the background on a separate process so that they are not terminated when we
    # close a terminal window.  In that case we create a "daemon".  The daemon example is not yet completed.
    #
    # At the top-most level is some entity that coordinates multiple devices (camera, flowmeters, pumps, etc.).  In
    # the example below, this is a PyQt GUI.

    if 0:
        print('\n' + ('=' * 50 + '\n') * 2)
        # Since Sensirion uses serial connection, we cannot make many instances of the sensirion serial connection class
        # because we will get an error when more than one serial connection is made on the same port.  Here we show how
        # it would look if we simply wanted to make a connection and get some data:
        sensirion_connection = SensirionSerialConnection(config=sensirion_config)
        print(sensirion_connection.get_all())
        # Confirm that our emulator will fail as expected if a second connection is attempted:
        try:
            sensirion_connection = SensirionSerialConnection(config=sensirion_config)
        except Exception as e:
            print('Exception', e)
        sensirion_connection.disconnect()
        # sys.exit()

    if 0:
        print('\n' + ('=' * 50 + '\n') * 2)
        # Direct communication with HPLC pump.  Probably multiple connections are ok since it is TCP communication.
        hplc_connection = ShimadzuTCPConnection(config=shimadzu_config)
        print(hplc_connection.get_all())
        hplc_connection.disconnect()

    if 0:
        print('\n' + ('=' * 50 + '\n') * 2)
        # Setup a ZeroMQ server for the sensirion.  This is a very thin wrapper over the serial communication class.
        sensirion_server = GenericZMQServer(config=sensirion_config)
        # The server class instance is created, but the server is not yet started.  That's what we do next.  However,
        # starting the server without threading will cause blocking.  This would be fine if you plan to start the server
        # manually in a separate terminal, for example.  But it will not work if you plan to create a client in the same
        # script.  The code below should be commented out to avoid blocking in this example.
        print('Blocked!')
        sensirion_server.start(threaded=False)
        print('Unblocked!')
        sys.exit()

    if 0:
        print('\n' + ('=' * 50 + '\n') * 2)
        sensirion_server = GenericZMQServer(config=sensirion_config)
        # Starting the server with threading will not block.  This might be good for testing so that the server output
        # is in the same place as other code.
        sensirion_server.start(threaded=True)

        # You will get an error if you try to start another server when one is already running.  Let's confirm that:
        try:
            sensirion_server = GenericZMQServer(config=sensirion_config)
        except Exception as e:
            print('Exception', e)

        # Now that a server is running, create client to talk to server:
        sensirion_client = GenericZMQClient(config=sensirion_config)
        # Now call one of the methods of the sensirion class.  The idea is that the ZMQ communication layer should be a
        # thin wrapper that lets you effectively call the class methods as usual, but indirectly.
        print(sensirion_client.get(method='get_flow_rate_reading'))

        # Check that the HPLC server/client also work in the same way:
        shimadzu_server = GenericZMQServer(config=shimadzu_config)
        shimadzu_server.start(threaded=True)
        shimadzu_client = GenericZMQClient(config=shimadzu_config)
        print(shimadzu_client.get(method='set_flow_rate', kwargs={'value': 20}))
        print(sensirion_client.get(method='get_flow_rate_reading'))
        print(shimadzu_client.get(method='set_flow_rate', kwargs={'value': 0}))
        print(sensirion_client.get(method='get_flow_rate_reading'))

        # Servers should be stopped when you are done with them.
        sensirion_server.stop()
        shimadzu_server.stop()
        # sys.exit()

    if 0:
        print('\n' + ('=' * 50 + '\n') * 2)
        # For convenience, when creating a client it can start a server automatically in the event that the server
        # does not already exist.  One problem with this is that when the client is destroyed, the server is also
        # destroyed, so any other clients will creash with it...
        sensirion_client = GenericZMQClient(config=sensirion_config, start_server=True)
        print(sensirion_client.get(method='get_flow_rate_reading'))
        # We will manually stop the server now:
        sensirion_client.server.stop()
        # sys.exit()

    # FIXME: Add an example that uses a daemon, which starts the server in the background.

    if True:
        print('\n' + ('=' * 50 + '\n') * 2)
        # Putting it all together, we make a GUI that reads flow rates with the Sensirion and allows us to set the flow
        # rates with the Shimadzu HPLC pump.
        gui = ClientGui()
        gui.start()
        # FIXME: The gui seems to block the terminal when it is closed.  Not sure of why that is.
