from hardware import bronkhorst
config = bronkhorst.make_config(dummy=True)
widget = bronkhorst.BronkhorstWidget(config=config, debug=3)
widget.start()
