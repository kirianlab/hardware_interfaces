import time
import datetime
import numpy as np
from hardware.utils.loggers import LogParser

def string_to_epoch_time(s="2023-07-12_12.50.00"):
    r"""Convert our standard time string to epoch time in seconds."""
    return datetime.datetime.strptime(s, r"%Y-%m-%d_%H.%M.%S").timestamp()

def current_time():
    # now = datetime.datetime.now().timestamp()
    now = string_to_epoch_time("2023-07-12_12.50.00")
    return now

logpath = "data"

sfa_parser = LogParser(logpath + "/single_frame_analysis/logs/*.csv")

def there_is_a_jet_right_now():
    now = current_time()
    sfa_parser.load_time_range(now-10, now)
    df = sfa_parser.df
    t = df["epoch_time"].values
    j = df["is_jet"].values
    w = np.where((t >= (now-10)) * (t <= now))
    jet_fraction = np.mean(j[w])
    return jet_fraction

t0 = time.time()
N = 1000
for i in range(N):
    print(i, 'of', N)
    if there_is_a_jet_right_now():
        print("Jet!")

print(time.time()-t0)  # Takes about 35 ms to check for jet; much faster than time it takes to measure
