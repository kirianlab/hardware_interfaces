import datetime
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import binned_statistic_dd
import joblib
from hardware.utils.loggers import LogParser

memory = joblib.Memory("joblib")

logpath = "test/logs/"
time_range_strings = [
    ["2023-07-13_09.48.10", "2023-07-13_23.13.05"],
    ["2023-07-14_09.48.10", "2023-07-14_13.40.31"],
]


def get_epoch_time(s="2023-07-12_12.50.00"):
    r"""Convert our standard time string to epoch time in seconds."""
    return datetime.datetime.strptime(s, r"%Y-%m-%d_%H.%M.%S").timestamp()


@memory.cache
def fetch_data(time_range_strings):
    sfa_parser = LogParser(logpath + "/single_frame_analysis/logs/*.csv")
    bronk_parser = LogParser(logpath + "/bronk*/data.log*")
    sens_parser = LogParser(logpath + "/sensirion_SLI-0430*/data.log*")
    parsers = [sfa_parser, bronk_parser, sens_parser]
    for p in parsers:
        for t in time_range_strings:
            p.load_time_range(get_epoch_time(t[0]), get_epoch_time(t[1]))
    epoch_time = sfa_parser.df["epoch_time"].values
    is_jet = sfa_parser.df["is_jet"].values.astype(float)
    bronk_flow = np.interp(
        epoch_time,
        bronk_parser.df["epoch_time"].values,
        bronk_parser.df["flow_rate"].values,
    )
    sens_flow = np.interp(
        epoch_time,
        sens_parser.df["epoch_time"].values,
        sens_parser.df["flow_rate"].values,
    )
    t = (epoch_time - np.min(epoch_time)) / 60 / 60
    return dict(
        liquid_flow=sens_flow,
        gas_flow=bronk_flow,
        is_jet=is_jet,
        epoch_time=epoch_time,
        relative_time_h=t,
    )


dat = fetch_data(time_range_strings)
statistic, edges, _ = binned_statistic_dd(
    dat["is_jet"],
    [dat["liquid_flow"], dat["gas_flow"]],
    statistic="mean",
    bins=[np.linspace(0, 100, 10), np.linspace(0, 100, 10)],
)
plt.figure()
plt.plot(dat["relative_time_h"], dat["is_jet"])
plt.figure()
plt.imshow(statistic)
plt.show()
