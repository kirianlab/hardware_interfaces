import re
import os
import datetime
import shutil
import logging
import logging.handlers as handlers
import time

shutil.rmtree('logs', ignore_errors=True)
os.makedirs('logs', exist_ok=True)


logger = logging.getLogger('my_app')
logger.setLevel(logging.INFO)

log_format = "%(asctime)s - %(levelname)s - %(message)s"
log_level = 10
handler = handlers.TimedRotatingFileHandler("logs/data.log", when="s", interval=1, backupCount=1e6)
handler.setLevel(log_level)
formatter = logging.Formatter(log_format)
handler.setFormatter(formatter)
handler.suffix = r"%Y-%m-%d_%H.%M.%S"

# def namer(default_name):
#     # This will be called when doing the log rotation
#     # default_name is the default filename that would be assigned, e.g. Rotate_Test.txt.YYYY-MM-DD
#     # Do any manipulations to that name here, for example this changes the name to Rotate_Test.YYYY-MM-DD.txt
#     # base_filename, ext, date = default_name.split(".")
#     return time.strftime(default_name+"_%Y%m%d-%H%M%S.log")
#
# # logger = logging.handlers.TimedRotatingFileHandler("C:\\logs\\Rotate_Test",'midnight',1)
# handler.namer = namer

# # add a suffix which you want
# handler.suffix = "%Y-%m-%d_%H_%M_%S.log"
#
# #need to change the extMatch variable to match the suffix for it
# handler.extMatch = re.compile(r"^\d{4}-\d{2}-\d{2}_-\d{2}-\d{2}-\d{2}.log$")

# finally add handler to logger
logger.addHandler(handler)

# ## Here we define our formatter
# formatter = logging.Formatter('%(asctime)s %(message)s')
#
# logHandler = handlers.TimedRotatingFileHandler('logs/normal.log', when='M', interval=1, backupCount=0)
# logHandler.setLevel(logging.INFO)
# logHandler.setFormatter(formatter)
# logHandler.suffix = "%Y"
#
# errorLogHandler = handlers.RotatingFileHandler('logs/error.log', maxBytes=5000, backupCount=0)
# errorLogHandler.setLevel(logging.ERROR)
# errorLogHandler.setFormatter(formatter)
#
# logger.addHandler(logHandler)
# logger.addHandler(errorLogHandler)

def main():
    for i in range(3):
        time.sleep(1)
        logger.info("A Sample Log Statement")
        logger.error("An error log statement")

main()