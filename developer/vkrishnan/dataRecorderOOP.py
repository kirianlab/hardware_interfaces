import tempfile, time, os, json, datetime, sys
from hardware.bronkhorst import Bronkhorst
from hardware.sensirion import Sensirion
from shimadzu_pump import shimadzu_driver as hplc
import pyqtgraph as pg
import numpy as np
import os
#pg.mkQApp()

'''Object Oriented version of Kkarpos's code for recordData.py
This is necessary for it to be used by other files.'''
class Record:
    def __init__(self):
        config = {'sensirion_serial_number': 'FTSTT5NA',
            'bronkhorst_com_port': 'COM3'}
        pathogen = lambda path: os.path.normpath(os.path.abspath(os.path.expanduser(path)))

        status_path = ''

        #FIXME: Update status paths to save/read in the repo, rather than on the desktop.
        #           Reasoning: These are txt files that trigger recordings, they do 
        #                      not need to be on the desktop.

        ##################################################
        # Set up Bronkhorst gas flow meter/controller
        ##################################################
        print("Setting up the Bronkhorst", flush=True)

        # set the status file path
        self.gas_status_path = pathogen("~/Desktop/sensor_status_gas.txt")
       
        # gather all the sensors available and initialize the class
        self.bronkhorst = Bronkhorst(com_port=config['bronkhorst_com_port'])
        
        print("Initialized Bronkhorst", flush=True)


        ##################################################
        # Set up Shimadzu liqid flow controller
        ##################################################
        print("Setting up the HPLC pump", flush=True)

        #FIXME: Update this to use the shimadzu.py script, it keeps things in the same units

        # set the status file path and connect to the pump
        self.hplc_status_path = pathogen("~/Desktop/sensor_status_hplc.txt")
        self.hplc = hplc.ShimadzuCbm20('192.168.200.99')
        self.hplc.login('raklab', 'sky2Blue')

        ##################################################
        # Set up Sensirion liquid flow meter
        ##################################################
        print("Setting up the Sensirion pump", flush=True)

        # set the status file path
        self.sensirion_status_path = pathogen("~/Desktop/sensor_status_liq.txt")

        # gather all the sensors available and initialize the class
        self.sensirion = Sensirion(serial_number=config['sensirion_serial_number'])

        ###################################################
        # Set up interface for fastcam recording
        ###################################################
        print("Setting up the FastCAM", flush=True)
        self.record_file = os.path.join(tempfile.gettempdir(), "ody_record")
        self.set_liquid(0)
        # import asyncio


    def write_sensirion_status(self,avg_n_points:int=10, wait_time:int=None):
        data = self.sensirion.data(avg_n_points=avg_n_points, wait_time=wait_time, resolution=16)
        # while data is None:
        #     data = sensirion.data(avg_n_points=avg_n_points, wait_time=wait_time)
        with open(self.sensirion_status_path, 'w') as f:
            json.dump(data, f, indent=4)  # sort_keys=True

    def write_bronkhorst_status(self, avg_n_points:int=10, setpoint:float=None):
        r"""
            FIXME: Add documentation here!
        """
        data = self.bronkhorst.data(avg_n_points=avg_n_points, setpoint=setpoint)
        # while data is None:
        #     data =  bronkhorst.data(avg_n_points=avg_n_points, setpoint=setpoint)
        with open(self.gas_status_path, 'w') as f:
            json.dump(data, f, indent=4)  # sort_keys=True
    def set_gas(self,flow):
        self.bronkhorst.setpoint = flow


    def write_hplc_status(self):
        now = datetime.datetime.now()
        timestamp = now.timestamp()  # machine readable
        timestamp_iso = now.isoformat(sep=" ")  # human   readable
        data = self.hplc.get_all()

        data['timestamp'] = timestamp
        data['timestamp_iso'] = timestamp_iso
        data['flow_units'] = 'ml/min'
        data['reading'] = data['flow'] * 1e3
        data['reading_units'] = 'ul/min'
        if data['pressure_unit'] != 1:
            raise ValueError("Is the HPLC pressure unit psi?")
        data['pressure_units'] = 'psi'

        with open(self.hplc_status_path, 'w') as f:
            json.dump(data, f, indent=4)
    def set_liquid(self, flow, start_pump=True):  # This is ul/min
       self.hplc.set('flow', flow*1e-3)
       self.hplc.start()
       self.write_hplc_status()


    def record(self):
        f = open(self.record_file, 'w')
        f.close()

    ####################################################
    # Record a range of liquid and gas flow rates
    ####################################################

    def write_status(self):
        self.write_bronkhorst_status()
        self.write_sensirion_status()
        self.write_hplc_status()

    def record_range(self,liquid_flows, gas_flows, 
                        equilibrate_time:int=10,
                        sweep_gas=True):
        print("\n\n Starting parameter sweep:", flush=True)
        count = 0
        for l in liquid_flows:
            # set the liquid flow rate
            self.set_liquid(l)
            print(f'Equilibrating liquid... waiting {equilibrate_time} seconds', flush=True)
            time.sleep(equilibrate_time)
            for g in gas_flows:
                # set the gas flow rate
                self.set_gas(g)
                time.sleep(5) # give gas a few seconds to adjust
                print('liquid =', l, ', gas = ', g, flush=True)

                # write the current gas and liq flow confgurations for recording
                self.write_status() # FIXME: write loop to check how much time has passed

                # record a movie
                print('recording video...', flush=True)
                self.record()

    def write_record(self):
        self.write_status()
        self.record()