import matplotlib
import json
import numpy as np
import math

from matplotlib import pyplot as plt

"This file will contain the calibration functions, this is because I cannot test code from the sensiron class without actually being connected to the sensor"
"The example is at the bottom of this document, I also added another function here to plot the data as that function is not added to the sensiron class"
def createRegressions(order=3, filepath="hplc_vs_sens_0.0-50.0ulpermin_heateroff_20220125.json"):
    # The dimensions array is of the form [high calibration0, highcalibration1, lowcalibration0, lowcalibration1] with each sub array of the form [sensiron max, hplc max]
    # FIXME: The dimensions array may need to be changed based upon what data from each sensor should be included in the fit.
    dimensions = [[50, 50], [5, 25], [5, 5], [20, 20]]
    f = json.load(open(filepath))
    config = f['config']
    data = f['data']
    xydata = []
    xydata.append([config['hplc_flowrate'], data['data_high_0']])
    xydata.append([config['hplc_flowrate'], data['data_high_1']])
    xydata.append([config['hplc_flowrate'], data['data_low_0']])
    xydata.append([config['hplc_flowrate'], data['data_low_1']])
    regressionArr = []
    for j in range(4):
        array = xydata[j]
        # the polynomial has x as the sensiron data and y as the hplc data
        x = array[1]
        y = array[0]
        indexes = []
        # Cuts out all data not in the specified range
        indexesy = [index for index in range(len(y)) if y[index] > dimensions[j][1]]
        indexesx = [index for index in range(len(x)) if x[index] > dimensions[j][0]]

        indexes = indexesx + indexesy;
        indexes = list(set(indexes));

        x = np.delete(x, indexes)
        y = np.delete(y, indexes)

        p = np.polyfit(x, y, order)

        regression = np.poly1d(p)

        # poly1d objects cannot be serialized, so I have to create an array with coefficients and a degree term
        regressionArr1 = [order]
        for i in range(order + 1):
            regressionArr1.append(regression[order - i])
        regressionArr.append(regressionArr1)
    return regressionArr
    # TODO: Possibly merge this function into createRegressions if there is no need to display regressions without writing to a json file
    # writeRegressions should be used to write regressions to json file, createregressions can be used to return the regressions
def writeRegressions(writeFilepath="Regressions.json"):
    with open(writeFilepath, 'w') as file:
        json_string = json.dumps(createRegressions())
        file.write(json_string)
def returnFittedFlowRate(sensorReading, index, regressionfilepath="Regressions.json"):
    # Index represents the sensor used
    # [0,1,2,3] = [high calibration0, high calibration1 low calibration0, low calibration1]
    # sensorReading should be a reading from the sensiron sensor, which would be converted using 3rd degree polynomial to a hplc result
    with open(regressionfilepath) as file:
        regression = json.load(file)[index]
    order = int(regression[0])
    result = 0
    for i in range(order + 1):
        result += regression[i + 1] * (sensorReading ** (order - i))
    return result
def plotRegressions(dimensions = [[50, 50], [5, 25], [5, 5], [20, 20]], datafilepath = "hplc_vs_sens_0.0-50.0ulpermin_heateroff_20220125.json"):
    f = json.load(open(datafilepath));
    config = f['config']
    data = f['data']
    fig, ax = plt.subplots(2, 2, figsize=(15, 10), tight_layout=True)
    plt.tight_layout()
    # Inintialize an array of attributes of the xy data
    xydata = []
    xydata.append(ax[0][0].plot(config['hplc_flowrate'], data['data_high_0'], '.b'))
    ax[0][0].set_title("High Flow, Calibration = 0")

    xydata.append(ax[0][1].plot(config['hplc_flowrate'], data['data_high_1'], '.b'))
    ax[0][1].set_title("High Flow, Calibration = 1")

    xydata.append(ax[1][0].plot(config['hplc_flowrate'], data['data_low_0'], '.r'))
    ax[1][0].set_title("Low Flow, Calibration = 0")

    # plot the low flow, calibration 1 data
    xydata.append(ax[1][1].plot(config['hplc_flowrate'], data['data_low_1'], '.r'))
    ax[1][1].set_title("Low Flow, Calibration = 1")

    for i in range(2):
        for j in range(2):
            ax[i][j].plot(config['hplc_flowrate'], config['hplc_flowrate'], 'k', label='HPLC Setpoints')
            xrange = dimensions[2*i+j][0]
            yrange = dimensions[2*i+j][1]
            ax[i][j].set_xlim(0, xrange)
            ax[i][j].set_ylim(0, yrange)
            ax[i][j].set_aspect('equal', adjustable='box')
            sensironfit = np.linspace(0, xrange+1, 20000)
            hplcfit = []
            for term in sensironfit:
                hplcfit.append(returnFittedFlowRate(term, 2*i+j))
            ax[i][j].plot(hplcfit, sensironfit, linewidth = 5, color='g')
            correlation_matrix = np.corrcoef(sensironfit, hplcfit)
            correlation_xy = correlation_matrix[0, 1]
            r_squared = correlation_xy ** 2

            print("Plot index: " + str(i) + ", " + str(j) + ": " + "R_squared = " + str(r_squared))



    fig.supylabel("Sensirion Flow Rate [uL/min]", fontsize=16)
    fig.supxlabel("HPLC Flow Rate [ul/min]", fontsize=16)
    fig.suptitle(f"Sensirion Low/High vs HPLC Flow Rate \n {config['avg_n_points']} Readings Averaged Per Datapoint",
                 fontsize=18)
    plt.show()






'''This will create the regressions.json file. It will be a 2d array: [high calibration0, high calibration1 low calibration0, low calibration1]
 Within each subarray, index 0 will be the fit order. While this isnt strictly necessary as it can be determined by the length of the subarray, I still added it
 '''
writeRegressions()
'''Now that there is a regressions.json file, a point of sensiron data can be converted to an approximate hplc flow rate using the following method'''
print(returnFittedFlowRate(10,0))
'''This is all the code does, however, I will now generate some plots of the regressions and the data so their accuracy can be observed'''
'''This method also prints rsq values for all plots'''
plotRegressions()





