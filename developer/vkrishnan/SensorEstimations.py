import json
import numpy as np

#This code assumes that the sensiron sensor is not providing the actual flow rates, and that the flow rates detected by the HPLC is a good representation of the actual flow rate

class Calibrater:

    filepath = ''
    def __init__(self, filepath):
        self.filepath = filepath
    #Takes a sensiron reading as a float or integer value, and converts that to a float which should be an estimation of the HPLC detected flow rate
    def returnEstimatedFlowRate(self, sensorReading, index):
        #Index represents the sensor used
        #[0,1,2] = [high calibration0, low calibration0, low calibration1]
        #sensorReading should be a reading from the sensiron sensor, which would be converted using 3rd degree polynomial to a hplc result
        with open(self.filepath) as file:
            regression = json.load(file)[index]
        order = int(regression[0])
        result = 0
        for i in range (order+1):
            result += regression[i+1]*sensorReading**(order-i)
        return(result)
#If you already have the regressions.json file, this class is irrelevant
#If you do not have said file, then run the method serialize, which will generate the regressions file from the original json heater off file, and then run return estimated flow rate
class Serializer:
    dimensions = []
    regressionArr = []
    def __init__(self):
        self.dimensions = [50,5,20]
        self.regressionArr.getRegressions()
    #The created json file will have a two dimensional array, with the first term of each nested array being the order, to create a polynomial
    #Within each nested array, the coefficients to the polynomial is kept in the order of highest degree term to 0 degree term.

    def serialize(self, filepath):
        with open(filepath, 'w') as file:
            json_string = json.dumps(self.getRegressions())
            file.write(json_string)
    #this function is there to construct the array. If you wanted to serialize to a json file, call the serialize method.
    def getRegressions(self, order=3):
        filepath = "hplc_vs_sens_0.0-50.0ulpermin_heateroff_20220125.json"
        f = json.load(open(filepath))
        config = f['config']
        data = f['data']
        xydata = []
        xydata.append([config['hplc_flowrate'], data['data_high_0']])
        xydata.append([config['hplc_flowrate'], data['data_low_0']])
        xydata.append([config['hplc_flowrate'], data['data_low_1']])
        regressionArr = []
        for j in range(3):
            array = xydata[j]
            # x and y are flipped so the polynomial has x as the sensiron data and y as the hplc data
            x = array[1]
            y = array[0]

            indexes = []

            indexes = [index for index in range(len(y)) if y[index] > self.dimensions[j]]

            x = np.delete(x, indexes)
            y = np.delete(y, indexes)
            p = np.polyfit(x, y, order)
            regression = np.poly1d(p)

            # poly1d objects cannot be serialized, so I have to create an array with coefficients and a degree term
            regressionArr1 = [order]
            for i in range(order + 1):
                regressionArr1.append(regression[order - i])

            regressionArr.append(regressionArr1)
        return regressionArr


