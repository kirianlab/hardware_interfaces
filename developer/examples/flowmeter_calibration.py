import time
import numpy as np
from hardware.harvard.pump import Pump
from hardware import sensirion


# CALIBRATION SETTINGS
# =====================================================
liquid_flows = [50, 20, 10, 5, 2, 1, 0.5, .2]
seconds_per_flow = 60
sample_rate_hz = 120
syringe_diameter_mm = 4.65
# ====================================================



# INITIALIZE SYRINGE PUMP
# ========================================================
harvard_config = {'port': 'COM9'}  # Ideally, serial number not port.  Ports can change.
pump1_config = {'diameter': syring_diameter_mm}
pump2_config = pump1_config
harvard_config['pump1'] = pump1_config
harvard_config['pump2'] = pump2_config
pump = Pump(port_to_use=harvard_config['port'])
pump.set_diameter(harvard_config['pump1']['diameter'], harvard_config['pump2']['diameter'])
pump.set_syringe_volume(1000, None)
#pump.set_infuse_rate(20, None)
#pump.set_target_volume(100, None)
#pump.start_infuse()
#FIXME: Need a "get_state" method for pump
# ====================================================



# INITIALIZE LIQUID FLOW METERS
# ========================================================
# FIXME: Need some configs for the flowmeters (with calibration fields set correctly)
n_samples = 1000
dt = 0.1
liquid1 = sensirion.Sensirion(serial_number='FTSTT5NA')
liquid2 = sensirion.Sensirion(serial_number='FTXEF0AHA')
# =======================================================


# COLLECT DATA

all_flows = []

for liquid_flow in liquid_flows:
    pump.set_infuse_rate(liquid_flow, None)
    pump.start_infuse()
    time.time.sleep(3)

# 	flow1 = np.zeros(n_samples)
# 	flow2 = np.zeros(n_samples)
# 	for i in range(n_samples):
# 		time.sleep(dt)
# 		flow1[i] = liquid1.reading
# 		flow2[i] = liquid2.reading
# 	all_flows.append([liquid_flow, flow1, flow2])  # Needs better data structure

# # Finally, save the results (or save files within the loop)

