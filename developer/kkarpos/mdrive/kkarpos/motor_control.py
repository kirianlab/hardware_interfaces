import serial
import numpy as np
import time


class MDrive:

    def __init__(self, axis, serial_port='', baudrate=9600, timeout=0):
        self.serial_port = serial_port
        self.axis = axis
        self.baudrate = baudrate
        self.timeout = timeout
        self.ser = None
        self.portFound = False
        self.serNbr = ''

        i = 0
        while self.portFound == False and i <= 127:

            try:
                self.serial_port = '/dev/ttyUSB' + str(i)

                self.ser = serial.Serial(
                    self.serial_port,
                    baudrate=self.baudrate,
                    timeout=self.timeout)

                if self.ser.isOpen():

                    serial_port = self.serial_port

                    self.ser.write('PR SN \r\n'.encode('utf-8'))
                    time.sleep(0.1)

                    self.serNbr = self.ser.read(
                        self.ser.in_waiting).decode('UTF-8')

                    self.serNbr = ''.join(
                        i for i in self.serNbr if i.isdigit())

                    if self.axis == 'y' and self.serNbr == '063140354':

                        self.serial_port = self.serial_port
                        self.portFound = True
                    if self.axis == 'x' and self.serNbr == '063140353':

                        self.serial_port = self.serial_port
                        self.portFound = True
                    elif self.axis == 'z' and self.serNbr == '315110225':

                        self.serial_port = self.serial_port
                        self.portFound = True

                    i += 1

            except BaseException:
                i += 1

        self.serial_port = serial_port
        self.ser = serial.Serial(
            self.serial_port,
            baudrate=self.baudrate,
            timeout=self.timeout)
        print('Serial port = ' + self.serial_port)
        print('axis = ' + self.axis)
        print('serNbr = ' + self.serNbr)
        print('Connection opened for {}-axis driver!'.format(self.axis))

    def translate(self, steps):
        command = 'MR {}\r\n'.format(steps)
        self.ser.write(command.encode('utf-8'))

    def acceleration(self, acc):
        if acc == 'slow':
            a = 50000
        elif acc == 'medium':
            a = 250000
        elif acc == 'fast':
            a = 2000000
        else:
            print('Error: Command not recognized. \nCurrent options: slow, medium, fast')

        command = 'A {}\r\n'.format(a)
        self.ser.write(command.encode('utf-8'))

    def move(self, mm):
        try:
            steps = 83035  # 83035 steps equates to one milimeter in translations
            mv = steps * mm
            if abs(mv) <= steps * \
                    3:  # making sure the motors doesn't over translate
                command = 'MR {}\r\n'.format(mv)
                self.ser.write(command.encode('utf-8'))
            else:
                print('Error: Move is too big. \nInput must be smaller than 3')

        except ValueError:
            print('Error: Command not recognized. \nInput must be an integer')


    def getVelocity(self):
        command = 'MR {}\r\n'.format('PR V')
        self.ser.write(command.encode('utf-8'))
        time.sleep(0.1)
        response = self.ser.read(100)
        print(response.decode()+'\n')


