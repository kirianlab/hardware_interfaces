# Motor Control 

This folder contains all the scripts needed to control MDrive motors. As of this writing, only one script is used to control the devices. 

***TO DO***: 
- Map motor steps to millimeters.
- Automate the motor serial number determination
- Figure out whats wrong with the USB serial permission issues 
- Get the serial communication cable ID's and write them here

Mapping steps to millimeters:
- 83035 steps equates to one millimeters

Fixing the USB serial permission issues:
1. Run this in the terminal `who` to get the username
2. Run this in the terminal `sudo usermod -aG dialout <username>`
3. Run this in the terminal `sudo chmod 777 /dev/ttyUSB*` 

Motors in use:
- Three M Drive 23 Plus

Serial communication cables:
- USB to RS422 Comm Converter Labeled X: serial number = 20052015121644, Vendor=10c4, ProdID=ea60, Rev=01.00,
  Product=Silicon Labs CP210x USB to UART Bridge
- USB to RS422 Comm Converter Labeled Y: serial number = 20052015121500, Vendor=10c4, ProdID=ea60, Rev=01.00,
  Product=Silicon Labs CP210x USB to UART Bridge
## Running the Script -- Examples

See the below examples:

### Controlling 3 Motors

In iPython, run the `motor_control.py` script. Then, 
```python 

# Initialize the motors:
x_axis = MDrive('x')
y_axis = MDrive('y')
z_axis = MDrive('z')

```

*NOTE* Currently, some manual work is involved with determining the correct serial port. See the serial port section below.

Say we'd like to translate the system up by 3000 steps, right by 5600 steps, and forward by 10000 steps. In iPython, run the following
```python

# Translate the z-axis
z_axis.translate(3000)

# Translate the y-axis
y_axis.translate(5600)

# Translate the x-axis
x_axis.translate(10000)

```

Let say we want to translate by one milimeter per given parameter we would run the following below 
```python 

# Runs the motor by the parameter times 83035 step/one milimeter:
x_axis.move(2)
y_axis.move(3)
z_axis.move(1)

```




## Serial Port Determination

Serial ports are annoying to figure out. Right now, the user needs to manually determine which port belongs to which motor. The method to determine this is as follows. 

1. Plug the x-axis motor into a USB port
2. Run this in the terminal `ls /dev/ttyUSB*` and make note of the address
3. Plug the y-axis into a different USB port
4. Repeat step 2
5. Plug the z-axis into a different USB port
6. Repeat step 2

When all three serial ports are determined, use those paths in the `MDrive` class during initialization.

After running through those steps, ensure that the motors are correct by running the script with few very small step values (~1000). This should eventually be automated.
