# core
import ctypes
import h5py
import numpy as np
import sys

# hardware 
sys.path.append("hardware/hardware_interfaces_sdk/ids")
import ueye
from hardware.harvard import Pump33DDS
from hardware.ids import ids_cam as ids


# graphics
import cv2
import pyqtgraph as pg
from pyqtgraph.Qt import QtCore, QtGui
import pyqtgraph.ptime as ptime




# simple class initialization script for pdc triggering project

# pump = Pump33DDS(serial_number='P100465')
pump = Pump33DDS('/dev/ttyACM0')



p1 = {"which_pump": "p1",
        "syringe_diameter": 1, `# mm
        "syringe_volume": 50, # ul 
        "target_volume": 40, # ul
        "infuse_rate": 1, # ul/min
        "withdraw_rate": 1, # ul/min
        "force": None}

p2 = {"which_pump": "p2",
        "syringe_diameter": 14, # mm
        "syringe_volume": 5000, # ul 
        "target_volume": 4000, # ul
        "infuse_rate": 100, # ul/min
        "withdraw_rate": 100, # ul/min
        "force": None}


