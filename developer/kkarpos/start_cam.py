# core
import ctypes
import h5py
import numpy as np
import sys
from subprocess import call

# hardware 
sys.path.append("hardware/hardware_interfaces_sdk/ids")
import ueye
from hardware.ids import ids_cam as ids

# graphics
import cv2
import pyqtgraph as pg
from pyqtgraph.Qt import QtCore, QtGui
import pyqtgraph.ptime as ptime

# cam = ids.IDS_Cam()

live = ids.IDS_Cam_Live()

# print("Opening WaveForms")
# sync_cam_pdc_fp = "/home/kkarpos/kkarpos/sync_pulse_pdc/sync_pulse_pdc.dwf3work"
# call(['xdg-open', sync_cam_pdc_fp])

live.pyqt_live_mode()
# live.opencv_live_triggered_mode()
# live.pyqt_live_triggered_mode()
