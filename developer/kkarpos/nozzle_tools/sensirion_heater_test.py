import tempfile, time, os, json, datetime, sys
from hardware.sensirion import Sensirion
from shimadzu_pump import shimadzu_driver as hplc
import pyqtgraph as pg
import numpy as np
import pylab as plt
pg.mkQApp()


config = {'sensirion_highflow': 'FTSTT5NA',
            'sensirion_lowflow': 'FTXEF0AHA',
            'low_flow_calibration': 0, # int, 0 or 1
            'high_flow_calibration': 0,
            'heater_status': 'on', # str, 'on' or 'off'
            'rec_n_datapoints': 10,
            'avg_n_points': 5,
            'hplc_flowrate': 25}

nl_to_ul = 1000


# setup the hplc pump
hplc = hplc.ShimadzuCbm20('192.168.200.99')
hplc.login('raklab', 'sky2Blue')

# setup the sensirion
sensirion_high = Sensirion(serial_number=config['sensirion_highflow'], 
                            calibration=config['high_flow_calibration'])
sensirion_low = Sensirion(serial_number=config['sensirion_lowflow'], 
                            calibration=config['low_flow_calibration'])

# clear anything in the buffer just in case
sensirion_high.clear_buffer
sensirion_low.clear_buffer

print(f"High Flow:  {sensirion_high.units}")
print(f"Low Flow:  {sensirion_low.units}")

#sys.exit()

def set_liquid(flow, start_pump=True):  # This is ul/min
    '''set the hplc flow rate
    '''
    hplc.set('flow', flow*1e-3)
    hplc.start()

set_liquid(config['hplc_flowrate'])

time.sleep(2)

data_high_hon = {'sdev':[], 'avg_data':[]}
data_low_hon = {'sdev':[], 'avg_data':[]}
rec_range = range(config['rec_n_datapoints'])
avg_n_points = config['avg_n_points']
for d in rec_range:
    dat_high = sensirion_high.data(avg_n_points=avg_n_points, heater=config['heater_status'])
    data_high_hon['avg_data'].append(dat_high['average_reading'] )
    data_high_hon['sdev'].append(dat_high['standard_deviation'])

    dat_low = sensirion_low.data(avg_n_points=avg_n_points, heater=config['heater_status'])
    data_low_hon['avg_data'].append(dat_low['average_reading'] / nl_to_ul)
    data_low_hon['sdev'].append(dat_low['standard_deviation'] / nl_to_ul)

    print(f"Datapoint {d+1}/{rec_range[-1]+1} done", flush=True)



# data_high_hoff = {'sdev':[], 'avg_data':[]}
# data_low_hoff = {'sdev':[], 'avg_data':[]}
# rec_range = range(config['rec_n_datapoints'])
# avg_n_points = config['avg_n_points']
# for d in rec_range:
#     dat_high = sensirion_high.data(avg_n_points=avg_n_points, heater='off')
#     data_high_hoff['avg_data'].append(dat_high['average_reading'] )
#     data_high_hoff['sdev'].append(dat_high['standard_deviation'])

#     dat_low = sensirion_low.data(avg_n_points=avg_n_points, heater='off')
#     data_low_hoff['avg_data'].append(dat_low['average_reading'] / nl_to_ul)
#     data_low_hoff['sdev'].append(dat_low['standard_deviation'] / nl_to_ul)

#     print(f"Datapoint {d+1}/{rec_range[-1]+1} done", flush=True)


hplc.stop()


fig, ax = plt.subplots(1, 1, figsize=(12, 10), sharey=True)
ax.errorbar(x=rec_range, y=data_high_hon['avg_data'], 
                yerr=data_high_hon['sdev'],
                # barsabove=True,
                ecolor='blue',
                # uplims=True, lolims=True,
                linestyle='none', alpha=0.3,
                label='High Flow Error')
ax.plot(rec_range, data_high_hon['avg_data'], '-b', label='Sensirion High Flow Readings')

ax.errorbar(x=rec_range, y=data_low_hon['avg_data'], 
                yerr=data_low_hon['sdev'],
                # barsabove=True,
                ecolor='red',
                # uplims=True, lolims=True,
                linestyle='none', alpha=0.3,
                label=' Low Flow Error')
ax.plot(rec_range, data_low_hon['avg_data'], '-r', label='Sensirion Low Flow Readings')

# ax[1].errorbar(x=rec_range, y=data_high_hoff['avg_data'], 
#                 yerr=data_high_hoff['sdev'],
#                 # barsabove=True,
#                 ecolor='blue',
#                 # uplims=True, lolims=True,
#                 linestyle='none', alpha=0.3,
#                 label='High Flow Error')
# ax[1].plot(rec_range, data_high_hoff['avg_data'], '-b', label='Sensirion High Flow Readings')

# ax[1].errorbar(x=rec_range, y=data_low_hoff['avg_data'], 
#                 yerr=data_low_hoff['sdev'],
#                 # barsabove=True,
#                 ecolor='red',
#                 # uplims=True, lolims=True,
#                 linestyle='none', alpha=0.3,
#                 label=' Low Flow Error')
# ax[1].plot(rec_range, data_low_hoff['avg_data'], '-r', label='Sensirion Low Flow Readings')


ax.hlines(y=config['hplc_flowrate'], 
            xmin=rec_range[0], xmax=rec_range[-1],
            colors='black', linestyle='dashed', alpha=0.5, label='HPLC Flow Rate')
ax.legend()
    #ax[i].set_xlim(1, rec_range[-1])

# ax[0].set_title('heater on')
# ax[1].set_title('heater off')
plt.xlabel('Recording Number')
ax.set_ylabel('Flow Rate [uL/min]')
fig.suptitle(f'''Sensirion Accuracy Test
    {avg_n_points} readings averaged per datapoint
    High flow calib: {config['high_flow_calibration']}; Low flow calib: {config['low_flow_calibration']}
    Heater status: {config['heater_status']}''')
fig.tight_layout()
#plt.legend()
plt.show()








