import tempfile, time, os, json, datetime, sys
from hardware.sensirion import Sensirion
from shimadzu_pump import shimadzu_driver as hplc
import pyqtgraph as pg
import numpy as np
import pylab as plt
pg.mkQApp()


config = {'sensirion_highflow': 'FTSTT5NA',
            'sensirion_lowflow': 'FTXEF0AHA'}

nl_to_ul = 1000


# setup the hplc pump
hplc = hplc.ShimadzuCbm20('192.168.200.99')
hplc.login('raklab', 'sky2Blue')

# setup the sensirion
sensirion_high = Sensirion(serial_number=config['sensirion_highflow'])
sensirion_low = Sensirion(serial_number=config['sensirion_lowflow'], calibration=1)



print(f"Low Flow:  {sensirion_low.units}")
print(f"High Flow:  {sensirion_high.units}")



# sys.exit()

def set_liquid(flow, start_pump=True):  # This is ul/min
    '''set the hplc flow rate
    '''
    hplc.set('flow', flow*1e-3)
    hplc.start()

hplc_flowrate = 20
set_liquid(hplc_flowrate)

time.sleep(2)

data_high = {'sdev':[],
        'avg_data':[]}
data_low = {'sdev':[],
        'avg_data':[]}
rec_range = range(15)
avg_n_points = 5
for d in rec_range:
    dat_high = sensirion_high.data(avg_n_points=avg_n_points, wait_time=None)
    data_high['avg_data'].append(dat_high['average_reading'] )
    data_high['sdev'].append(dat_high['standard_deviation'])
    dat_low = sensirion_low.data(avg_n_points=avg_n_points, wait_time=None)
    data_low['avg_data'].append(dat_low['average_reading'] / nl_to_ul)
    data_low['sdev'].append(dat_low['standard_deviation'] / nl_to_ul)
    print(f"Datapoint {d+1}/{rec_range[-1]+1} done", flush=True)


hplc.stop()


plt.figure()
plt.errorbar(x=rec_range, y=data_high['avg_data'], 
                yerr=data_high['sdev'],
                # barsabove=True,
                ecolor='blue',
                # uplims=True, lolims=True,
                linestyle='none', alpha=0.3,
                label='High Flow Error')
plt.plot(rec_range, data_high['avg_data'], '-b', label='Sensirion High Flow Readings')

plt.errorbar(x=rec_range, y=data_low['avg_data'], 
                yerr=data_low['sdev'],
                # barsabove=True,
                ecolor='red',
                # uplims=True, lolims=True,
                linestyle='none', alpha=0.3,
                label=' Low Flow Error')
plt.plot(rec_range, data_low['avg_data'], '-r', label='Sensirion Low Flow Readings')

plt.hlines(y=hplc_flowrate, 
            xmin=rec_range[0], xmax=rec_range[-1],
            colors='black', linestyle='dashed', alpha=0.5, label='HPLC Flow Rate')
plt.xlabel('Recording Number')
plt.ylabel('Flow Rate [uL/min]')
plt.title(f'''Sensirion Accuracy Test \n {avg_n_points} readings averaged per datapoint''')
plt.legend()
plt.xlim(1, rec_range[-1])
plt.show()








