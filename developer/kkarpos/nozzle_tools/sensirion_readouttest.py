import tempfile, time, os, json, datetime, sys
from hardware.sensirion import Sensirion
from shimadzu_pump import shimadzu_driver as hplc
import pyqtgraph as pg
import numpy as np
import pylab as plt
from time import time
pg.mkQApp()

r'''
    This script will test the resolution function of the sensirion class. According to 
    Sensirion, changing the resolution directly affects the readout speed. 
    We test that here.
'''

# -----
# set things up
# -----

config = {'sensirion_highflow': 'FTSTT5NA',
            'sensirion_lowflow': 'FTXEF0AHA',
            'hplc_flowrate': 20, #ul/min
            'record_n_readings': 50,
            'average_n_points': 1,
            'resolution': range(9,17)
            }

nl_to_ul = 1000


# setup the hplc pump
hplc = hplc.ShimadzuCbm20('192.168.200.99')
hplc.login('raklab', 'sky2Blue')

# setup the sensirion
sensirion_high = Sensirion(serial_number=config['sensirion_highflow'])
#                            resolution=config['resolution'])
sensirion_low = Sensirion(serial_number=config['sensirion_lowflow'],
#                            resolution=config['resolution'],
                            calibration=1)


#print(f"Sensirion high resolution = {sensirion_high.resolution}", flush=True)
#print(f"Sensirion low resolution = {sensirion_low.resolution}", flush=True)

# ----
# testing the speed
# ----
hplc.stop()
hplc.set('flow', config['hplc_flowrate']*1e-3)
hplc.start()
# low_time_sm = []
# high_time_sm = []
# for t in config['resolution']:
#     sensirion_high.resolution = t
#     tik1 = time()
#     for d in range(config['record_n_readings']):
#         dat_high = sensirion_high.data(avg_n_points=config['average_n_points'], wait_time=None)
#     high_time_sm.append(time() - tik1)

#     sensirion_low.resolution = t
#     tik2 = time()
#     for d in range(config['record_n_readings']):
#         dat_low = sensirion_low.data(avg_n_points=config['average_n_points'], wait_time=None)
#     low_time_sm.append(time() - tik2)

# single measure mode
low_time_sm = []
high_time_sm = []
for i, t in enumerate(config['resolution']):
    sensirion_high.resolution = t
    dat_high, t_high = sensirion_high.rec_n_points(n_points=config['record_n_readings'], timeit=True)
    high_time_sm.append(t_high)
    dat_low, t_low = sensirion_high.rec_n_points(n_points=config['record_n_readings'], timeit=True)
    low_time_sm.append(t_low)
    print(f"single measure mode resolution: {i+1}/{len(config['resolution'])}", flush=True)


# continuous measure mode
low_time_cm = []
high_time_cm = []
# sensirion_high.set("start_continuous_measure")
# sensirion_low.set("start_continuous_measure")
for i, t in enumerate(config['resolution']):
    sensirion_high.resolution = t
    dat_high, t_high = sensirion_high.rec_n_points_continuous(n_points=config['record_n_readings'], timeit=True)
    high_time_cm.append(t_high)
    dat_low, t_low = sensirion_high.rec_n_points_continuous(n_points=config['record_n_readings'], timeit=True)
    low_time_cm.append(t_low)
    print(f"continuous measure mode resolution: {i+1}/{len(config['resolution'])}", flush=True)

# sensirion_high.set("stop_continuous_measure")
# sensirion_low.set("stop_continuous_measure")



hplc.stop()

title = f"""
    Datapoints recorded per resolution value: {config['record_n_readings']} 
    """

fig, ax = plt.subplots(2,1, sharex=True, figsize=(12,10))
ax[0].plot(config['resolution'], low_time_sm, 'ob', label='Low flow Single Measure Mode')
ax[0].plot(config['resolution'], high_time_sm, 'xr', label='High flow Single Measure Mode')
ax[1].plot(config['resolution'], low_time_cm, 'ob', label='Low flow Continuous Measure Mode')
ax[1].plot(config['resolution'], high_time_cm, 'xr', label='High flow Continuous Measure Mode')

fig.suptitle(title, fontsize=14)
ax[1].set_xlabel('Resolution [bits]', fontsize=12)
for i in [0,1]:
    ax[i].set_ylabel('Record time [seconds]', fontsize=12)
    ax[i].grid(alpha=0.5)
    ax[i].legend()
fig.tight_layout()
plt.show()

#print(f"""
#    At a resolution of {config['resolution']} bits,
#    Recorded {config['record_n_readings']} datapoints
#    with {config['average_n_points']} readings averaged per datapoints
#    in {time:0.2f} seconds""")



















