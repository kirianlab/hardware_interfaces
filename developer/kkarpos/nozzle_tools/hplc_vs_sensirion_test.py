import tempfile, time, os, json, datetime, sys
from hardware.sensirion import Sensirion
from shimadzu_pump import shimadzu_driver as hplc
import pyqtgraph as pg
import numpy as np
import pylab as plt
import json, os
pg.mkQApp()

r"""
This script makes four plots. It loops through a series of HPLC flow rates, collects a single datapoint,
and appends it to a master list. From there, it plots the HPLC flow rates versus the 
Sensirion flow rates. 

It also loops across each calibration factor and makes a 4x4 grid of datapoints. 

Note that data is collected in continuous mode.

"""



config = {'sensirion_highflow': 'FTSTT5NA',
            'sensirion_lowflow': 'FTXEF0AHA',
            'heater_status': ['off'], # ['on'], ['off'], ['default'], or ['off', 'on']
            'avg_n_points': 1, # the number of readings averaged per datapoint
            'hplc_flowrate':np.linspace(0, 50, 50).tolist(), # must be a list of flow rates in ul/min
            'wait_time': True, # Waits 5 seconds after changing flow rates to allow for equilibration
            'date': 20220223}

nl_to_ul = 1000 # conversion factor for the low flow sensor
savedat = True
savename = f"hplc_vs_sens_{config['hplc_flowrate'][0]}-{config['hplc_flowrate'][-1]}ulpermin_heater{config['heater_status'][0]}_{config['date']}"
showfig = False

# setup the hplc pump
hplc = hplc.ShimadzuCbm20('192.168.200.99')
hplc.login('raklab', 'sky2Blue')

def set_liquid(flow, start_pump=True):  # This is ul/min
    '''set the hplc flow rate
    '''
    hplc.set('flow', flow*1e-3)
    hplc.start()


# start with a calibration of 0 for both sensors
sensirion_high = Sensirion(serial_number=config['sensirion_highflow'], calibration=0)
sensirion_low = Sensirion(serial_number=config['sensirion_lowflow'], calibration=0)

data = {'data_high_0': [],
        'data_high_1': [],
        'data_low_0': [],
        'data_low_1': [],
        'hplc_flowrate': config['hplc_flowrate']}


for flow in config['hplc_flowrate']:

    set_liquid(flow) # set the hplc flowrate
    if config['wait_time']:
        print(f"Letting the liquid equilibrate to {flow:0.2f} ul/min... waiting 5 seconds.", end="\r", flush=True)
        time.sleep(5)

    for i in [0, 1]:
        # set the sensirion calibration field
        sensirion_high.calibration = i
        time.sleep(0.01)
        dat_high = sensirion_high.data(avg_n_points=config['avg_n_points'], heater=config['heater_status'][0])
        data[f'data_high_{sensirion_high.calibration}'].append(dat_high['average_reading'])

    for j in [0, 1]:
        # set the sensirion calibration field
        sensirion_low.calibration = j
        time.sleep(0.01)
        dat_low = sensirion_low.data(avg_n_points=config['avg_n_points'], heater=config['heater_status'][0])
        data[f'data_low_{sensirion_low.calibration}'].append(dat_low['average_reading'] / nl_to_ul)


##############
hplc.stop() # stop the hplc pump after the data recording is done
##############


fig, ax = plt.subplots(2,2, figsize=(15,10), sharex=True, tight_layout=True)

ax[0][0].plot(config['hplc_flowrate'], data['data_high_0'], '.b')
ax[0][0].set_title("High Flow, Calibration = 0")

ax[0][1].plot(config['hplc_flowrate'], data['data_high_1'], '.b')
ax[0][1].set_title("High Flow, Calibration = 1")

ax[1][0].plot(config['hplc_flowrate'], data['data_low_0'], '.r')
ax[1][0].set_title("Low Flow, Calibration = 0")

ax[1][1].plot(config['hplc_flowrate'], data['data_low_1'], '.r')
ax[1][1].set_title("Low Flow, Calibration = 1")

for i in [0,1]:
    for j in [0,1]:
        ax[i][j].plot(config['hplc_flowrate'], config['hplc_flowrate'], 'k', label='HPLC Setpoints')

fig.supylabel("Sensirion Flow Rate [uL/min]", fontsize=16)
fig.supxlabel("HPLC Flow Rate [ul/min]", fontsize=16)
fig.suptitle(f"Sensirion Low/High vs HPLC Flow Rate \n {config['avg_n_points']} Readings Averaged Per Datapoint", fontsize=18)

ax[0][1].legend()


if savedat:
    os.makedirs('data_and_figures', exist_ok=True)
    plt.savefig('data_and_figures/'+savename+'.png')
    out_data = {'data': data, 'config': config}
    with open('data_and_figures/'+savename+'.json', "w") as out:
        json.dump(out_data, out)
if showfig:
    plt.show()





























