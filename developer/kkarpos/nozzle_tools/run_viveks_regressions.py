import json, os
from hardware.sensirion import Sensirion
import pyqtgraph as pg
import numpy as np


pathogen = lambda path: os.path.normpath(os.path.abspath(os.path.expanduser(path)))
sensirion = Sensirion(serial_number='FTSTT5NA')


basepath = "/hardware_interfaces/developer/kkarpos/nozzle_tools/" 
datapath = pathogen(basepath+"data_and_figures/hplc_vs_sens_0.0-50.0ulpermin_heateroff_20220223.json")
outpath = pathogen(basepath+"data_and_figures/hplc_vs_sens_0.0-50.0ulpermin_heateroff_20220223_regressions.json")

# reg = sensirion.create_regressions(filepath=datapath)
sensirion.plot_regressions(datapath=datapath)

# reg = sensirion.write_regressions(regression_filepath=datapath, write_filepath=outpath)

# for i in [0,1,2,3]:
#     reg_fit = sensirion.return_fitted_flowate(sensor_reading=, index=i, regression_filepath=outpath)








































































