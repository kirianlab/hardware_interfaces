import tempfile, time, os, json, datetime, sys
from hardware.bronkhorst import Bronkhorst
from hardware.sensirion import Sensirion
from shimadzu_pump import shimadzu_driver as hplc
import pyqtgraph as pg
import numpy as np
pg.mkQApp()


config = {'sensirion_serial_number': 'FTSTT5NA',
            'bronkhorst_com_port': 'COM3'}





pathogen = lambda path: os.path.normpath(os.path.abspath(os.path.expanduser(path)))

status_path = ''

#FIXME: Update status paths to save/read in the repo, rather than on the desktop.
#           Reasoning: These are txt files that trigger recordings, they do 
#                      not need to be on the desktop.

##################################################
# Set up Bronkhorst gas flow meter/controller
##################################################
print("Setting up the Bronkhorst", flush=True)

# set the status file path
gas_status_path = pathogen(r"D:\data\microjets\logs\bronkhorst_EL-Flow_COM3\status.json")

# gather all the sensors available and initialize the class
bronkhorst = Bronkhorst(com_port=config['bronkhorst_com_port'])

print("Initialized Bronkhorst", flush=True)

def write_bronkhorst_status(avg_n_points:int=10, setpoint:float=None):
    r"""
        FIXME: Add documentation here!
    """
    data = bronkhorst.data(avg_n_points=avg_n_points, setpoint=setpoint)
    # while data is None:
    #     data =  bronkhorst.data(avg_n_points=avg_n_points, setpoint=setpoint)
    with open(gas_status_path, 'w') as f:
        json.dump(data, f, indent=4)  # sort_keys=True
def set_gas(flow):
    bronkhorst.setpoint = flow


##################################################
# Set up Shimadzu liqid flow controller
##################################################
print("Setting up the HPLC pump", flush=True)

#FIXME: Update this to use the shimadzu.py script, it keeps things in the same units

# set the status file path and connect to the pump
hplc_status_path = pathogen(r"D:\data\microjets\logs\sensirion_SLI-0430_FTSTT5NA\status.json")
hplc = hplc.ShimadzuCbm20('192.168.200.99')
hplc.login('raklab', 'sky2Blue')

def write_hplc_status():
    now = datetime.datetime.now()
    timestamp = now.timestamp()  # machine readable
    timestamp_iso = now.isoformat(sep=" ")  # human   readable
    data = hplc.get_all()

    data['timestamp'] = timestamp
    data['timestamp_iso'] = timestamp_iso
    data['flow_units'] = 'ml/min'
    data['reading'] = data['flow'] * 1e3
    data['reading_units'] = 'ul/min'
    if data['pressure_unit'] != 1:
        raise ValueError("Is the HPLC pressure unit psi?")
    data['pressure_units'] = 'psi'

    with open(hplc_status_path, 'w') as f:
        json.dump(data, f, indent=4)
def set_liquid(flow, start_pump=True):  # This is ul/min
   hplc.set('flow', flow*1e-3)
   hplc.start()
   write_hplc_status()



##################################################
# Set up Sensirion liquid flow meter
##################################################
print("Setting up the Sensirion pump", flush=True)

# set the status file path
sensirion_status_path = pathogen(r"D:\data\microjets\logs\shimadzu_LC-20AD\status.json")

# gather all the sensors available and initialize the class
sensirion = Sensirion(serial_number=config['sensirion_serial_number'])

def write_sensirion_status(avg_n_points:int=10, wait_time:int=None):
    data = sensirion.data(avg_n_points=avg_n_points, wait_time=wait_time)
    # while data is None:
    #     data = sensirion.data(avg_n_points=avg_n_points, wait_time=wait_time)
    with open(sensirion_status_path, 'w') as f:
        json.dump(data, f, indent=4)  # sort_keys=True


###################################################
# Set up interface for fastcam recording
###################################################
print("Setting up the FastCAM", flush=True)
record_file = os.path.join(tempfile.gettempdir(), "ody_record")
# import asyncio
def record():
    f = open(record_file, 'w')
    f.close()
    while os.path.isfile(record_file):
        time.sleep(0.2)
    print("recording done")

####################################################
# Record a range of liquid and gas flow rates
####################################################

def write_status():
    write_bronkhorst_status()
    write_sensirion_status()
    write_hplc_status()

def record_range(liquid_flows, gas_flows, 
                    equilibrate_time:int=30,
                    sweep_gas=True):
    print("\n\n Starting parameter sweep:", flush=True)
    count = 0
    for l in liquid_flows:
        # set the liquid flow rate
        set_liquid(l)
        print(f'Equilibrating liquid... waiting {equilibrate_time} seconds', flush=True)
        time.sleep(equilibrate_time)
        for g in gas_flows:
            # set the gas flow rate
            set_gas(g)
            time.sleep(20) # give gas a few seconds to adjust
            print('liquid =', l, ', gas = ', g, flush=True)

            # write the current gas and liq flow confgurations for recording
            write_status() # FIXME: write loop to check how much time has passed

            # record a movie
            print('recording video...', flush=True)
            record()

def write_record():
    write_status()
    record()

# record_range(liquid_flows=[10,12], 
#                 gas_flows=[10,15,20])

# hplc.stop()

# if 1:
#     for i in range(10):
#         write_status()
