import tempfile, time, os, json, datetime, sys
from hardware.sensirion import Sensirion
from shimadzu_pump import shimadzu_driver as hplc
import pyqtgraph as pg
import numpy as np
import pylab as plt
pg.mkQApp()

r"""
This script will make a plot of sensirion readings as a function of time. 
The main goal here is to test that the calibration and heater parameters
are doing what we expect.

The heater status has two states, on and off. The script tests the 
calibration parameters (0 and 1 for each sensor) against the heater status.

The code will make a 4x2 plot if config['heater_status'] == ['off', 'on']. 
Otherwise, it will make a 2x2 plot with the given heater parameter. 
Do not use other options, the code will crash. A heater status of 'default'
is meant to confirm that the default state is 'off'. Make sure to comment
out the sensor status fields in sensirion.data(), the default here is 'off'.
From initial tests, it was obvious that the default is off anyway, so there
may not be a need to do this again.


For those with Slack access, you can find the discussion here:
https://kirianlab.slack.com/archives/CPVU4JK8U/p1643099809045400

"""

config = {'sensirion_highflow': 'FTSTT5NA',
            'sensirion_lowflow': 'FTXEF0AHA',
            'heater_status': ['default'], # ['on'], ['off'], ['default'], or ['off', 'on']
            'rec_n_datapoints': 30, # the number of datapoints to capture
            'avg_n_points': 10, # the number of readings averaged per datapoint
            'hplc_flowrate': [2, 5, 10, 20, 30, 50], # must be a list of flow rates in ul/min
            'wait_time': True, # Waits 10 seconds after changing flow rates to allow for equilibration
            'savefig': True, # saves figures instead of plt.plot()
            'date': 20220125 # date of recording, needed for savefig.
            }

nl_to_ul = 1000 # conversion factor for low-flow sensirion


# setup the hplc pump
hplc = hplc.ShimadzuCbm20('192.168.200.99')
hplc.login('raklab', 'sky2Blue')

def set_liquid(flow, start_pump=True):  # This is ul/min
    '''sets the hplc flow rate
    '''
    hplc.set('flow', flow*1e-3)
    hplc.start()

# rec_range = range(config['rec_n_datapoints'])
# avg_n_points = config['avg_n_points']

# setup the sensirions with the calibration set to 0
sensirion_high = Sensirion(serial_number=config['sensirion_highflow'], calibration=0)
sensirion_low = Sensirion(serial_number=config['sensirion_lowflow'], calibration=0)

def loop_record(heater, rec, avg):
    # gets the averaged data
    high = {'sdev':[], 'avg_data':[], 'rec_time': []}
    low = {'sdev':[], 'avg_data':[], 'rec_time': []}
    tik = time.time()
    for d in rec:
        # record high flow data
        dh = sensirion_high.data(avg_n_points=avg, heater=heater)
        high['avg_data'].append(dh['average_reading'] )
        high['sdev'].append(dh['standard_deviation'])
        high['rec_time'].append(time.time() - tik)

        # record low flow data
        dl = sensirion_low.data(avg_n_points=avg, heater=heater)
        low['avg_data'].append(dl['average_reading'] / nl_to_ul)
        low['sdev'].append(dl['standard_deviation'] / nl_to_ul)
        low['rec_time'].append(time.time() - tik)

        # printing stuff for terminal debugging
        print(f"\t\t\t Datapoint {d+1}/{range(config['rec_n_datapoints'])[-1]+1} done", flush=True, end="\r")
        if d == rec[-1]:
            print(f"\t\t\t Datapoint {d+1}/{range(config['rec_n_datapoints'])[-1]+1} done", flush=True)

    return high, low



# start the data loop
for flow in config['hplc_flowrate']:

    set_liquid(flow) # set the hplc flowrate
    if config['wait_time']:
        print('Letting the liquid equilibrate... waiting 10 seconds. \n', flush=True)
        time.sleep(10)

    data = {'heater_status': [],
            'calib_high': [],
            'calib_low': [],
            'data_high': [],
            'sdev_high': [],
            'rec_time_high': [],
            'data_low': [],
            'sdev_low': [],
            'rec_time_low': []
            }

    for h in config['heater_status']:
        print(f"Heater Status: {h}", flush=True)
        for i in [0, 1]:
            for j in [0, 1]:
                # set the sensirion calibration field
                sensirion_high.calibration = i
                sensirion_low.calibration = j
                print(f"\t\t Calibration fields: (high: {sensirion_high.calibration}, low:{sensirion_low.calibration})",
                        flush=True)

                data['heater_status'].append(h)
                data['calib_high'].append(sensirion_high.calibration)
                data['calib_low'].append(sensirion_low.calibration)

                dat_high, dat_low = loop_record(heater=h, rec=range(config['rec_n_datapoints']), avg=config['avg_n_points'])
                data['data_high'].append(dat_high['avg_data'])
                data['sdev_high'].append(dat_high['sdev'])
                data['rec_time_high'].append(dat_high['rec_time'])
                data['data_low'].append(dat_low['avg_data'])
                data['sdev_low'].append(dat_low['sdev'])
                data['rec_time_low'].append(dat_low['rec_time'])

        if h == 'off':
            print("Letting the heater warm up... waiting 5 seconds.", flush=True)
            time.sleep(5)


    ##############
    hplc.stop() # stop the hplc pump after the data recording is done
    ##############


    if len(config['heater_status']) == 1:
        x_cells, y_cells = 2, 2
    elif len(config['heater_status']) == 2:
        x_cells, y_cells = 4, 2


    fig, ax = plt.subplots(x_cells, y_cells, figsize=(16, 10), tight_layout=True, sharex=True)
    for h in range(y_cells):
        for i in range(x_cells):

            if x_cells == 2:
                ax[i][h].set_title(f"Heater Status: Default -- Calibration: (high: {data['calib_high'][i+x_cells*h]}, low:{data['calib_low'][i+x_cells*h]})")

            elif x_cells == 4:
                if h == 1:
                    ax[i][h].set_title(f"Heater Status: On -- Calibration: (high: {data['calib_high'][i+x_cells*h]}, low:{data['calib_low'][i+x_cells*h]})")

                elif h == 0:
                    ax[i][h].set_title(f"Heater Status: Off -- Calibration: (high: {data['calib_high'][i+x_cells*h]}, low:{data['calib_low'][i+x_cells*h]})")

                else:
                    ax[i][h].set_title(f"Calibration: (high: {data['calib_high'][i+4*h]}, low:{data['calib_low'][i+4*h]})")

            ax[i][h].errorbar(x=data['rec_time_high'][i+x_cells*h], y=data['data_high'][i+x_cells*h], 
                            yerr=data['sdev_high'][i+x_cells*h],
                            ecolor='blue',
                            linestyle='none', alpha=0.3,
                            label='High Flow Error')
            ax[i][h].plot(data['rec_time_high'][i+x_cells*h], data['data_high'][i+x_cells*h], '-b', label='Sensirion High Flow Readings')

            ax[i][h].errorbar(x=data['rec_time_low'][i+x_cells*h], y=data['data_low'][i+x_cells*h], 
                            yerr=data['sdev_low'][i+x_cells*h],
                            ecolor='red',
                            linestyle='none', alpha=0.3,
                            label='Low Flow Error')
            ax[i][h].plot(data['rec_time_low'][i+x_cells*h], data['data_low'][i+x_cells*h], '-r', label='Sensirion Low Flow Readings')



    # make hplc horizontal line
    for i in range(y_cells):
        for j in range(x_cells):
            ax[i][j].hlines(y=flow, 
                    xmin=0, xmax=np.max(data['rec_time_low'][-1]),
                    colors='black', linestyle='dashed', alpha=0.5, label=f'HPLC Flow Rate = {flow} ul/min')

    ax[0][1].legend()
    fig.supylabel("Flow Rate [uL/min]")
    fig.supxlabel("Time [s]")
    fig.suptitle("Sensirion Low/High Flow Calibration and Heater Test", fontsize=18)

    if config['savefig']:
        if len(config['heater_status']) == 2:
            heat = 'onoff'
        else:
            heat = 'default'
        plt.savefig(f"sensirion_tests_{flow}ulpermin_{config['date']}_heater-{heat}.png")
    else:
        plt.show()


















