#-------------------------------------------------------------------------------
# Name:        example_3_continuous_measurement
# Purpose:     Example code (3) for the RS485 Sensor Cable with the SHDLC
#              Protocol
#
#              Switch on the heater of the sensor
#              Set the measurement resolution of the sensor to 16 bit
#              Clear the measurement buffer
#              Starts a continuous measurement with 100 ms period
#              Read 10 measured values from the sensor
#              Stop the continuous measurement
#              Switch off the heater of the sensor
#
# Author:      nsaratz
#
# Created:     04. 10. 2012
# Copyright:   (c) Sensirion AG 2012
# Licence:     All Rights Reserved
#-------------------------------------------------------------------------------
#!/usr/bin/env python


# Serial port package for Python
import serial

# initialize a global variable for the serial connection
global ser

# enable/disable explain-mode (details of command generation printed to stdout)
explainmode=True

# ##############################################################################
# SUB ROUTINES (functions)


def compute_SHDLC_checksum(listofbytes):
    """
    compute the SHDLC checksum of a list of bytes
    """
    # sum up all bytes
    tmpchecksum = sum(listofbytes)

    # take least significant byte
    tmpchecksum = tmpchecksum & 0xff

    # invert (bit-wise XOR with 0xff)
    tmpchecksum = 0xff ^ tmpchecksum

    return tmpchecksum

def byte_stuff(listofbytes):
        """
        Perform byte stuffing on 'listofbytes',
        i.e. replace special characters as follows:
        0x7e --> 0x7d, 0x5e
        0x7d --> 0x7d, 0x5d
        0x11 --> 0x7d, 0x31
        0x13 --> 0x7d, 0x33
        """
        i=0
        while i<len(listofbytes):
            if listofbytes[i]==0x7e:
                listofbytes[i]=0x7d
                listofbytes.insert(i+1,0x5e)
                i+=1
            elif listofbytes[i]==0x7d:
                listofbytes[i]=0x7d
                listofbytes.insert(i+1,0x5d)
                i+=1
            elif listofbytes[i]==0x11:
                listofbytes[i]=0x7d
                listofbytes.insert(i+1,0x31)
                i+=1
            elif listofbytes[i]==0x13:
                listofbytes[i]=0x7d
                listofbytes.insert(i+1,0x33)
                i+=1
            i+=1
        return listofbytes


def make_and_send_SHDLC_command(address, commandID, data):
    """
    sends command 'commandID' with data 'data' to device on address 'address'.
    address: number between 0 and 254 (address 255 is reserved for broad-cast)
    commandID: byte
    data: list of bytes
    """
    datalength = len(data)
    # compose command
    command = [address, commandID, datalength] + data
    if explainmode: print('command before checksum:',command)

    # compute checksum
    command.append(compute_SHDLC_checksum(command))
    if explainmode: print('command with checksum:',command)

    # do byte stuffing
    command = byte_stuff(command)
    if explainmode: print('command with byte stuffing:', command)

    # add start byte and stop byte
    command = [0x7e] + command + [0x7e]
    if explainmode: print('command with start stop byte:',command)

    # convert list of numbers to bytearray
    command = bytearray(command)

    # send command to the device
    ser.write(command)

def read_SHDLC_response():
    """
    reads the response from the SHDLC device and
    returns the data as list of bytes
    """
    response = []
    res = ''

    # Iterate read until res is empty or stop byte received
    firstbyte=True
    while True:
        res = ser.read(1)
        if not res:
            break
        elif firstbyte or (ord(res) != 0x7e):
            firstbyte = False
            response.append(ord(res))
        else:
            response.append(ord(res))
            break

    # print response as it comes from the device
    if explainmode: print('response from sensor:',response)

    # remove first element (the start byte) from the response
    response.pop(0)
    # remove the last element (the stop byte) form the response
    response.pop(-1)

    # print response without start- and stop-bytes
    if explainmode: print('response w/o start/stop:',response)


    # Check for bytes that are stuffed
    i=0
    #loop through response list
    while i<len(response):
        if response[i] == 0x7D:
            # 0x7d marks stuffed bytes. see SHDLC documentation
            if response[i+1] == 0x5E:
                response[i] = 0x7E
            elif response[i+1] == 0x5D:
                response[i] = 0x7D
            elif response[i+1] == 0x31:
                response[i] = 0x11
            elif response[i+1] == 0x33:
                response[i] = 0x13
            response.pop(i+1)
        i+=1

    if explainmode: print('response w/o byte stuffing:',response)

    # confirm check sum is correct

    #remove last element from response-list and store it in variable checksum
    checksum = response.pop(-1)

    # compare to received checksum
    if explainmode: print('checksum correct?',checksum == compute_SHDLC_checksum(response))

    if explainmode: print('response without checksum', response)
    if explainmode: print('address:',response[0])
    if explainmode: print('command:',hex(response[1]))
    if explainmode: print('status:', response[2])
    if explainmode: print('data length:', response[3])
    if explainmode: print('data:', response[4:])

    # return only the 'data' portion of the response
    return response[4:]

# END OF SUBROUTINES
# ##############################################################################



# ##############################################################################
# Main Sequence


# ------------------------------------------------------------------------------
# open the serial connection
# ------------------------------------------------------------------------------

# The COM port number depends on the hardware settings
# (python starts at 0, windows at 1, so use -1)
windowsCOMport = 4
portNumber = windowsCOMport - 1

# Open Serial Port (for example USB to RS485 Adapter)
# Settings according to SHDLC documentation
ser = serial.Serial(
    port = 'COM4',          #number of device, numbering starts at
                                #zero.
    baudrate=115200,            #baudrate
    bytesize=serial.EIGHTBITS,  #number of databits
    parity=serial.PARITY_NONE,  #enable parity checking
    stopbits=serial.STOPBITS_ONE,  #number of stopbits
    timeout=1,                  #set a timeout value (example only because reset
                                #takes longer)
    xonxoff=0,                  #disable software flow control
    rtscts=0,                   #disable RTS/CTS flow control
)


# specify the address of the RS485 adapter cable
ADDRESS = 0



# ------------------------------------------------------------------------------
# switch on heater
# ------------------------------------------------------------------------------
make_and_send_SHDLC_command(ADDRESS, 0x42, [1])
read_SHDLC_response()

# ------------------------------------------------------------------------------
# set resolution
# ------------------------------------------------------------------------------
# set resolution to 16 bit
make_and_send_SHDLC_command(ADDRESS, 0x41, [16])
read_SHDLC_response()

# ------------------------------------------------------------------------------
# clear measurement buffer
# ------------------------------------------------------------------------------
# discard any old measurements which may still be in the buffer
make_and_send_SHDLC_command(ADDRESS, 0x36, [2])
read_SHDLC_response()

# ------------------------------------------------------------------------------
# start continuous measurement
# ------------------------------------------------------------------------------
# start continuous measurement with measure interval 100 ms
# NOTE: the measure interval must be supplied as 16 bit integer, i.e. two bytes
# (most significant, least significant) must be sent.
make_and_send_SHDLC_command(ADDRESS, 0x33, [1,0])
read_SHDLC_response()

# ------------------------------------------------------------------------------
# read the measured data
# ------------------------------------------------------------------------------

# create an empty list to store all data points
all_data=[]

# loop until 10 data points have been acquired
while len(all_data)<10:

    #create empty data list
    data=[]
    # loop until data is available and read.
    while len(data)<2:
        # get measurement buffer
        make_and_send_SHDLC_command(ADDRESS, 0x36, [])
        data = read_SHDLC_response()

    # loop through the received data
    while len(data)>0:
        # combine the first two data bytes into one 16bit data value
        value_from_sensor = data[0]*256 + data[1]
        if explainmode: print('value from sensor:',value_from_sensor)

        #compute two's complement (handle negative numbers!)
        if value_from_sensor >= 2**15:  # 2**15 = 32768
            value_from_sensor = value_from_sensor-2**16     # 2**16 = 65536

        if explainmode: print('value with 2s comlement:',value_from_sensor)

        # apply the scale factor to get real flow units. The scale factor is 13.0 for
        # the SLQ-QT105 flow meter
        if explainmode: print('value with scale factor:',value_from_sensor / 13.0)

        # append the datapoint to 'all_data'
        all_data.append(value_from_sensor/13.0)
        # remove the first two data bytes from the 'data' list
        data.pop(0)
        data.pop(0)

# ------------------------------------------------------------------------------
# stop continuous measurement
# ------------------------------------------------------------------------------
make_and_send_SHDLC_command(ADDRESS, 0x34, [])
read_SHDLC_response()

# ------------------------------------------------------------------------------
# switch off heater
# ------------------------------------------------------------------------------
make_and_send_SHDLC_command(ADDRESS, 0x42, [0])
read_SHDLC_response()



print('All Data:', all_data)

