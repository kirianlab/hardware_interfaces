import tempfile, time, os, json, datetime, sys
from hardware.sensirion import Sensirion
from shimadzu_pump import shimadzu_driver as hplc
import pyqtgraph as pg
import numpy as np
import pylab as plt


config = {'sensirion_highflow': 'FTSTT5NA',
            'sensirion_lowflow': 'FTXEF0AHA',
            'low_flow_calibration': 1, # int, 0 or 1
            'high_flow_calibration': 0,
            'heater_status': 'off', # str, 'on' or 'off'
            'rec_n_datapoints': 1,
            'avg_n_points': 10,
            'hplc_flowrate': 20,
            'runtime': 1200, # seconds, = 20 minutes
            'wait_time_between_collections': 15 # seconds
            }

nl_to_ul = 1000


# setup the hplc pump
hplc = hplc.ShimadzuCbm20('192.168.200.99')
hplc.login('raklab', 'sky2Blue')

# setup the sensirion
sensirion_high = Sensirion(serial_number=config['sensirion_highflow'], 
                            calibration=config['high_flow_calibration'])
sensirion_low = Sensirion(serial_number=config['sensirion_lowflow'], 
                            calibration=config['low_flow_calibration'])

print("Sensirions setup!", flush=True)

# clear anything in the buffer just in case
sensirion_high.clear_buffer
sensirion_low.clear_buffer

print(f"High Flow:  {sensirion_high.units}", flush=True)
print(f"Low Flow:  {sensirion_low.units}", flush=True)


def set_liquid(flow, start_pump=True):  # This is ul/min
    '''set the hplc flow rate
    '''
    hplc.set('flow', flow*1e-3)
    hplc.start()

# start the timer
tik = time.time()
# start the pump
set_liquid(config['hplc_flowrate'])

# wait a few seconds before recordin sensirion data
time.sleep(2)

highflow = []
lowflow = []
times = []

sensirion_high.set("start_continuous_measure", 1,0)
sensirion_low.set("start_continuous_measure", 1,0)
while time.time() - tik < config['runtime']:
    # collect the time stamp
    times.append(float(f"{time.time() - tik:0.3f}"))
    # collect high and low flow sensor values
    x = sensirion_high.rec_n_points_continuous(n_points=config['avg_n_points'], timeit=False, start_continuous=False)
    y = sensirion_low.rec_n_points_continuous(n_points=config['avg_n_points'], timeit=False, start_continuous=False)
    highflow.append(np.mean(x))
    lowflow.append(np.mean(y) / nl_to_ul)
    print(f'Time ellapsed: {times[-1]} seconds', flush=True)
    # wait 5 seconds
    time.sleep(config['wait_time_between_collections'])


hplc.stop()

sensirion_high.set("stop_continuous_measure")
sensirion_low.set("stop_continuous_measure")


fig, ax = plt.subplots(1, 1, figsize=(12, 10), sharey=True)
ax.errorbar(x=times, y=highflow, 
                yerr=np.std(highflow),
                ecolor='blue',
                linestyle='none', alpha=0.3,
                label='High Flow Error')
ax.plot(times, highflow, '-b', label='Sensirion High Flow Readings')

ax.errorbar(x=times, y=lowflow, 
                yerr=np.std(lowflow),
                ecolor='red',
                linestyle='none', alpha=0.3,
                label=' Low Flow Error')
ax.plot(times, lowflow, '-r', label='Sensirion Low Flow Readings')

ax.hlines(y=config['hplc_flowrate'], 
            xmin=times[0], xmax=times[-1],
            colors='black', linestyle='dashed', alpha=0.5, label='HPLC Flow Rate')

ax.set_xlabel('Time [seconds]')
ax.set_ylabel(f'Flow Rate [{sensirion_high.units}]')
ax.legend()
ax.set_title('Sensirion Flow Rate / HPLC Calibration Test')

plt.show()


















