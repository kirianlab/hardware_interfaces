# Starting the HPLC - Normal Method

To start the HPLC using the Schimadzu program, follow the method listed below.

1. run `start_hplc.sh`

2. Wait for the Internet Explorer window to open 

3.  Under 'System Name (Click to Login)', click the serial name for the HPLC. For the Kirian Lab system, this is `L20225751683`. 

4. A new window should open prompting the user to input the username and password of the device, this is the standard Kirian Lab login info. Push `Login`.

5. A new window should open with the control system.

To change flow rates, change the value under the `pump` tab. 

- ***NOTE:*** You need to push `Tab` on your keyboard to execute the new flow rate! Don't ask why, `Enter` does not work.
- ***NOTE:*** The IP address in the `start_hplc.sh` script is fixed to the microjet testing station computer. If this is moved to a new computer, please reconfigure the IP (good luck). 