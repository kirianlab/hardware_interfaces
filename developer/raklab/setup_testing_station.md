Configure passwordless login to xfel1.  On the test station computer gitbash terminal, set up the xfel1 host by adding the following to ~/.ssh/config:

```
Host server1
     HostName server1.cyberciti.biz
     User nixcraft
     Port 4242
     IdentityFile /nfs/shared/users/nixcraft/keys/server1/id_rsa
```


