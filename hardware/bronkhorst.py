r"""
Read information from the Bronkhorst mass flow sensors. See the README file in this
same directory for usage information.

Author: Sahba Zaare
Editors: Konstantinos Karpos, Richard Kirian
"""
import os
import sys
import time
import serial
import logging
import logging.handlers as handlers
import threading
import json
import numpy as np
from .utils import time_tools
from .utils import zmq_tools
from .utils import loggers
# from .utils import serial_communication_tools as sct
import pyqtgraph as pg

debug_default = 0


class BronkhorstDevice:
    r""" Class for maintaining serial connection and communications with Bronkhorst gas flowmeter. """
    debug_threshold = 0
    config = None
    serial_connection = None
    thread = None
    current_flow = -1
    current_smoothed_flow = None
    current_time = -1
    last_time = 0
    buffering = False
    buffer_idx = -1
    buffer_t0 = 0
    _current_setpoint = 0
    _requested_setpoint = 0
    units = 'mg/min'

    def __init__(self, config=None, debug=debug_default):
        r"""
        Arguments:
            config (dict): See make_config function to create a default configuration.
            debug (int): Level of verbosity for debug messages.  0 means silent, larger numbers are more verbose.
        """
        self._dbgmsg('__init__')
        self.debug_threshold = debug
        if config is None:
            config = make_config()
        # All user-configurable settings go here:
        # ======================================================================
        self.com_port = config.get('com_port', 'COM1')
        self.max_flow = config.get('max_flow', 100)
        self.polling_frequency = config.get('polling_frequency', 1)
        self.log_directory = config['log_file_path']+'/bronkhorst_EL-Flow_'+self.com_port+'/'
        os.makedirs(os.path.dirname(self.log_directory), exist_ok=True)
        self.status_file_path = self.log_directory + 'status.json'
        
        self.baud_rate = config.get('baud_rate', 38400)
        setpoint = config.get('setpoint', None)
        # ======================================================================
        self.establish_connection(self.com_port, self.baud_rate)
        if setpoint is not None:
            self.set_setpoint(setpoint)
        self.buffer_interval = 1 / self.polling_frequency
        self.buffer_length = int(12 * 3600 * self.buffer_interval)
        self.buffer_flow = np.empty(self.buffer_length, dtype=float)
        self.buffer_setpoint = np.empty(self.buffer_length, dtype=float)
        self.buffer_times = np.empty(self.buffer_length, dtype=float)
        self.log_file_path = None
        self.datalog = None
        self.setup_datalog()
        self.start_buffering()

    def setup_datalog(self):
        filepath = os.path.dirname(self.status_file_path) + '/data.log'
        data_names = ('epoch_time', 'flow_rate')
        data_formats = (r'%f', r'%f')
        self.datalog = loggers.DataLogger(filepath, data_names=data_names, data_formats=data_formats)

    def log_datapoint(self):
        self.datalog.log_data(time.time(), self.current_flow)

    def _dbgmsg(self, *args, level=1, **kwargs):
        r""" For debugging purposes. """
        if level <= self.debug_threshold:
            print('BronkhorstDevice:', *args, **kwargs)

    def get_setpoint(self):
        r""" This is the method to use for a client to get the flow rate setpoint.  The actual communication
        with the device occurs in the buffer thread. """
        self._dbgmsg('get_setpoint')
        return self.current_time, self._current_setpoint

    def set_flow_rate(self, flow):
        r""" Same as set_setpoint. """
        self._dbgmsg('set_flow_rate')
        self.set_setpoint(flow)

    def _get_setpoint(self):
        r""" Communicates directly with the device to get the current flow rate setpoint.  Should only be used by
        internal buffer thread. """
        self._dbgmsg('get_setpoint', level=2)
        t = time.time()
        val = self.comm(':06800401210121\r\n')
        self._dbgmsg('get_setpoint:val', val, '.', level=2)
        if len(val) < 12:
            val = None
        else:
            val = int(val[11:], 16)
            val = (val / 32000.0) * self.max_flow
            val = float("{:.1f}".format(val))
        return t, val

    def set_setpoint(self, val):
        r""" This is the method to use for a client to set the flow rate setpoint.  The actual communication
        with the device occurs in the buffer thread. """
        self._dbgmsg('set_setpoint: Requested value:', val)
        self._requested_setpoint = val

    def _set_setpoint(self, val):
        r""" Communicates directly with the device to set the flow rate setpoint.  Should only be used by internal
        buffer thread. """
        self._dbgmsg('_set_setpoint: Requested value:', val)
        self._requested_setpoint = val
        self._current_setpoint = val
        if val > 0:
            val = 32000 * (1.0 * val / self.max_flow)
            val = hex(int(val))
            val = val.upper()
            val = val[2:].rstrip('L')
        else:
            val = '0000'
        deficit = 4 - len(val)
        if deficit > 0:
            val = '0' * deficit + val
        # if len(val) == 3:
        # val = '0' + val
        # resp = \
        self.comm(':0680010121' + val + '\r\n')
        # check = resp[5:].strip()
        # if check == '000005':
        #     resp = 'ok'
        # else:
        #     resp = 'error'
        # self.get_setpoint()

    # @property
    # def reading(self):
    #     r""" FIXME: Documentation. """
    #     flow = None  # FIXME: If the flow rate is junk, return None.
    #     error = 0
    #     while error < 10:
    #         val = self.comm(':06800401210120\r\n')  # Read flow
    #         try:
    #             val = val[-6:]
    #             num = int(val, 16)
    #             flow = (1.0 * num / 32000) * self.max_flow
    #             break
    #         except ValueError:
    #             error = error + 1
    #     flow = float("{:.2f}".format(flow))
    #     return flow

    # @property
    # def units(self):  # TODO
    #     # TODO: Figure out how to get units from the device.
    #     # read_capacity = ':1A8004F1EC7163006D71660001AE0120CF014DF0017F077101710A\r\n'
    #     # response = self.comm(read_capacity)
    #     # response = response[77:-26]
    #     # response = response.decode()   # "hex" ?
    #     # return str(response)
    #     return 'mg/min'

    # @property
    # def sensor_range(self):  # TODO
    #     r""" FIXME: Documentation. """
    #     return [0, self.max_flow]

    def get_flow_rate(self):
        return self.current_time, self.current_flow

    def _get_flow_rate(self):
        self._dbgmsg('get_flow_rate', level=2)
        flow = None  # FIXME: If the flow rate is junk, return None.
        error = 0
        while error < 10:
            self._dbgmsg('get_flow_rate:reading', level=2)
            val = self.comm(':06800401210120\r\n')  # Read flow
            try:
                val = val[-6:]
                num = int(val, 16)
                flow = (1.0 * num / 32000) * self.max_flow
                break
            except ValueError:
                error = error + 1
        flow = float("{:.2f}".format(flow))
        return time.time(), flow

    def establish_connection(self, port, baud_rate):
        r""" Create serial connection.  Assumes device can be controlled (i.e. has a configured setpoint). """
        self._dbgmsg('establish_connection')
        try:
            self.serial_connection = serial.Serial(port, baud_rate)
            # time.sleep(0.2)
            # # Verify serial port actually belongs to this sensor
            # if self.get_flow_rate() is None:
            #     raise ValueError
        except serial.SerialTimeoutException as e:
            print("!!! Serial Comm Timeout:\n\t", e)
            sys.exit()
        except serial.SerialException as e:
            print("!!! Bad Serial Port:\n\t", e)
            sys.exit()
        except ValueError as e:
            msg = "!!! {sensor}: Serial comm established, but sensor is either \n \
                       1) unresponsive or \n \
                       2) on a different serial port than {port} \n \
                       Error: {error}"
            print(msg.format(sensor="Bronkhorst", port=port, error=repr(e)))
            sys.exit()
        except Exception as e:
            print(repr(e))
            sys.exit()
        self.comm(':058001010412\r\n')  # Set to RS232 so we can specify setpoint. TODO: Handle devices without control.

    def comm(self, command):
        r""" FIXME: Documentation. """
        self.serial_connection.write(command.encode())
        time.sleep(0.1)  # FIXME: Is this sleep necessary?
        return_string = self.serial_connection.read(self.serial_connection.inWaiting())
        return return_string

    def read_counter_value(self):
        r""" FIXME: Documentation. """
        read_counter = ':06030401210141\r\n'
        response = self.comm(read_counter)
        return str(response)

    # def set_control_mode(self):
    #     r""" FIXME: Documentation. """
    #     set_control = ':058001010412\r\n'  # set to RS232 so we can specify setpoint
    #     response = self.comm(set_control)
    #     return str(response)

    # def read_serial(self):
    #     r""" FIXME: Documentation. """
    #     read_serial = ':1A8004F1EC7163006D71660001AE0120CF014DF0017F077101710A\r\n'
    #     error = 0
    #     response = ''
    #     while error < 10:
    #         response = self.comm(read_serial)
    #         response = response[13:-84]
    #         try:
    #             response = response.decode('hex')
    #         except TypeError:
    #             response = ''
    #         if response == '':
    #             error = error + 1
    #         else:
    #             error = 10
    #     return str(response)

    # def read_capacity(self):  # TODO
    #     r""" FIXME: Documentation. """
    #     # TODO: read capacity during initialization to set max_flow automatically
    #     read_capacity = ':1A8004F1EC7163006D71660001AE0120CF014DF0017F077101710A\r\n'
    #     response = self.comm(read_capacity)
    #     response = response[67:-43]
    #     # response = response.decode('hex')
    #     return str(response)

    def settle_on_setpoint(self, setpoint, max_readings=100, delay=0.5, thresh=None):
        r""" FIXME: Documentation. """
        self._dbgmsg('settle_on_setpoint', level=2)
        if thresh is None:
            thresh = 20
        sep = "\n" + "=" * 20 + "\n"
        response_time = -1
        reading = -1
        last_reading = -1
        stagnant = 1
        self._dbgmsg("setpoint set to", setpoint)
        # self.setpoint = setpoint
        for i in range(1, max_readings):
            if stagnant >= 5:
                self._dbgmsg(sep, "Stagnant Gas Flow: ", reading, sep)
                break
            _, reading = self.get_flow_rate()
            self._dbgmsg(i, "\t", reading)
            if abs(reading - last_reading) < thresh:
                stagnant += 1
            if abs(reading - setpoint) < thresh:
                response_time = int(i) * delay
                self._dbgmsg(sep, "Response Time: ", response_time, "seconds", sep)
                break
            last_reading = reading
            time.sleep(delay)
        return reading, response_time

    # def data(self, avg_n_points: int = 10, setpoint: float = None, wait_time: int = None):
    #     r"""
    #         Gathers N data points and averages them.
    #
    #         Arguments:
    #             avg_n_points (int): The total number of data points to gather and average
    #             setpoint (float): optional, if given will set the setpoint to the desired value.
    #     """
    #     data = {}
    #     if setpoint:
    #         self.setpoint = setpoint
    #         print('Waiting 5 seconds for gas to stabilize')
    #         time.sleep(5)
    #     tik, tik_iso = get_datetime()
    #     dat = self.rec_n_points(n_points=avg_n_points, wait_time=wait_time)
    #     tok, tok_iso = get_datetime()
    #     # FIXME: Get rid of reading later, check if it crashes without it
    #     data['average_reading'] = np.mean(dat)
    #     data['readings'] = dat
    #     data['reading'] = data['average_reading']
    #     data['n_points'] = avg_n_points
    #     data['variance'] = np.var(dat)
    #     data['units'] = self.units
    #     data['initial_reading_time_epoch'] = tik
    #     data['initial_reading_time_human'] = tik_iso
    #     data['final_reading_time_epoch'] = tok
    #     data['final_reading_time_human'] = tok_iso
    #     data['setpoint'] = self.setpoint
    #     data['deviation_from_setpoint'] = np.abs(data['average_reading'] - self.setpoint)
    #     return data

    def start_buffering(self):
        self._dbgmsg('start_buffering')
        self.thread = threading.Thread(target=self.buffer_thread)
        self.buffering = True
        self.thread.start()

    def stop_buffering(self):
        self._dbgmsg('stop_buffering')
        self.buffering = False
        if self.thread is not None:
            time.sleep(2 * self.buffer_interval)
            self.thread.join()

    def buffer_thread(self):
        self._dbgmsg('buffer_thread', level=2)
        self.buffer_t0 = time.time()
        status_time = np.floor(time.time())
        while self.buffering:
            t = time.time()
            if (t - self.last_time) > self.buffer_interval:
                self._dbgmsg('buffer_thread: Updating')
                _, s = self._get_setpoint()
                if self._requested_setpoint != s:
                    self._set_setpoint(self._requested_setpoint)
                t, val = self._get_flow_rate()
                idx = self.buffer_idx + 1
                idx %= self.buffer_length
                self.buffer_flow[idx] = val
                self.buffer_times[idx] = t
                self.buffer_idx = idx
                self.current_flow = val
                self.log_datapoint()
                if idx == 0:
                    self.current_smoothed_flow = val
                else:
                    w = 0.2
                    self.current_smoothed_flow = self.current_smoothed_flow * (1 - w) + val * w
                self.current_time = t
                self._dbgmsg('buffer_thread:', self.current_time - self.buffer_t0, self.current_flow, level=2)
                self.last_time = t
                if np.abs(status_time - t) > 1:  # Update status file at 1 Hz
                    status_time = np.floor(t)
                    self.write_status_json_text()
            time.sleep(time.time() - self.last_time)

    def write_status_json_text(self):
        self._dbgmsg('write_status_json_text', level=2)
        filepath = self.status_file_path
        data = dict()
        data = time_tools.append_timestamps_to_dict(data)
        data['reading'] = self.get_flow_rate()[1]
        data['units'] = self.units
        with open(filepath, 'w') as f:
            json.dump(data, f, indent=4)

    def disconnect(self):
        self.stop_buffering()

    def __del__(self):
        self._dbgmsg('__del__')
        self.stop_buffering()


class BronkhorstDeviceDummy:
    r""" Dummy class for developing/testing code without physical device. """
    setpoint = 0

    def __init__(self, *args, **kwargs):
        pass

    def set_setpoint(self, val):
        self.setpoint = val

    def get_flow_rate(self):
        return time.time(), self.setpoint + 0.5 + np.random.rand()

    def get_setpoint(self):
        return time.time(), self.setpoint

    def disconnect(self):
        pass


def make_config(zmq_port=5556, dummy=False):
    default_config = {
        'device_class': BronkhorstDevice,
        'com_port': 'COM3',
        'polling_frequency': 100,
        'log_file_path': 'D:/data/microjets/logs',
        'logging': True,
        'zmq_client_address': 'tcp://localhost:PORT',
        'zmq_server_address': 'tcp://*:PORT',
        'zmq_daemon_pid_file': '/tmp/zmq_daemon_PORT.pid'
    }
    if dummy:
        default_config['device_class'] = BronkhorstDeviceDummy
    default_config['zmq_client_address'] = default_config['zmq_client_address'].replace('PORT', zmq_port.__str__())
    default_config['zmq_server_address'] = default_config['zmq_server_address'].replace('PORT', zmq_port.__str__())
    default_config['zmq_daemon_pid_file'] = default_config['zmq_daemon_pid_file'].replace('PORT', zmq_port.__str__())
    return default_config


# =================================
# =================================


# get bronkhorst com ports:

# def get_bronkhorst_serial_number(com_port:str) -> str:

#     r'''
#         open communication to a bronkhorst device and return the serial number
#         this function is specific to bronkhorst mass flow controllers.

#         Reason for this: 
#             rs232 usb converters mask the serial number of the device, pyserial
#             can't find it. So, we need to loop through all pieces of hardware 
#             and get a best guess of which com port is which.
#     '''

#     print(com_port, type(com_port))

#     # open the connection 
#     ser = serial.Serial(com_port, baudrate=38400)

#     print(ser)


#     # request serial number info from device (bronkhorst specific)

#     x = ser.write(b':1A8004F1EC7163006D71660001AE0120CF014DF0017F077101710A\r\n')

#     print(x)

#     # get the response
#     return_string = ser.read(ser.in_waiting)#.decode('UTF-8')

#     print(return_string.decode('UTF-8') + 'hi')

#     # do some magic
#     # val = return_string[13:-84]

#     # # convert from hex to human readable
#     # response = bytes.fromhex(val)


#     ser.close()

# return response.decode()


# def get_bronkhorst_port_number(device_nickname:str, devices_df, config_df) -> str:
#     r"""
#     FIXME: Documentation.
#     """
#     devices = sct.identify_devices()
#     print(devices.keys())
# empty_ser_num = devices.loc[devices['serial_number'] == '']
# for e in empty_ser_num['com_port']:
#     # open the connection
#     ser = serial.Serial(e, baudrate=38400)
#     # request serial number info from device (bronkhorst specific)
#     x = ser.write(b':1A8004F1EC7163006D71660001AE0120CF014DF0017F077101710A\r\n')
#     time.sleep(0.2) # needed to wait for the physical communication between devices
#     # get the response
#     return_string = ser.read(ser.in_waiting).decode('UTF-8')
#     # do some magic
#     val = return_string[13:-84]
#     # convert from hex to human readable
#     response = bytes.fromhex(val).decode()
#     ser.close()
#     x = config_df.loc[(config_df['serial_number'] == response) &
#                        (config_df['device_nickname'] == device_nickname)].reset_index(drop=True)
#     if len(x) == 1:
#         return e


class BronkhorstClient(zmq_tools.GenericZMQClient):
    r""" ZMQ client for Bronkhorst flowmeters.  See GenericZMQClient superclass for more details and keyword
    arguments. """

    def __init__(self, config=None, **kwargs):
        r"""
        Arguments:
            config (dict): Configuration parameters.  Use make_config() to get a starting point.
            start_server (bool): Set to True if you want to start a server upon instantiation of the client.
        """
        if config is None:
            raise ValueError('Your config dictionary is None.  Use "make_config" and modify as appropriate.')
        super().__init__(config=config, **kwargs)


    def get_flow_rate(self):
        r""" Method is passed to Bronkhorst device class.  See Bronkhorst class documentation. """
        return self.get(method='get_flow_rate')

    def get_setpoint(self):
        r""" Method is passed to Bronkhorst device class.  See Bronkhorst class documentation. """
        return self.get(method='get_setpoint')

    def set_flow_rate(self, *args, **kwargs):
        r""" Method is passed to Bronkhorst device class.  See Bronkhorst class documentation. """
        return self.get(method='set_flow_rate', args=args, kwargs=kwargs)


class BronkhorstServer(zmq_tools.GenericZMQServer):
    r""" ZMQ server for Bronkhorst flowmeters.  See GenericZMQServer superclass for more details and keyword
    arguments. """

    def __init__(self, config=None, **kwargs):
        r"""
        Arguments:
            config (dict): Configuration parameters.  Use make_config() to get a starting point.
            threaded (bool): Start the server in a thread to avoid blocking.
        """
        if config is None:
            raise ValueError('Your config dictionary is None.  Use "make_config" and modify as appropriate.')
        super().__init__(config=config, **kwargs)


class BronkhorstWidget(pg.Qt.QtWidgets.QWidget):
    r""" A graphical interface for the Bronkhorst class."""

    debug_threshold = 0
    setpoint_modified = False

    def __init__(self, config, debug=debug_default):
        r"""
        Arguments:
            config (dict): Configurations.  TODO: Document.
        """
        self.debug_threshold=debug
        self._dbgmsg('Initializing.')
        app = pg.mkQApp()
        app.aboutToQuit.connect(self.quit)
        self.app = app
        super().__init__()
        self.config = config
        self.buffer_idx = 0
        self.buffer_length = int(1e6)
        self.buffer_update_interval = 0.1
        self.plot_update_interval = 0.1
        self.is_initialized = False
        self.is_paused = False
        self.do_update_plot = True
        self.ring_buffer_y = np.zeros(self.buffer_length)
        self.ring_buffer_t = np.zeros(self.buffer_length)
        self.client = BronkhorstClient(config=config, start_server=True)
        self._setup_interface()

    def _dbgmsg(self, *args, level=1, **kwargs):
        if level <= self.debug_threshold:
            print('BronkhorstWidget:', *args, **kwargs)

    def _setup_interface(self):
        main_layout = pg.Qt.QtWidgets.QVBoxLayout()
        # Setup plot window
        plt_layout = pg.Qt.QtWidgets.QHBoxLayout()
        plot = pg.PlotWidget()
        plot.addLegend()
        plot.setLabel('bottom', 'Relative Time (s)')
        plot.setTitle('Bronkhorst Gas Flowmeter')
        pg.setConfigOptions(antialias=True)
        curve = plot.plot(name='Flow Rate (mg/min)', pen=None, symbol='o', symbolPen=None, symbolBrush='g')
        # curve2 = plot.plot(pen=None, symbol='o', symbolPen=None, symbolBrush='r')
        plt_layout.addWidget(plot)
        # plt_layout.addWidget(plot2)
        main_layout.addItem(plt_layout)
        # Setup device status widget (row of widgets)
        btn_layout = pg.Qt.QtWidgets.QHBoxLayout()
        # Flow rate status
        btn_layout.addWidget(pg.Qt.QtWidgets.QLabel('Flow rate:'))
        self.flow_label = pg.Qt.QtWidgets.QLabel('0')
        btn_layout.addWidget(self.flow_label)
        # Setpoint status
        btn_layout.addWidget(pg.Qt.QtWidgets.QLabel('Flow rate setpoint:'))
        self.flow_setpoint_label = pg.Qt.QtWidgets.QLabel('0')
        btn_layout.addWidget(self.flow_setpoint_label)
        main_layout.addItem(btn_layout)
        # Setup control widget (row of widgets)
        ctl_layout = pg.Qt.QtWidgets.QHBoxLayout()
        self.set_flow_setpoint_btn = pg.Qt.QtWidgets.QPushButton('Set flow rate')
        self.set_flow_setpoint_btn.clicked.connect(self._set_flow)
        ctl_layout.addWidget(self.set_flow_setpoint_btn)
        ctl_layout.addSpacing(20)
        self.flow_setpoint_lineedit = pg.Qt.QtWidgets.QLineEdit()
        self.flow_setpoint_lineedit.setText('20')
        self.flow_setpoint_lineedit.returnPressed.connect(self._set_flow)
        ctl_layout.addWidget(self.flow_setpoint_lineedit)
        main_layout.addItem(ctl_layout)

        # Setup stop flow widget
        ctl_layout = pg.Qt.QtWidgets.QHBoxLayout()
        self.stop_flow_btn = pg.Qt.QtWidgets.QPushButton('STOP FLOW NOW!')
        self.stop_flow_btn.clicked.connect(self._stop_flow)
        ctl_layout.addWidget(self.stop_flow_btn)
        main_layout.addItem(ctl_layout)

        # Timers for auto-updating things
        buffer_update_timer = pg.QtCore.QTimer()
        buffer_update_timer.timeout.connect(self._update_buffer)
        plot_update_timer = pg.QtCore.QTimer()
        plot_update_timer.timeout.connect(self._update_plot)

        self.setLayout(main_layout)
        self.layout = main_layout
        self.buffer_update_timer = buffer_update_timer
        self.plot_update_timer = plot_update_timer
        self.curve = curve
        self.plot = plot

    def start(self, app_exec=False):
        r""" Start the GUI, make it visible. """
        self._dbgmsg('Starting application.')
        self.show()
        self.buffer_update_timer.start(int(self.buffer_update_interval * 1e3))
        self.plot_update_timer.start(int(self.plot_update_interval * 1e3))
        if app_exec:
            self.app.exec_()

    def _update_buffer(self):
        r""" Fetch data and update the internal memory buffer. """
        i = self.buffer_idx % self.buffer_length
        t, f = self.client.get(method='get_flow_rate')
        if f is not None:
            self.ring_buffer_t[i] = t
            self.ring_buffer_y[i] = f
            self.buffer_idx += 1
            self.flow_label.setText('%3.2f mg/min' % f)
        else:
            self._dbgmsg('Requested data is None.')
        _, setpoint = self.client.get(method='get_setpoint')
        self._dbgmsg('_update_buffer: setpoint  --------------------------->>>> ', setpoint)
        if setpoint is not None:
            self._dbgmsg('Updating setpoint label  -------------------------->>>> ', setpoint)
            self.flow_setpoint_label.setText('%3.2f mg/min' % setpoint)

    def _update_plot(self):
        r""" Update the plot. """
        if self.buffer_idx < 10:
            return
        t = self.ring_buffer_t
        y = self.ring_buffer_y
        if self.buffer_idx < self.buffer_length:  # Don't show garbage data values, or zeros.
            t = t[:self.buffer_idx]
            y = y[:self.buffer_idx]
        self.curve.setData(t - np.max(t), y)
        # self.curve2.setData()...
        if not self.is_initialized:
            self.plot.enableAutoRange('x', False)  # Stop auto-scaling after the first data set is plotted.
            self.plot.setXRange(-60, 0)
            self.is_initialized = True

    def _set_flow(self):
        r""" Set the flow rate, based on value in the widget. """
        self._dbgmsg('_set_flow')
        self.setpoint_modified = True
        try:
            self._dbgmsg('Getting requested flow rate...')
            flow = float(self.flow_setpoint_lineedit.text())
            self._dbgmsg('Requested flow rate:', flow)
        except:
            self._dbgmsg('Flow setpoint is not a float value.')
            return
        self._dbgmsg('Setting flow rate setpoint...')
        self.client.get(method='set_setpoint', args=[flow])

    def _stop_flow(self):
        self.client.get(method='set_setpoint', args=[0])

    def quit(self):
        if self.client.server is not None:
            self._stop_flow()
            self.client.server.stop()

# if __name__ == '__main__':
#
#     import pandas as pd
#
#     config_location = 'D:\\hardware_interfaces\\utils\\serial_tools\\com_port_config.json'
#
#     # It's hackin' time: FIX ME AT SOME POINT. DO NOT USE MORE THAN ONE BRONKHORST DEVICES FOR NOW
#
#     devices = identify_devices()
#
#     config_d = pd.read_json(config_location) #, lines=True, orient='split')
#     config_df = pd.json_normalize(config_d['all_devices']) # needed due to the structure of the json
#
#     bronk_com = get_bronkhorst_port_number(device_nickname='test_station_helium_sensor', devices_df=devices, config_df=config_df)
#
#     bronk = Bronkhorst(com_port=bronk_com)
#     print('Bronkhorst connection sensor opened! ')
#     print('Initialized class as:   bronk')
