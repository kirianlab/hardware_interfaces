import datetime


def append_timestamps_to_dict(d):
    r"""For convenience, append timestamps to dictionary"""
    if type(d) != dict:
        raise ValueError("Must be dict tpe.")
    now = datetime.datetime.now()
    d["timestamp"] = now.timestamp()  # machine readable
    d["timestamp_iso"] = now.isoformat(sep=" ")  # human   readable
    return d
