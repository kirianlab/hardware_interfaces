from serial.tools import list_ports
import pandas as pd


def identify_devices():
    r"""
    Fetch information about devices (for example, serial numbers).  This function will return
    a pandas dataframe with the following entries: 'com_port', 'vendor_id', 'product_id',
    'serial_number', 'description', 'location', 'manufacturer', 'product', 'interface', 'hwid'

    Procedure to determine a device serial number:

    (0) Plug in your device.

    (1) Run this function and print the dataframe that is returned.

    (2) Unplug the device.

    (3) Repeat step (1) and compare.

    Returns:
        pandas dataframe
    """
    a = list_ports.comports()
    df = pd.DataFrame()
    df['com_port'] = [port.name for port in a]
    df['vendor_id'] = [port.vid for port in a]
    df['product_id'] = [port.pid for port in a]
    df['serial_number'] = [port.serial_number for port in a]
    df['description'] = [port.description for port in a]
    df['location'] = [port.location for port in a]
    df['manufacturer'] = [port.manufacturer for port in a]
    df['product'] = [port.product for port in a]
    df['interface'] = [port.interface for port in a]
    df['hwid'] = [port.hwid for port in a]

    return df


def get_serial_port_from_serial_number(serial_number):
    r""" Just as the name of the function implies. """
    serial_port = None
    df = identify_devices()
    if serial_number in df['serial_number'].values:
        serial_port = df[df['serial_number'] == serial_number]['com_port']
    return serial_port


def access_config(fp):
    r"""
    FIXME: Documentation.

    FIXME: I don't think we can convert configs to pandas dataframes because they are too restrictive; if you don't have
    FIXME: data that is structured like a spreadsheet, pandas is awkward to work with.  We don't really know what the
    FIXME: structure of our config files will be at this time, and dictionary types give us needed flexibility.

    Arguments:
        fp (str): Path to the config file.

    Returns:
        pandas dataframe
    """
    df = pd.read_json(fp, lines=True)
    norm_df = pd.json_normalize(df['all_devices'])
    return norm_df
