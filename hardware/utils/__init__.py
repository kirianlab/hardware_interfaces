import sys
import json
import pkg_resources

from . import serial_communication_tools
from . import SHDLC_communicator


#def kirian_lab_config():
#    r"""
#    Returns a config dictionary from the utils/config/com_port_config.json file
#    """
#    file_path = pkg_resources.resource_filename('hardware', 'utils/config/com_port_config.json')
#    return json.load(file_path)

DEBUG_LEVEL = 0

def debug(*args, **kwargs):
    if 'level' not in kwargs.keys():
        level = 1
    else:
        level = int(kwargs['level'])
    if level > DEBUG_LEVEL:
        print("DEBUG:", *args, **kwargs, flush=True)
