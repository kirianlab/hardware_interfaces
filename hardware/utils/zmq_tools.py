# TODO: Catch errors when e.g. HPLC pump is not in remote and device instance fails
import pickle
import threading
try:
    import daemon3x
except:
    daemon3x = None
import zmq


class GenericZMQClient:
    r""" A client that can communicates with a GenericZMQServer instance via TCP protocol."""

    debug_prefix = ''
    debug_threshold = 0

    def __init__(self, config, start_server=False, debug=True):
        r"""
        Arguments:
        """
        self.debug = debug
        self.debug_prefix = 'GenericZMQClient:' + config['device_class'].__name__ + ':'
        self.server = None
        if start_server:
            self.dbgmsg('Attempting to start server.')
            try:
                self.server = GenericZMQServer(config=config)
                self.server.start()
            except zmq.error.ZMQError as e:
                # FIXME: Need to check if a server is already running instead of simply trying and failing...
                self.dbgmsg('Attempted to start server but failed with error:', e)
        address = config['zmq_client_address']
        self.dbgmsg('Creating client context.')
        context = zmq.Context()
        self.dbgmsg('Creating client socket.')
        socket = context.socket(zmq.REQ)
        socket.setsockopt(zmq.RCVTIMEO, 2000)
        socket.connect(address)
        self.dbgmsg('Done.')
        self.config = config
        self.address = address
        self.context = context
        self.socket = socket

    def dbgmsg(self, *args, level=1, **kwargs):
        if level <= self.debug_threshold:
            print(self.debug_prefix, *args, **kwargs)

    def get(self, method, args=[], kwargs={}):
        r""" Request that a method be called on the device.
        Arguments:
            method (str): The method name
            args (list): Positional arguments
            kwargs (dict): Keyword arguments
        """
        self.dbgmsg('sending request to server.')
        try:
            self.socket.send(pickle.dumps({'method': method, 'args': args, 'kwargs': kwargs}))
            data = pickle.loads(self.socket.recv())
            self.dbgmsg('Received data from server.')
        except:
            self.dbgmsg('Cannot get data from server.  Is it running?')
            data = None
        return data


class GenericZMQServer:
    r""" A server that is meant to create and thinly wrap around a device class instance.  Waits for a client to request
    that a method of the device class be called (along with positional and keyword arguments), and then pickles and
    returns the output of the method.

    We impose the following restrictions on the device class:
    1) It must accept the keyword argument 'config' that contains all instantiation parameters.
    2) For all relevant methods, it must be possible to pickle all input arguments and return data.
    3) There exists a 'disconnect' method that will do all appropriate steps needed before deleting the instance.
    """

    debug_prefix = ''
    debug_threshold = 0

    def __init__(self, config=None, debug=0):
        r"""
        Arguments:
            config (dict): The configuration dictionary.  Must contain the following keys:
                               device_class: A string with the name of the device class to be instantiated
                               zmq_server_address: Address of the server.  Example: `tcp://*:5555`
                            The configuration dictionary is passed on to the device class, so it should also contain
                            the appropriate configurations.
            debug (bool): If True, print out debugging messages.
        """
        self.debug_threshold = debug
        self.debug_prefix = 'GenericZMQServer:' + config['device_class'].__name__ + ':'
        self.dbgmsg('__init__')
        address = config['zmq_server_address']
        self.dbgmsg('Creating server context.')
        context = zmq.Context()
        self.dbgmsg('Creating server socket.')
        socket = context.socket(zmq.REP)
        poller = zmq.Poller()
        poller.register(socket, zmq.POLLIN)
        # socket.RCVTIMEO = 1000
        self.dbgmsg('Connecting to address', address)
        socket.bind(address)
        self.dbgmsg('Initialized')
        self.config = config
        self.address = address
        self.context = context
        self.socket = socket
        self.poller = poller
        # self.device is an instance of the class that communicates with the hardware
        self.device = config['device_class'](config=config)
        self.thread = None
        self.do_listen = True

    def dbgmsg(self, *args, level=1, **kwargs):
        if level <= self.debug_threshold:
            print(self.debug_prefix, *args, **kwargs)

    def start(self, threaded=True):
        r""" Start the server. """
        self.dbgmsg('Start.')
        # this is the where the server is waiting for a client to ask it something

        def listen():
            while self.do_listen:
                self.dbgmsg('Listening...')
                if not self.poller.poll(1000):
                    continue
                message = pickle.loads(self.socket.recv()) # if it gets here, it has received data from client
                method = message['method']
                args = message['args']
                kwargs = message['kwargs']
                self.dbgmsg('Request received:', method, args, kwargs)
                out = getattr(self.device, method)(*args, **kwargs)
                self.dbgmsg('Method called:', method, args, kwargs)
                self.socket.send(pickle.dumps(out)) # only thing that matters is that the output is pickled

        if threaded:
            if self.thread is None:
                self.dbgmsg('Starting thread.')
                self.thread = threading.Thread(target=listen)
                self.thread.start()
        else:
            listen()

    def stop(self):
        r""" Stop the server.  Kill the thread if it is threaded. """
        self.dbgmsg('Stopping', self.address)
        self.do_listen = False
        if self.thread is not None:
            self.dbgmsg('Joining thread', self.address)
            self.thread.join()
        self.dbgmsg('Closing socket', self.address)
        # self.socket.disconnect()
        self.socket.close()
        self.dbgmsg('Disconnecting device')
        self.device.disconnect()


# class GenericZMQDaemon(daemon3x.DaemonABC):
#     r""" A daemon that starts a server as a background process so that it persists after your program has exited.
#     This might be useful if more than one client will be fetching data."""
#
#     def __init__(self, pidfile=None, device_class=None, args=[], kwargs={}):
#         r"""
#         Arguments:
#             pidfile (str): Path to process ID file.
#             server (class): The type of server you want to initialize.
#             args (list): Positional arguments passed to server upon instantiation.
#             kwargs (dict): Keyword arguments passed to server upon instantiation.
#         """
#         super().__init__(pidfile)
#         self.pidfile = pidfile
#         self.server = None
#         self.device_class = device_class
#         self.args = args
#         self.kwargs = kwargs
#
#     def run(self):
#         r""" Start the server. """
#         if self.server is None:
#             self.server = GenericZMQServer(device_class=self.device_class, args=self.args, kwargs=self.kwargs)
#             self.server.start()