import re
import glob
import datetime
import logging
import logging.handlers as handlers
import pandas
import numpy as np


class TimedRotatingDataFileHandler(handlers.TimedRotatingFileHandler):
    def __init__(self, logfile, interval=20, header=""):
        r"""Same as logging.handlers.TimedRotatingFileHandler but adds a header to each file.

        FIXME: This presently writes headers in the middle of files... can't figure out how to

        Arguments:
            logfile (str): Full path to log file.  This name will have datetime stamps appended to it.
            interval (int): How many minutes before creating a new log file.
            header (str): The header to write at the top of the file.
        """
        super().__init__(logfile, when="m", interval=interval)
        self.header = header
        self.logger = None

    def doRollover(self):
        super().doRollover()
        if self.logger is not None:
            self.logger.info(self.header)


class DataLogger:
    def __init__(self, filename, data_names=None, data_formats=None, interval=10):
        r"""
        Basic text-based data logger.  Example:

        datalogger = DataLogger('/my/path/log', data_names=('time', 'x', 'y'), data_formats=(r'%f', r'%f', r'%f'))
        datalogger.log_data(t, x, y)

        Arguments:
            filename (str): The full path to the log file.  Datetime stamps will be appended.
            data_names (tuple): The names of each data column.
            data_formats (tuple): The format strings for each data column (e.g. r'%0.4f')
            interval (int): How many minutes before making a new log file.
        """
        datalog = logging.getLogger(filename)
        datalog.setLevel(logging.INFO)
        header = " ".join(data_names)
        data_handler = TimedRotatingDataFileHandler(
            filename, interval=interval, header=header
        )
        data_handler.logger = datalog
        data_handler.suffix = r"%Y-%m-%d_%H.%M.%S"
        data_handler.setLevel(logging.INFO)
        data_handler.setFormatter(logging.Formatter("%(message)s"))
        datalog.addHandler(data_handler)
        # FIXME: This should be comma separated not whitespace separated... but we
        #        need to fix the LogParser also if we make this change.
        self.format = " ".join(data_formats)
        self.datalog = datalog
        self.datalog.info(header)

    def log_data(self, *data):
        r"""
        Add data to the log.  Example: logger.log_data(dattime, dat1, dat1)
        """
        self.datalog.info(self.format % data)


class LogParser:
    files = []
    df = None
    df_file = None

    def __init__(self, regex):
        r"""A helper for intelligently parsing log files."""
        self.datepattern = re.compile("\d{4}-\d{2}-\d{2}_\d{2}.\d{2}.\d{2}")
        self.regex = regex
        self.glob_files()
        # if self.files:
        #     self.load_file(self.files[0])

    def glob_files(self):
        r"""Get all the log files."""
        self.files = sorted(glob.glob(self.regex))
        print(f"Globbed {len(self.files)} files")
        # for f in self.files:
        #     print(f)

    def load_file(self, filepath, append=False):
        r"""Load a file into pandas dataframe."""
        # print("load_file", filepath)
        with open(filepath) as f:
            header = f.readline()
            space = False
            if ',' not in header:
                space = True
            header_rows = []
            for i, line in enumerate(f):
                if line.startswith(header):
                    header_rows.append(i+1)
        print(f"Loading {filepath}")
        df = pandas.read_csv(filepath, delim_whitespace=space, skiprows=header_rows)
        if self.df is None:
            self.df = df
            return
        if not append:
            self.df = df
            return
        print(f"Appending data")
        self.df = pandas.concat([self.df, df])

    def load_time_range(self, t1, t2, _glob=True, append=True):
        r""" Combine all relevant log files into a single pandas dataframe. """
        print("Loading time range")
        # print("load_time_range")
        # Check if we already have a good dataframe
        if self.df is not None:
            et = self.df["epoch_time"].values
            if t1 >= et[0] and t1 <= et[1]:
                return
        # Find the range of files we need to load
        f1, idx1 = self.find_file(t1)
        f2, idx2 = self.find_file(t2)
        print("Time range", f1, f2)
        if (f1 is None or f2 is None) and _glob is True:
            # We don't have the full range, so update the list and
            # try one more time.
            self.glob_files()
            return self.load_time_range(t1, t2, _glob=False, append=True)
        if f1 is None and f2 is None:
            # No log files exist in this time range
            return
        if idx1 is None:
            # Go back as far as we can (silent fail...)
            idx1 = 0
        if idx2 is None:
            # Go out as far as we can (silent fail...)
            idx2 = len(self.files) - 1
        # Here is our file list to be loaded
        files = self.files[idx1:idx2+1]
        # Wipe out the existing dataframe
        if not append:
            self.df = None
        for f in files:
            self.load_file(f, append=True)

    def find_file(self, epoch_time):
        r"""Find which file corresponds to a certain epoch time."""
        # print("find_file")
        fs = []
        ets = []
        for f in self.files:
            try:  # ... to find date substring and convert to epoch time
                print(f)
                matcher = self.datepattern.search(f)
                t = matcher.group(0)
                print(t)
                et = datetime.datetime.strptime(t, r"%Y-%m-%d_%H.%M.%S").timestamp()
                print(et, et-epoch_time)
            except:
                print("FAIL")
                continue
            fs.append(f)
            ets.append(et)
        if len(ets) == 0:
            return None, None
        ets = np.array(ets)
        w = np.where(ets <= epoch_time)[0]
        if w.size > 0:
            print("Seeking:", epoch_time)
            print("Found this one:", fs[w[-1]], w[-1])
            return fs[w[-1]], w[-1]
        else:
            return None, None

    def get_columns(self):
        r"""Get the names of columns in the log file."""
        columns = []
        if self.df is not None:
            columns = self.df.columns
        return columns

    def get_value(self, epoch_time, column, _load_file=True):
        r"""Get value of given column corresponding to a certain epoch time."""
        # If there is no dataframe in memory, read the file and try again
        # If there is a dataframe in memory, check if the epoch time is within range
        # If the epoch time is out of range, attempt to load a file
        if self.df is None and _load_file:
            f, i = self.find_file(epoch_time)
            if f is not None:
                self.load_file(f)
            return self.get_value(epoch_time=epoch_time, column=column, _load_file=False)
        if self.df is None:
            return None
        t = self.df["epoch_time"].values
        c = self.df[column].values
        if len(t) < 2:
            return None
        dt = np.mean(t[1:-1] - t[0:-2])
        if epoch_time < (t[0] - dt) or epoch_time > (t[-1] + dt):
            self.df = None
            return self.get_value(epoch_time=epoch_time, column=column, _load_file=True)
        val = np.interp(epoch_time, t, c)
        return val
