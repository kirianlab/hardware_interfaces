r""" Standard ways for us to handle timestamps in text and data files. """
import time
import datetime

def get_datetime():
    r"""
    Returns:
        timestamp (str): Machine readable timestamp
        timestamp_iso (str): Human readable timestamp. i.e., 'yyyy-mm-dd h:m:s:ms'
    """
    now = datetime.datetime.now()
    return to_epoch(now), to_str(now)

def to_epoch(dt):
    r""" Convert datetime object to epoch time (float type). """
    return dt.timestamp()

def to_str(dt):
    r""" Convert datetime object to human-readable ISO timestamp (str type) in local time. """
    return dt.isoformat(sep=" ")

def append_timestamps_to_dict(d):
    r""" For convenience, append timestamps to dictionary"""
    if type(d) != dict:
        raise ValueError('Must be dict tpe.')
    epoch, now = get_datetime()
    d['timestamp'] = epoch  # machine readable
    d['timestamp_iso'] = now  # human readable
    return d
