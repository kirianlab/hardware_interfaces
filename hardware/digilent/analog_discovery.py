# core
from ctypes import *
import sys
import time

# sdk
dwf = cdll.LoadLibrary("libdwf.so")
sys.path.append("../hardware_interfaces_sdk/digilent")
from dwfconstants import *


class Arbitrary_Function_Generator:

    def __init__():
        r"""
        Class to controll the Digilent Analog Discovery 2 with python.

        This class will automatically communicate with the connected device.
        However, you must close the device manually (using the close_connection method) to prevent issues.

        Possible waveforms:
              dc  Generate DC value set as offset.
            sine  Generate sine waveform.
          square  Generate square waveform.
        triangle  Generate triangle waveform.
          rampUp  Generate a waveform with a ramp-up voltage at the beginning.
        rampDown  Generate a waveform with a ramp-down voltage at the end.
           noise  Generate noise waveform from random samples.
           pulse  Generate pulse waveform.
        """
        self.hdwf = c_int()
        self.channel1 = c_int(0)  # 1
        self.channel2 = c_int(1)  # 2
        self.waveforms = {"dc": c_ubyte(0),
                          "sine": c_ubyte(1),
                          "square": c_ubyte(2),
                          "triangle": c_ubyte(3),
                          "rampUp": c_ubyte(4),
                          "rampDown": c_ubyte(5),
                          "noise": c_ubyte(6),
                          "pulse": c_ubyte(7),
                          "custom": c_ubyte(30),
                          "play": c_ubyte(31),}
        dwf.FDwfDeviceOpen(c_int(-1), byref(self.hdwf))  # open device

        if self.hdwf.value == c_int(0).value:  # check that connection is not none
            szerr = create_string_buffer(512)
            dwf.FDwfGetLastErrorMsg(szerr)
            print("Failed to open device:\n\n")
            print(str(szerr.value))
            quit()

    def start_afg_channel(self, channel: int, waveform: str,
                          frequency: float = 120, amplitude: float = 5,  duration: int = 5,
                          duty: float = 10):
        r"""
        Enable channel 2 function generator.

        Arguments
        ---------
            channel (int):  channel to start (1 or 2 only)
            waveform (str): desired waveform for generated signal
            frequency (float): desired frequency of generated signal in Hz (default is 120)
            amplitude (float): desired amplitude of generated signal in V (default is 5)
            duration (float): desired length of generated signal in s (default is 5)
            duty (float): duty cycle as % (on for square)

        Returns
        -------
            None
        """
        if waveform not in self.waveforms.keys():
            print(f"Error waveform must be:\n\n{self.waveforms.keys()}")
        carrier  = c_int(0)
        if channel == 1:
            self._start_afg_channel(self.channel1, carrier, waveform, frequency, amplitude, duration, duty)
        elif channel == 2:
            self._start_afg_channel(self.channel2, carrier, waveform, frequency, amplitude, duration, duty)
        elif channel == 3:
            self.start_afg_channels_synced(carrier, waveform, frequency, amplitude, duration, duty)
        else:
            print("Error! You can only control channel 1 or 2.")

    def _start_afg_channel(self, channel, carrier, waveform, frequency, amplitude, duration, duty):
        # turn on channel
        dwf.FDwfAnalogOutNodeEnableSet(self.hdwf, channel, carrier, c_bool(True))
        # set the waveform
        dwf.FDwfAnalogOutNodeFunctionSet(self.hdwf, channel, carrier, self.waveforms[waveform])
         # set the frequency in Hz
        dwf.FDwfAnalogOutNodeFrequencySet(self.hdwf, channel, carrier, c_double(frequency))
         # set the amplitude in Volts
        dwf.FDwfAnalogOutNodeAmplitudeSet(self.hdwf, channel, carrier, c_double(amplitude))
        if waveform == "square":
            dwf.FDwfAnalogOutSymmetrySet(self.hdwf, channel, c_double(duty))
        # start signal
        dwf.FDwfAnalogOutConfigure(self.hdwf, channel, c_bool(True))
        time.sleep(duration)

    def start_afg_channels_synced(self, carrier, waveform, frequency, amplitude, duration, duty, phase):
        r"""
        Enable channel 2 function generator.

        Arguments
        ---------
            channel (int):  channel to start (1 or 2 only)
            waveform (str): desired waveform for generated signal
            frequency (float): desired frequency of generated signal in Hz (default is 120)
            amplitude (float): desired amplitude of generated signal in V (default is 5)
            duration (float): desired length of generated signal in s (default is 5)
            duty (float): duty cycle as % (on for square)
            phase (float): phase offset between channels in degrees

        Returns
        -------
            None
        """
        # enable both channels
        dwf.FDwfAnalogOutNodeEnableSet(self.hdwf, self.channel1, carrier, c_int(True))
        dwf.FDwfAnalogOutNodeEnableSet(self.hdwf, self.channel2, carrier, c_int(True))
        # set channel 1 as leader and channle 2 as follower
        dwf.FDwfAnalogOutMasterSet(self.hdwf, self.channel2, self.channel1);
        # set parameters
        dwf.FDwfAnalogOutNodeFunctionSet(self.hdwf, c_int(-1), carrier, funcSine)
        dwf.FDwfAnalogOutNodeFrequencySet(self.hdwf, c_int(-1), carrier, c_double(frequency))
        dwf.FDwfAnalogOutNodeAmplitudeSet(self.hdwf, c_int(-1), carrier, c_double(amplitude))
        # set phase for second channel
        dwf.FDwfAnalogOutNodePhaseSet(self.hdwf, self.channel2, carrier, c_double(phase))
        # start signal
        dwf.FDwfAnalogOutConfigure(self.hdwf, self.channel1, c_bool(True))
        time.sleep(duration)

    def stop_afg_channel(self, channel: int):
        r"""
        Stop channel 2 wavegen.

        Resets wavegen to stop it.

        Arguments
        ---------
            c (int): channel to stop (1 or 2 only)

        Returns
        -------
            None
        """
        if channel == 1:
            self.dwf.FDwfAnalogOutReset(self.hdwf, self.channel2)
        if channel == 2:
            self.dwf.FDwfAnalogOutReset(self.hdwf, self.channel2)
        else:
            print("Error! You can only control channel 1 or 2.")

    def close_connection(self):
        # close the device
        self.dwf.FDwfDeviceCloseAll()
