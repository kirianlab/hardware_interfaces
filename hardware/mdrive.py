import serial
import time
from .utils import serial_communication_tools


aerosol_injector_x_serial = '063140353'
aerosol_injector_y_serial = '063140354'
aerosol_injector_z_serial = '315110225'

class MDrive:

    steps_per_mm = 83035  # Convert physical distance to stepper motor steps
    sleep_time = 0.1  # Amount of time to sleep between commands
    ser = None

    def __init__(self, serial_port=None, serial_number=None, baudrate=9600, timeout=0):
        r""" Class for moving MDrive motors. """
        if serial_number is not None:
            serial_port = serial_communication_tools.get_serial_port_from_serial_number(serial_number)
            if serial_port is None:  # The motor might be behind a USB to UART Bridge
                devs = serial_communication_tools.identify_devices()
                devs = devs[devs['manufacturer'] == 'Silicon Labs']
                for port in devs['com_port']:
                    try:
                        ser = serial.Serial(port, baudrate=baudrate, timeout=timeout)
                    except:
                        continue
                    if ser.isOpen():
                        ser.write('PR SN\r\n'.encode('utf-8'))
                        time.sleep(self.sleep_time)
                        ser_num = ser.read(ser.in_waiting).decode('UTF-8').split()[2].strip()
                        if ser_num == serial_number:
                            serial_port = port
                            self.ser = ser
                            break
        if serial_port is None:
            raise ValueError('MDrive: Serial port is None.  Possibly bad serial number, or the port cannot be found.')
        if self.ser is None:
            self.ser = serial.Serial(serial_port, baudrate=baudrate, timeout=timeout)
        if not self.ser.isOpen():
            raise ConnectionRefusedError('MDrive: The serial port was not opened for some reason.')
        self.set_acceleration('slow')

    def set_acceleration(self, acc):
        r""" Set the acceleration.  You may choose a raw value, or "slow", "medium", or "fast". """
        if acc == 'slow':
            a = 50000
        elif acc == 'medium':
            a = 250000
        elif acc == 'fast':
            a = 2000000
        else:
            a = acc
        command = 'A {}\r\n'.format(a)
        self.ser.write(command.encode('utf-8'))
        time.sleep(self.sleep_time)

    def translate_steps(self, steps):
        r""" Translate by number of stepper motor steps. """
        command = 'MR {}\r\n'.format(int(steps))
        self.ser.write(command.encode('utf-8'))
        time.sleep(self.sleep_time)

    def translate_mm(self, mm, max_move=3):
        if mm > max_move:
            print("Don't move more than %f mm in a single translation" % (max_move,))
        self.translate_steps(int(mm * self.steps_per_mm))

    def get_velocity(self):
        r""" Get velocity in ??? units. """  # FIXME: Docs
        command = 'PR V\r\n'
        self.ser.write(command.encode('utf-8'))
        time.sleep(self.sleep_time)
        response = self.ser.read(100).decode('utf-8').split()[-2]
        return int(response)

#    def __del__(self):
 #       self.ser.close()
