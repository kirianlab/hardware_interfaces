r"""
Tools for working with Sensirion liquid flowmeters.

Authors: Sahba Zaare, Konstantinos Karpos, Vivek Krishnan, Richard Kirian
"""
import os
import serial
import sys
import time
import threading
import logging
import logging.handlers as handlers
import json
import numpy as np
from .utils import serial_communication_tools as com_tools
from .utils import SHDLC_communicator as shdlc
import pylab as plt
from os import path
import datetime
from .utils import zmq_tools
from .utils import time_tools
from .utils import loggers
import pyqtgraph as pg
import math


def known_devices():
    return dict(test_station_high_flow = dict(nickname='High Flow', serial='FTSTT5NA'),
                test_station_low_flow  = dict(nickname='Low Flow', serial='FTXEF0AHA'))


def array_to_decimal(array: list, base: int = 256) -> int:
    r""" Convert an array of base `base` integers to a single decimal number"""
    return np.sum(np.array(array)[::-1]*base**np.arange(len(array)))


def char_array_to_ascii_str(array: list) -> str:
    r"""
    Convert an array of chars (each 0-255) to an ASCII string.
    Terminates the string upon reaching the null char anywhere.
    """
    cpy = array.copy()
    final = ""
    while cpy:
        new = cpy.pop(0)
        if not new:     # null terminator is decimal 0 in ASCII
            break
        final += chr(new)
    return final


class SensirionDevice(shdlc.SHDLC_SerialConnection):
    r""" Device class for Sensirion liquid flowmeter.  Maintains serial connection to device.  Maintains a ring
    buffer of flow readings and regularly updates it."""
    # For more information on the communication commands see this document:
    # https://drive.google.com/drive/folders/1VkH-MKINcctKvCGH1PAK2_c3EDKsRw4V
    CMDS = dict(
        get_device_information      =0xD0,
        sensor_type                 =0x24,
        i2c_address                 =0x25,
        get_sensor_status           =0x30,
        start_single_measurement    =0x31,
        get_single_measurement      =0x32,
        start_continuous_measurement  =0x33,
        stop_continuous_measurement  =0x34,
        get_last_measurement        =0x35,
        get_measurement_buffer      =0x36,
        totalizer_status            =0x37,
        totalizer_value             =0x38,
        reset_totalizer             =0x39,
        measure_type                =0x40,
        resolution                  =0x41,
        heater                      =0x42,
        calibration                 =0x43,
        sensor_name                 =0x50,
        sensor_item_number          =0x51,
        flow_unit                   =0x52,
        scale_factor                =0x53,
        sensor_serial_num           =0x54,
        measure_data_type           =0x55
    )
    MAIN_UNIT = {8: 'liter', 9: 'gram'}
    TIME_BASE = {1: '/microsecond', 2: '/millisecond', 3: '/second', 4: '/minute', 5: '/hour', 6: '/day'}
    DIMENSION = {3: 'nano', 4: 'micro', 5: 'milli', 6: ''}
    debug_threshold = 0
    sensor_name = None

    def _dbgmsg(self, *args, level=1, **kwargs):
        if level <= self.debug_threshold:
            print('Sensirion:', *args, **kwargs)

    def __init__(self, config=None, serial_number=None, debug=0):
        r"""
        Arguments:
            config (dict): Configurations.  See make_config() for an example.
            serial_number (str): Serial number to identify COM port.
            debug (int): Zero means no debug messages.  Larger numbers increase verbosity of debug messages.
        """
        # If entries in the config dictionary are missing, then we swap in the keyword arguments
        self._dbgmsg('__init__')
        self.debug_threshold = debug
        if config is None:
            config = make_config()
        com_port = config.get('com_port', None)
        serial_number = config.get('serial_number', serial_number)
        resolution = config.get('resolution', 16)
        calibration = config.get('calibration', 0)
        polling_frequency = config.get('polling_frequency', 30)
        # Fetch the COM port in the case that only the serial number is provided
        if com_port is None:
            df = com_tools.identify_devices()
            df = df.loc[df['serial_number'] == serial_number]
            com_port = df['com_port'].values[0]
        self.calibration = None
        self.resolution = None
        self.thread = None
        self.config = config
        self.com_port = com_port
        self.serial_number = serial_number  # TODO: Figure out why this differs from the number returned by the device
        self.current_flow = None
        self.current_smoothed_flow = None
        self.current_time = None
        self.buffer_t0 = 0
        self.buffer_length = int(12*3600*polling_frequency)
        self.buffer_interval = 1/polling_frequency
        self.buffer_data = np.empty(self.buffer_length, dtype=float)
        self.buffer_times = np.empty(self.buffer_length, dtype=float)
        self.buffer_idx = -1
        self.last_time = 0
        self.buffering = False
        super().__init__()
        self.establish_connection()
        self.stop_continuous_measurement()
        self.sensor_item_number = self.get_sensor_item_number()
        self.serial_num = self.get_serial_num()
        self.sensor_name = self.get_sensor_name()
        self.set_calibration(calibration)
        self.set_resolution(resolution)
        self.sensor_type = self.get_sensor_type()
        self.units = self.get_units()
        self.scale_factor = self.get_scale_factor()
        self.log_directory = config['log_file_path']+'/sensirion_'+self.sensor_name+'_'+self.serial_number+'/'
        os.makedirs(self.log_directory, exist_ok=True)
        self.status_file_path = self.log_directory + 'status.json'
        self.log_file_path = None
        self.datalog = None
        self.setup_datalog()
        self.start_buffering()
        self._dbgmsg('Sensirion initialized')

    def setup_datalog(self):
        filepath = os.path.dirname(self.status_file_path) + '/data.log'
        data_names = ('epoch_time', 'flow_rate')
        data_formats = (r'%f', r'%f')
        self.datalog = loggers.DataLogger(filepath, data_names=data_names, data_formats=data_formats)

    def log_datapoint(self):
        self.datalog.log_data(time.time(), self.current_flow)

    def establish_connection(self):
        self._dbgmsg('establish_connection')
        try:
            self.open_connection(self.com_port)
        except (serial.SerialTimeoutException,
                serial.serialutil.SerialTimeoutException,
                serial.serialutil.SerialException,
                serial.SerialException) as e:
            print("!!! Sensirion: Serial Exception:", repr(e))
            sys.exit()
        except (OSError, FileNotFoundError) as e:
            print("!!! Sensirion: Serial Connection Error:", repr(e))
            sys.exit()
        except ValueError as e:
            print("!!! Sensirion: Serial Comm Established but Either Unresponsive or Incorrect:", repr(e))
            sys.exit()
        except Exception as e:
            print("!!! Sensirion: Misc Serial Connection Exception:", repr(e))
            sys.exit()

    def start_buffering(self):
        self._dbgmsg('start_buffering')
        self.thread = threading.Thread(target=self.buffer_thread)
        self.buffering = True
        self.thread.start()

    def stop_buffering(self):
        self._dbgmsg('stop_buffering')
        self.buffering = False
        if self.thread is not None:
            time.sleep(2*self.buffer_interval)
            self.thread.join()
        self.stop_continuous_measurement()

    def buffer_thread(self):
        self._dbgmsg('buffer_thread', level=2)
        self.start_continuous_measurement()
        self.buffer_t0 = time.time()
        slp = float(self.buffer_interval) / 10
        status_time = np.floor(time.time())
        while self.buffering:
            t = time.time()
            if (t - self.last_time) > self.buffer_interval:
                buf = self.get_measurement_buffer()
                while buf is None:
                    buf = self.get_measurement_buffer()
                    time.sleep(slp)
                for val in buf:
                    idx = self.buffer_idx + 1
                    idx %= self.buffer_length
                    self.buffer_data[idx] = val
                    self.buffer_times[idx] = t
                    self.buffer_idx = idx
                    self.current_flow = val
                    self.log_datapoint()
                    if idx == 0:
                        self.current_smoothed_flow = val
                    else:
                        w = 0.1
                        self.current_smoothed_flow = self.current_smoothed_flow*(1-w) + val*w
                    self.current_time = t
                    self._dbgmsg('buffer_thread:', self.current_time - self.buffer_t0, self.current_flow, level=2)
                self.last_time = t
                if np.abs(status_time - t) > 1:  # Update status file at 1 Hz
                    status_time = np.floor(t)
                    self.write_status_json_text()
            time.sleep(time.time()-self.last_time)


    def get_past_measurements(self,t1,t0, use_current_time = True):
        r""" Return avg. flow rate between
        [current time - t0, current_time - t1] or [t1, t0]
        """
        if use_current_time:
            t = time.time()
            t = [t-t0,t-t1]
        else: t = [t1,t0]
        ind = [np.argmax(self.buffer_times >= t[0]), np.argmax(self.buffer_times >= t[1])]
        return np.average(self.buffer_data[ind[0]:ind[1]+1])


    def get_units(self) -> str:
        r""" Return flow unit of this sensor at current calib field.

        Sensor response to flow unit request is 2 Bytes = 16 bits:
        * most significant byte, bits 8-12: Main pressure/volume unit (x 16^2 = 256)
        * least significant byte combines dimension (e.g. nano) and time base (e.g. per second).  bits 4-7: time base
        (x 16^1 = 16).  bits 0-3: dimension (x 16^0)

        Example: sensor response to flow unit request is [8, 67] (2 Bytes)

        "measure" = array_to_decimal(array=response, base=16) = 2115 = (8 x 16^2) + (4 x 16^1) + (3 x 16^0)

        2115(dec) = 843(hex)
        8   (dec) = 8  (hex) => main unit = 8 (liter)
        67  (dec) = 43 (hex) => dimension = 3 (nano),
        time base = 4 (per min)
        """
        self._dbgmsg('get_units')
        response, err = self.SHDLC_send_and_receive(0, self.CMDS["flow_unit"], [])
        unit = hex(array_to_decimal(response)).lstrip('0x')
        M = int(unit[0])
        T = int(unit[1])
        D = int(unit[2])
        result = ""
        if (M in self.MAIN_UNIT) and (T in self.TIME_BASE) and (D in self.DIMENSION):
            result += self.DIMENSION[D]
            result += self.MAIN_UNIT[M]
            result += self.TIME_BASE[T]
        else:
            result = "unknown"
        self._dbgmsg('flow units:', result)
        return result

    def get_flow_rate(self):
        r""" Get's the most recent flow rate from the RAM buffer. """
        self._dbgmsg('get_flow_rate', level=2)
        return self.current_time, self.current_flow

    def get_smoothed_flow_rate(self):
        self._dbgmsg('get_smoothed_flow_rate', level=2)
        return self.current_time, self.current_smoothed_flow

    def set_calibration(self, calibration):
        r""" Set the calibration field on the device.  Affects the resolution and flow range.  Read the manual for
        more info.  """
        self._dbgmsg('set_calibration(', calibration, ')')
        self.wait_until_sensor_is_not_busy()
        response, err = self.SHDLC_send_and_receive(0, self.CMDS["calibration"], [int(calibration)])
        # self.dbgmsg(response, err)
        if err:
            print('WARNING: Sensirion.set_calibration received error', err)
        self.calibration = calibration

    def set_resolution(self, resolution):
        r""" Set the calibration field on the device.  Affects the resolution and flow range.  Read the manual for
        more info.  """
        self._dbgmsg('set_resolution(', resolution, ')')
        self.wait_until_sensor_is_not_busy()
        response, err = self.SHDLC_send_and_receive(0, self.CMDS["resolution"], [int(resolution)])
        if err:
            print('WARNING: Sensirion.set_resolution received error', err)
        self.get_resolution()

    def get_resolution(self):
        r""" Set the calibration field on the device.  Affects the resolution and flow range.  Read the manual for
        more info.  """
        self._dbgmsg('get_resolution')
        response, err = self.SHDLC_send_and_receive(0, self.CMDS["resolution"], [])
        if err:
            print('WARNING: Sensirion.get_resolution received error', err)
        resolution = array_to_decimal(response)
        self._dbgmsg('resolution:', resolution)
        self.resolution = resolution

    def get_scale_factor(self):
        r""" Scale factor is needed to convert digital readings to physical units.  You should not need to know what
        this parameter is. """
        self._dbgmsg('get_scale_factor')
        self.wait_until_sensor_is_not_busy()
        response, err = self.SHDLC_send_and_receive(0, self.CMDS["scale_factor"], [])
        if err:
            print('WARNING: Sensirion.get_scale_factor received error', err)
        return array_to_decimal(response)

    def get_sensor_item_number(self):
        r""" Get the sensor item number (the item ID you see when purchasing). """
        self._dbgmsg('get_sensor_item_number')
        self.wait_until_sensor_is_not_busy()
        response, err = self.SHDLC_send_and_receive(0, self.CMDS["sensor_item_number"], [])
        if err:
            print('WARNING: Sensirion.get_sensor_item_number received error', err)
        id = char_array_to_ascii_str(response)
        self._dbgmsg('ID:', id)
        return id

    def get_sensor_status(self):
        r""" Get status dictionary that includes the entries 'sensor_busy' and
        'continuous_measurement_enabled'. """
        self._dbgmsg('get_sensor_status', level=2)
        response, err = self.SHDLC_send_and_receive(0, self.CMDS["get_sensor_status"], [])
        if err:
            print('WARNING: Sensirion.get_sensor_status received error', err)
        s = np.unpackbits(np.uint8(response[0]))
        status = dict(sensor_busy=s[-1], continuous_measurement_enabled=s[-2])
        self._dbgmsg('status:', status, level=2)
        return status

    def sensor_is_busy(self):
        r""" Check if the device is busy. """
        return self.get_sensor_status()['sensor_busy']

    def wait_until_sensor_is_not_busy(self, check_interval=0.1):
        r""" Keep checking the device until it is not busy.

        Arguments:
            check_interval (float): How long to wait between checks.
        """
        if self.sensor_is_busy():
            while self.sensor_is_busy():
                self._dbgmsg('Device is busy... waiting...')
                time.sleep(check_interval)

    def get_serial_num(self):
        r""" Get the device serial number. """
        self._dbgmsg('get_serial_num')
        self.wait_until_sensor_is_not_busy()
        response, err = self.SHDLC_send_and_receive(0, self.CMDS["get_device_information"], [3])
        serial_number = char_array_to_ascii_str(response)
        self._dbgmsg('serial number:', serial_number)
        return serial_number

    def get_sensor_name(self):
        r""" Get a custom sensor name that is useful for those working in the lab. """
        self._dbgmsg('get_sensor_name')
        if self.sensor_name is not None:
            return self.sensor_name
        self.wait_until_sensor_is_not_busy()
        response, err = self.SHDLC_send_and_receive(0, self.CMDS["sensor_name"], [])
        if err:
            print('WARNING: Sensirion.get_sensor_name received error', err)
        name = char_array_to_ascii_str(response).strip()
        self._dbgmsg('name:', name)
        return name

    def get_sensor_type(self):
        r""" Get a custom sensor name that is useful for those working in the lab. """
        self._dbgmsg('get_sensor_type')
        self.wait_until_sensor_is_not_busy()
        response, err = self.SHDLC_send_and_receive(0, self.CMDS["sensor_type"], [])
        if err:
            print('WARNING: Sensirion.get_sensor_type received error', err)
        return int(array_to_decimal(response))

    def get_measurement_buffer(self):
        r""" Attempts to get all the measurements in memory.  Converts to floating point flow units.  Returns a numpy
        array. """
        self._dbgmsg('get_measurement_buffer', level=2)
        self.get_sensor_status()
        response, err = self.SHDLC_send_and_receive(0, self.CMDS["get_measurement_buffer"], [])
        if err:
            return None
        n_measurements = int(len(response)/2)
        if n_measurements < 1:
            return None
        buf = np.empty(n_measurements, dtype=float)
        for i in range(n_measurements):
            val = response[i*2:i*2+2]
            val = array_to_decimal(val)
            if val >= 2 ** (self.resolution - 1):
                val = val - 2 ** self.resolution
            val = float(val) / self.scale_factor
            buf[i] = val
        return buf

    def start_continuous_measurement(self):
        r""" Clears the device buffer and starts continuous measurement buffering.  Should be used with
        get_measurement_buffer method. """
        self.SHDLC_send_and_receive(0, self.CMDS['start_continuous_measurement'], [0, 0])

    def stop_continuous_measurement(self):
        r""" Stops continuous measurement buffering. """
        self.SHDLC_send_and_receive(0, self.CMDS['stop_continuous_measurement'], [])

    def write_status_json_text(self):
        filepath = self.status_file_path
        data = dict()
        data = time_tools.append_timestamps_to_dict(data)
        data['device'] = self.sensor_name
        data['serial'] = self.serial_number
        data['reading'] = self.get_smoothed_flow_rate()[1]
        data['units'] = 'ul/min'
        with open(filepath, 'w') as f:
            json.dump(data, f, indent=4)

    def wait(self, filepath=None):
        if(filepath is None):
            filepath = self.config["status_file"]
        while(path.exists(filepath)):
            time.sleep(0.1)

    def create_regressions(self, datapath:str, order:int=3):
        # The dimensions array is of the form [high calibration0, highcalibration1, lowcalibration0, lowcalibration1] 
        # with each sub array of the form [sensiron max, hplc max]
        # FIXME: The dimensions array may need to be changed based upon what data from each sensor should be included in the fit.
        dimensions = [[50, 50], [5, 25], [5, 5], [20, 20]]
        f = json.load(open(datapath))
        config = f['config']
        data = f['data']
        xydata = []
        xydata.append([config['hplc_flowrate'], data['data_high_0']])
        xydata.append([config['hplc_flowrate'], data['data_high_1']])
        xydata.append([config['hplc_flowrate'], data['data_low_0']])
        xydata.append([config['hplc_flowrate'], data['data_low_1']])
        regressionArr = []
        for j in range(4):
            array = xydata[j]
            # x and y are flipped so the polynomial has x as the sensiron data and y as the hplc data
            x = array[1]
            y = array[0]
            indexes = []
            # Cuts out all data not in the specified range
            indexesy = [index for index in range(len(y)) if y[index] > dimensions[j][1]]
            indexesx = [index for index in range(len(x)) if x[index] > dimensions[j][0]]
            indexes = indexesx + indexesy;
            indexes = list(set(indexes))
            x = np.delete(x, indexes)
            y = np.delete(y, indexes)
            p = np.polyfit(x, y, order)
            regression = np.poly1d(p)
            # poly1d objects cannot be serialized, so I have to create an array with coefficients and a degree term
            regressionArr1 = [order]
            for i in range(order + 1):
                regressionArr1.append(regression[order - i])
            regressionArr.append(regressionArr1)
        return regressionArr

    def plot_regressions(self, datapath:str):
        dimensions = [[50, 50], [5, 25], [5, 5], [20, 20]]
        f = json.load(open(datapath));
        config = f['config']
        data = f['data']

        fig, ax = plt.subplots(2,2, figsize=(15,10), tight_layout=True)
        # Inintialize an array of attributes of the xy data
        xydata = []
        xydata.append(ax[0][0].plot(config['hplc_flowrate'], data['data_high_0'], '.b'))
        ax[0][0].set_title("High Flow, Calibration = 0")

        xydata.append(ax[0][1].plot(config['hplc_flowrate'], data['data_high_1'], '.b'))
        ax[0][1].set_title("High Flow, Calibration = 1")

        xydata.append(ax[1][0].plot(config['hplc_flowrate'], data['data_low_0'], '.r'))
        ax[1][0].set_title("Low Flow, Calibration = 0")

        # plot the low flow, calibration 1 data
        xydata.append(ax[1][1].plot(config['hplc_flowrate'], data['data_low_1'], '.r'))
        ax[1][1].set_title("Low Flow, Calibration = 1")

        for i in range(2):
            for j in range(2):
                ax[i][j].plot(config['hplc_flowrate'], config['hplc_flowrate'], 'k', label='HPLC Setpoints')
                x_range = dimensions[2*i+j][0]
                y_range = dimensions[2*i+j][1]
                ax[i][j].set_xlim(0, x_range)
                ax[i][j].set_ylim(0, y_range)
                ax[i][j].set_aspect('equal', adjustable='box')
                sensironfit = np.linspace(0, x_range+1, 20000)
                hplcfit = []
                for term in sensironfit:
                    hplcfit.append(self.return_fitted_flowrate(sensor_reading=term, term=2*i+j, datapath=datapath))
                ax[i][j].plot(hplcfit, sensironfit, linewidth = 5, color='g')
                correlation_matrix = np.corrcoef(sensironfit, hplcfit)
                correlation_xy = correlation_matrix[0, 1]
                r_squared = correlation_xy ** 2

                # print("Plot index: " + str(i) + ", " + str(j) + ": " + "R_squared = " + str(r_squared))
        fig.supylabel("Sensirion Flow Rate [uL/min]", fontsize=16)
        fig.supxlabel("HPLC Flow Rate [ul/min]", fontsize=16)
        fig.suptitle(f"Sensirion Low/High vs HPLC Flow Rate \n {config['avg_n_points']} Readings Averaged Per Datapoint",
                     fontsize=18)
        plt.show()

    #TODO: Possibly merge this function into createRegressions if there 
    #       is no need to display regressions without writing to a json file
    # writeRegressions should be used to write regressions to json file, 
    #       createregressions can be used to return the regressions
    def write_regressions(self, regression_filepath:str, write_filepath:str="regressions.json"):
        with open(write_filepath, 'w') as file:
            json_string = json.dumps(self.create_regressions(regression_filepath))
            file.write(json_string)

    def return_fitted_flowrate(self, term, sensor_reading, datapath):
        # Index represents the sensor used
        # [0,1,2,3] = [high calibration0, high calibration1 low calibration0, low calibration1]
        # sensor_reading should be a reading from the sensiron sensor, which would be converted using 3rd degree polynomial to a hplc result

        regression = self.create_regressions(datapath=datapath, order=3)[term]
        order = int(regression[0])
        result = 0
        for i in range(order+1):
            result += regression[i+1]*sensor_reading**(order-i)
        return result

    def disconnect(self):
        self.stop_buffering()

    def __del__(self):
        self._dbgmsg('__del__', level=2)
        self.stop_buffering()


class SensirionDeviceDummy:
    r""" Dummy class for developing/testing code without physical device. """
    def __init__(self, *args, **kwargs):
        pass
    def get_flow_rate(self):
        return time.time(), np.random.rand() - 0.5
    def get_smoothed_flow_rate(self):
        return time.time(), (np.random.rand() - 0.5)*0.1
    def disconnect(self):
        pass
    def get_sensor_name(self):
        return "Dummy Sensor"


def make_config(zmq_port=5555, dummy=False):
    r""" Create a standard configuration dictionary for use with Sensirion Device, Client and Server classes.

    Arguments:
        zmq_port (int): The port to be used for server/client instances.
        dummy (bool): Set to true if you want a dummy SensirionDevice that emulates a real one (for debugging).
    """
    config = {
        'device_class': SensirionDevice,  # This is the class that should be instantiated for this device.
        'serial_number': 'FTSTT5NA',  # All devices should have some identifier that is unique to the specific device.
        'calibration': 0,  # Sensirion "calibration" for low (0) or high (1) flow rates
        'resolution': 16,  # Measurement resolution (bits)
        'polling_frequency': 100,  # Specific to this type of device
        'log_file_path': 'D:/data/microjets/logs',  # Where to keep log data
        'logging': True,  # Write log files?
        'zmq_client_address': 'tcp://localhost:PORT',  # Make sure the port is unique to this device.
        'zmq_server_address': 'tcp://*:PORT',  # Make sure this port is matching the above.
        'zmq_daemon_pid_file': '/tmp/zmq_daemon_PORT.pid'  # This is for creating a daemon server, if desired.
    }
    if dummy:
        config['device_class'] = SensirionDeviceDummy
    if zmq_port is not None:
        config['zmq_client_address'] = config['zmq_client_address'].replace('PORT', zmq_port.__str__())
        config['zmq_server_address'] = config['zmq_server_address'].replace('PORT', zmq_port.__str__())
        config['zmq_daemon_pid_file'] = config['zmq_daemon_pid_file'].replace('PORT', zmq_port.__str__())
    return config


class SensirionClient(zmq_tools.GenericZMQClient):
    r""" ZMQ client for Sensirion flowmeters.  See GenericZMQClient superclass for more details and keyword
    arguments. """
    def __init__(self, config=None, **kwargs):
        r"""
        Arguments:
            config (dict): Configuration parameters.  Use make_config() to get a starting point.
            start_server (bool): Set to True if you want to start a server upon instantiation of the client.
        """
        if config is None:
            raise ValueError('Your config dictionary is None.  Use "make_config" and modify as appropriate.')
        super().__init__(config=config, **kwargs)
    def get_flow_rate(self):
        r""" Method is passed to Sensirion device class.  See Sensirion class documentation. """
        return self.get(method='get_flow_rate')


class SensirionServer(zmq_tools.GenericZMQServer):
    r""" ZMQ server for Sensirion flowmeters.  See GenericZMQServer superclass for more details and keyword
    arguments. """
    def __init__(self, config=None, **kwargs):
        r"""
        Arguments:
            config (dict): Configuration parameters.  Use make_config() to get a starting point.
            threaded (bool): Start the server in a thread to avoid blocking.
        """
        if config is None:
            raise ValueError('Your config dictionary is None.  Use "make_config" and modify as appropriate.')
        super().__init__(config=config, **kwargs)


class SensirionWidget(pg.Qt.QtWidgets.QWidget):
    r""" A for the Sensirion device class.  Creates a SensirionClient, which creates a SensirionServer, which
    creates a Sensirion instance. """
    def __init__(self, config):
        r"""
        Arguments:
            config (dict): Configurations.  TODO: Document.
        """
        self._dbgmsg('Initializing.')
        app = pg.mkQApp()
        app.aboutToQuit.connect(self.quit)
        self.app = app
        super().__init__()
        self.config = config
        self.buffer_idx = 0
        self.buffer_length = int(1e6)
        self.buffer_update_interval = 0.1
        self.plot_update_interval = 0.1
        self.is_initialized = False
        self.is_paused = False
        self.do_update_plot = True
        self.ring_buffer_y = np.zeros(self.buffer_length)
        self.ring_buffer_ys = np.zeros(self.buffer_length)
        self.ring_buffer_t = np.zeros(self.buffer_length)
        self.client = SensirionClient(config=config, start_server=True)
        self._setup_interface()

    def _dbgmsg(self, *args, **kwargs):
        print('SensirionWidget:', *args, **kwargs)

    def _setup_interface(self):

        main_layout = pg.Qt.QtWidgets.QVBoxLayout()
        # Setup plot window
        plt_layout = pg.Qt.QtWidgets.QHBoxLayout() #QGridLayout() for grid layout, or QSlider() for slider window thing
        plot = pg.PlotWidget()
        plot.setLabel('bottom', 'Relative Time (s)')
        plot.setLabel('left', 'Flow Rate (ul/min)')
        plot.setTitle('Sensirion Liquid Flowmeter (' + self.client.get('get_sensor_name') + ')')
        pg.setConfigOptions(antialias=True)
        curve = plot.plot(pen=None, symbol='o', symbolPen=None, symbolBrush='g')
        curves = plot.plot(pen=None, symbol='o', symbolPen=None, symbolBrush='r')
        plt_layout.addWidget(plot)
        main_layout.addItem(plt_layout)
        # Setup device status widget (row of widgets)
        btn_layout = pg.Qt.QtWidgets.QHBoxLayout()
        # Flow rate status
        btn_layout.addWidget(pg.Qt.QtWidgets.QLabel('Flow rate:'))
        self.flow_label = pg.Qt.QtWidgets.QLabel('0')
        btn_layout.addWidget(self.flow_label)
        # Timers for auto-updating things
        buffer_update_timer = pg.QtCore.QTimer()
        buffer_update_timer.timeout.connect(self._update_buffer)
        plot_update_timer = pg.QtCore.QTimer()
        plot_update_timer.timeout.connect(self._update_plot)
        self.setLayout(main_layout)
        self.layout = main_layout
        self.buffer_update_timer = buffer_update_timer
        self.plot_update_timer = plot_update_timer
        self.curve = curve
        self.curves = curves
        self.plot = plot

    def start(self, app_exec=False):
        r""" Start the GUI, make it visible. """
        self._dbgmsg('GUI: Starting application.')
        self.show()
        self.buffer_update_timer.start(int(self.buffer_update_interval * 1e3))
        self.plot_update_timer.start(int(self.plot_update_interval * 1e3))
        if app_exec:
            self.app.exec_()

    def _update_buffer(self):
        r""" Fetch data and update the internal memory buffer. """
        i = self.buffer_idx % self.buffer_length
        data = self.client.get(method='get_flow_rate')
        datas = self.client.get(method='get_smoothed_flow_rate')
        if data is None:
            self._dbgmsg('GUI: Requested data is None.')
            return
        self.ring_buffer_t[i] = data[0]
        self.ring_buffer_y[i] = data[1]
        self.ring_buffer_ys[i] = datas[1]
        self.buffer_idx += 1
        if datas[1] != None:
            self.flow_label.setText('%3.2f ul/min' % datas[1])
        else:
            print(f"Sensirion buffer returning {datas[1]}!")

    def _update_plot(self):
        r""" Update the plot. """
        if self.buffer_idx < 10:
            return
        t = self.ring_buffer_t
        y = self.ring_buffer_y
        ys = self.ring_buffer_ys
        if self.buffer_idx < self.buffer_length:  # Don't show garbage data values, or zeros.
            t = t[:self.buffer_idx]
            y = y[:self.buffer_idx]
            ys = ys[:self.buffer_idx]
        self.curve.setData(t - np.max(t), y)
        self.curves.setData(t - np.max(t), ys)
        if not self.is_initialized:
            self.plot.enableAutoRange('x', False)  # Stop auto-scaling after the first data set is plotted.
            self.plot.setXRange(-60, 0)
            self.plot.setLabel('bottom', 'Relative Time (s)')
            self.is_initialized = True

    def quit(self):
        if self.client.server is not None:
            self.client.server.stop()
