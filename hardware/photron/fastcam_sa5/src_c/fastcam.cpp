#include "fastcam.h"
#include "stdlib.h"

////////////////////////////////////////////////
// Main
////////////////////////////////////////////////

int main(){

    ULONG nRet, nErrCode;
    ULONG CamID = 999;
    ULONG IP_Found;
    ULONG IP_Desired = 0xC0A8000A;  /* 192.168.0.10 */
    ULONG IPList[PDC_MAX_DEVICE];  /* IP address being searched */
    PDC_DETECT_NUM_INFO DetectNumInfo;  /* Search result */
    IPList[0] = IP_Desired;
//    IPList[0] = 0xC0A80000;

    nRet = PDC_Init(&nErrCode);
    S_check_error(nRet, nErrCode, "PDC_Init");

    nRet = PDC_DetectDevice(PDC_INTTYPE_G_ETHER,  /* Gigabit-Ether I/F */
                            IPList,               /* IP address */
                            1,                    /* Maximum number of searched devices */
                            PDC_DETECT_NORMAL,    /* Specifies an IP address explicitly */
                            &DetectNumInfo,
                            &nErrCode);

    S_check_error(nRet, nErrCode, "PDC_DetectDevice");

    IP_Found = DetectNumInfo.m_DetectInfo[0].m_nTmpDeviceNo;

    printf( "\t\t IP Found    : %lu.%lu.%lu.%lu \n\n",
            (IP_Found & 0xFF000000) >> 24,
            (IP_Found & 0x00FF0000) >> 16,
            (IP_Found & 0x0000FF00) >> 8,
             IP_Found & 0x000000FF);

//    if (DetectNumInfo.m_nDeviceNum == 0) {
//        printf("\t!!! PDC_DetectDevice didn't find a device at this IP!\n");
//        return 1;
//    }

//    if (IP_Found != IPList[0]) {
//
//        printf("\t!!! PDC_DetectDevice found a device but with a different IP!\n\n");
//
//        printf( "\t\t IP Desired  : %lu.%lu.%lu.%lu \n",
//                (IP_Desired & 0xFF000000) >> 24,
//                (IP_Desired & 0x00FF0000) >> 16,
//                (IP_Desired & 0x0000FF00) >> 8,
//                 IP_Desired & 0x000000FF);
//
//        printf( "\t\t IP Found    : %lu.%lu.%lu.%lu \n\n",
//                (IP_Found   & 0xFF000000) >> 24,
//                (IP_Found   & 0x00FF0000) >> 16,
//                (IP_Found   & 0x0000FF00) >> 8,
//                 IP_Found   & 0x000000FF);
//
//    }

    printf("%lu", CamID);
    return 0;
}

//int main()
//{
//    S_Init();
//    ULONG CamID = S_Connect();
//    printf("%lu", CamID);
////    S_Run_Simplest(CamID);           // g_CamID
////    S_Run(CamID);                       // g_CamID
////    S_CloseDevice(CamID);               // g_CamID
//
//    return 0;
//}

////////////////////////////////////////////////
// Execution Sequence
////////////////////////////////////////////////

void S_Run_Simplest(ULONG CamID)
{
    printf("\tGetting Frame Rate\n");
    printf("\t\tFrame Rate (fps) = %lu\n", S_GetRecordRate(CamID));
    
    printf("\tSetting Frame Rate\n");
    
    S_SetRecordRate(CamID, 10000);
    printf("\t\tFrame Rate (fps) = %lu\n", S_GetRecordRate(CamID));
    
    S_SetRecordRate(CamID, 3600);
    printf("\t\tFrame Rate (fps) = %lu\n", S_GetRecordRate(CamID));
}

void S_Run(ULONG CamID)
{
    ;
}

////////////////////////////////////////////////
// Getters' "Main"
////////////////////////////////////////////////

void S_main_getters(ULONG CamID)
{
    // get all params here

    // S_GetExternalInMode(CamID, 1);     // 1,2,3 = SyncIn, GenIn, TrigIn  
    // S_GetTriggerMode(CamID);
    // S_GetShadingMode(CamID);
}

////////////////////////////////////////////////
// Setters' "Main"
////////////////////////////////////////////////

void S_main_setters(ULONG CamID)
{
    // Set all params here

    S_SetBurstTransfer(CamID);
    // printf("------- PDC_EXT_IN_SYNC_POS %lu \n", PDC_EXT_IN_SYNC_POS);
    S_SetExternalInMode(CamID, 1, PDC_EXT_IN_OTHERSSYNC_POSI);     // Sync In   = On Others Pos
    S_SetTriggerMode(CamID, PDC_TRIGGER_END, 0, 0, 0);    // Trig Mode = END
    // S_SetShadingMode(CamID);
}

////////////////////////////////////////////////
// Routines
////////////////////////////////////////////////

////////////////
// Device Setup 
////////////////

void S_Init(void)     
{
    // DLLs: Windows (on Linux, use 'LD_PRELOAD' instead of 'SetDllDirectory')
    if (0 == SetDllDirectory("../sdk/Dll/64bit(x64)")) {
        printf("Windows: SetDllDirectory failed!");}

    ULONG nRet, nErrCode;

    nRet = PDC_Init(&nErrCode);
    S_check_error(nRet, nErrCode, "PDC_Init");
}

ULONG S_Connect(void)
{
    ULONG nRet, nErrCode;
    ULONG CamID = 0;
    // ULONG nChildNo  = 1;
    ULONG IP_Found, IP_Desired= 0xC0A8000A; /* 192.168.0.10 */
    ULONG IPList[PDC_MAX_DEVICE];           /* IP address being searched */
    PDC_DETECT_NUM_INFO DetectNumInfo;      /* Search result */
    IPList[0] = IP_Desired;                         
    
    nRet = PDC_DetectDevice(PDC_INTTYPE_G_ETHER,    /* Gigabit-Ether I/F */
                            IPList,                 /* IP address */
                            1,                      /* Maximum number of searched devices */
                            PDC_DETECT_NORMAL,      /* Specifies an IP address explicitly */
                            &DetectNumInfo,
                            &nErrCode);

    S_check_error(nRet, nErrCode, "PDC_DetectDevice");
    
    IP_Found = DetectNumInfo.m_DetectInfo[0].m_nTmpDeviceNo;

    if (DetectNumInfo.m_nDeviceNum == 0) {
        printf("\t!!! PDC_DetectDevice didn't find a device at this IP!\n"); 
        return 1;
    }

    if (IP_Found != IPList[0]) {

        printf("\t!!! PDC_DetectDevice found a device but with a different IP!\n\n"); 
        
        printf( "\t\t IP Desired  : %lu.%lu.%lu.%lu \n",
                (IP_Desired & 0xFF000000) >> 24,
                (IP_Desired & 0x00FF0000) >> 16,
                (IP_Desired & 0x0000FF00) >> 8,
                 IP_Desired & 0x000000FF);

        printf( "\t\t IP Found    : %lu.%lu.%lu.%lu \n\n",
                (IP_Found   & 0xFF000000) >> 24,
                (IP_Found   & 0x00FF0000) >> 16,
                (IP_Found   & 0x0000FF00) >> 8,
                 IP_Found   & 0x000000FF);
        
        return 1;
    }
    
    nRet = PDC_OpenDevice(&(DetectNumInfo.m_DetectInfo[0]), /* detected device */
                          &CamID, 
                          &nErrCode);

    S_check_error(nRet, nErrCode, "PDC_OpenDevice");
    // g_CamID = CamID;

    return CamID;
}

void S_CloseDevice(ULONG CamID)
{
    ULONG nRet, nErrCode;
    nRet = PDC_CloseDevice(CamID, &nErrCode); 
    // S_check_error(nRet, nErrCode, "PDC_CloseDevice");
}

//////////
// Misc
//////////

void S_check_status(ULONG CamID, const char* PDC_status="", const char* S_fx="")
{
    ULONG nStatus = S_GetStatus(CamID);

    if (PDC_status && !PDC_status[0])    // Equiv: if ((PDC_status != NULL) && (PDC_status[0] == '\0'))
    {
        static char fmt[] = "\n\t\t ---- PDC Current Status: %s \n\n";
        switch (nStatus)
        {
            case  0: printf(fmt, "LIVE"       ); break;
            case  1: printf(fmt, "PLAYBACK"   ); break; 
            case  2: printf(fmt, "RECREADY"   ); break; 
            case  4: printf(fmt, "ENDLESS"    ); break;
            case  8: printf(fmt, "REC"        ); break;
            case 16: printf(fmt, "SAVE"       ); break;
            case 32: printf(fmt, "LOAD"       ); break;
            default: printf(fmt, "unknown!"   ); break;
        }
    }
}

void S_check_error(ULONG nRet, ULONG nErrCode, const char* PDC_fx) 
{
	// To find C map between string and unsigned long PDC_Error codes (not explicitly mentioned in SDK manual): 

	static char fmt[] = "\t %17s | %s \n";

    if (nRet == PDC_FAILED) {
        printf("\n\n !!!!!!!!!!!!!!!!! %s Error:  %lu !!!!!!!!!!!!!!!!!\n\n", PDC_fx, nErrCode);
		switch ( nErrCode ) {
		case PDC_ERROR_NOERROR           : printf(fmt,"NOERROR"           , "No error. Normal!") ; break;
		case PDC_ERROR_UNINITIALIZE      : printf(fmt,"UNINITIALIZED"     , "PDCLIB not initialized.") ; break;
		case PDC_ERROR_ILLEGAL_DEV_NO    : printf(fmt,"ILLEGAL_DEV_NO"    , "Invalid Device Num.") ; break;
		case PDC_ERROR_ILLEGAL_CHILD_NO  : printf(fmt,"ILLEGAL_CHILD_NO"  , "Invalid child Device Num.") ; break;
		case PDC_ERROR_ILLEGAL_VALUE     : printf(fmt,"ILLEGAL_VALUE"     , "Invalid Fx Arg.") ; break;
		case PDC_ERROR_ALLOCATE_FAILED   : printf(fmt,"ALLOCATE_FAILED"   , "Mem Allocation failed.") ; break;
		case PDC_ERROR_INITIALIZED       : printf(fmt,"INITIALIZED"       , "PDCLIB already initialized.") ; break;
		case PDC_ERROR_NO_DEVICE         : printf(fmt,"NO_DEVICE"         , "Device not found.") ; break;
		case PDC_ERROR_TIMEOUT           : printf(fmt,"TIMEOUT"           , "Process Timed out.") ; break;
		case PDC_ERROR_FUNCTION_FAILED   : printf(fmt,"FUNCTION_FAILED"   , "Fx Exec Failed.") ; break;
		case PDC_ERROR_FILEREAD_FAILED   : printf(fmt,"FILEREAD_FAILED"   , "File-read failed.") ; break;
		case PDC_ERROR_FILEWRITE_FAILED  : printf(fmt,"FILEWRITE_FAILED"  , "File-write Failed.") ; break;
		case PDC_ERROR_NOT_SUPPORTED     : printf(fmt,"NOT_SUPPORTED"     , "Unsupported in this child device.") ; break;
		case PDC_ERROR_DROP_FRAME        : printf(fmt,"DROP_FRAME"        , "Frame not captured.") ; break;
		case PDC_ERROR_FILE_OPEN_ALREADY : printf(fmt,"FILE_OPEN_ALREADY" , "File already opened.") ; break;
		case PDC_ERROR_FILE_NOTOPEN      : printf(fmt,"FILE_NOTOPEN"      , "File not opened yet.") ; break;
		case PDC_ERROR_LOAD_FAILED       : printf(fmt,"LOAD_FAILED"       , "Failed to load module.") ; break;
		case PDC_ERROR_FREE_FAILED       : printf(fmt,"FREE_FAILED"       , "Failed to open module.") ; break;
		case PDC_ERROR_LOADED            : printf(fmt,"LOADED"            , "Module already loaded.") ; break;
		case PDC_ERROR_NOTLOADED         : printf(fmt,"NOTLOADED"         , "Module not loaded.") ; break;
		case PDC_ERROR_UNDETECTED        : printf(fmt,"UNDETECTED"        , "A device search has not been performed.") ; break;
		case PDC_ERROR_OVER_DEVICE       : printf(fmt,"OVER_DEVICE"       , "number of devices is too large.") ; break;
		case PDC_ERROR_INIT_FAILED       : printf(fmt,"INIT_FAILED"       , "Init failed.") ; break;
		case PDC_ERROR_OPEN_ALREADY      : printf(fmt,"OPEN_ALREADY"      , "Device already opened.") ; break;
		case PDC_ERROR_NOTOPEN           : printf(fmt,"NOTOPEN"           , "Device not opened.") ; break;
		case PDC_ERROR_LIVEONLY          : printf(fmt,"LIVEONLY"          , "Fx Invalid in live mode.") ; break;
		case PDC_ERROR_PLAYBACKONLY      : printf(fmt,"PLAYBACKONLY"      , "Fx Invalid in memory playback mode.") ; break;
		case PDC_ERROR_SEND_ERROR        : printf(fmt,"SEND_ERROR"        , "Command transmission failed.") ; break;
		case PDC_ERROR_CLEAR_ERROR       : printf(fmt,"CLEAR_ERROR"       , "Interface re-init failed.") ; break;
		case PDC_ERROR_COMMAND_ERROR     : printf(fmt,"COMMAND_ERROR"     , "Abnormal command.") ; break;
		case PDC_ERROR_STORAGE_NO_CARD   : printf(fmt,"STORAGE_NO_CARD"   , "Storage: Unconnected.") ; break;
		case PDC_ERROR_STORAGE_WRITE     : printf(fmt,"STORAGE_WRITE"     , "Storage: write failed.") ; break;
		case PDC_ERROR_STORAGE_READ      : printf(fmt,"STORAGE_READ"      , "Storage: read  failed.") ; break;
		case PDC_ERROR_STORAGE_SIZE      : printf(fmt,"STORAGE_SIZE"      , "Storage: full") ; break;
		case PDC_ERROR_STORAGE_FORMAT    : printf(fmt,"STORAGE_FORMAT"    , "Storage: format failed.") ; break;
		// case PDC_ERROR_RECIEVE_ERROR  : printf(fmt,"RECIEVE_ERROR"     , "Command reception failed.") ; break;
		// case PDC_ERROR_ILLEGAL_MODE   : printf(fmt,"ILLEGAL_MODE"      , "Invalid Mode: live memory playback or low-light") ; break;

		default: printf("\n\t!!! Unknown Error Code %lu\n\n", nErrCode);
		}
        return;
    }
}


//////////////////////////////////
// Setters
//////////////////////////////////

void S_SetStatus_Live(ULONG CamID) {S_SetStatus(CamID, PDC_STATUS_LIVE);}
void S_SetStatus_Play(ULONG CamID) {S_SetStatus(CamID, PDC_STATUS_PLAYBACK);}
void S_SetStatus(ULONG CamID, ULONG status)
{
    // Resource intensive. Use Wisely.

    ULONG nRet, nErrCode, nStatus; 
    nRet = PDC_GetStatus(CamID, &nStatus, &nErrCode);
    S_check_error(nRet, nErrCode, "PDC_GetStatus");    

    if (nStatus != status) {
        nRet = PDC_SetStatus(CamID, status, &nErrCode);
        S_check_error(nRet, nErrCode, "PDC_SetStatus");   
    }
}

void S_SetBurstTransfer(ULONG CamID)
{
    // TRY to enable burst transfer 

    ULONG nRet, nErrCode;
    char  cStatus;

    nRet = PDC_IsFunction(CamID,1,PDC_EXIST_BURST_TRANSFER,&cStatus,&nErrCode);           
    
    S_check_error(nRet, nErrCode, "PDC_IsFunction");  
    
    if (cStatus == PDC_EXIST_SUPPORTED) {
        nRet = PDC_SetBurstTransfer(CamID, PDC_FUNCTION_ON, &nErrCode);
        S_check_error(nRet, nErrCode, "PDC_SetBurstTransfer");}
}

void S_SetShutterSpeedFps(ULONG CamID, ULONG nVal)
{    
    // TODO: check if desired value is valid
    // c.f. Photron SDK: PDC_SetShutterLockMode(nMode = PDC_SHUTTERLOCK_MODE1)
   
    ULONG nRet, nErrCode;
    S_SetStatus_Live(CamID);        
    nRet = PDC_SetShutterSpeedFps(CamID, 1, nVal, &nErrCode); 
    S_check_error(nRet, nErrCode, "PDC_SetShutterSpeedFps"); 
}

void S_SetMinShutterSpeedFps(ULONG CamID)
{
    ULONG nCount;
    ULONG * V = S_GetShutterSpeedFpsList(CamID, &nCount, false); 
    if (V) { S_SetShutterSpeedFps(CamID, V[0]); }
}

void S_SetRecordRate(ULONG CamID, ULONG nVal)
{
    // TODO: check if desired value is valid

    ULONG nRet, nErrCode;
    S_SetStatus_Live(CamID);        
    nRet = PDC_SetRecordRate(CamID, 1, nVal, &nErrCode); 
    S_check_error(nRet, nErrCode, "PDC_SetRecordRate");
}

void S_SetResolution(ULONG CamID, ULONG nWidth, ULONG nHeight)
{
    // TODO: check if desired value is valid

    ULONG nRet, nErrCode;
    S_SetStatus_Live(CamID);        
    nRet = PDC_SetResolution(CamID, 1, nWidth, nHeight, &nErrCode); 
    S_check_error(nRet, nErrCode, "PDC_SetResolution");
//    printf("\n\t S_SetResolution (HxW): %lu x %lu\n", nHeight, nWidth);
}

void S_SetMaxResolution(ULONG CamID)
{
    ULONG nHeight, nWidth;     
    S_GetMaxResolution( CamID, &nWidth, &nHeight);
    S_SetResolution(    CamID,  nWidth,  nHeight);
}

void S_SetBitDepth(ULONG CamID, char nVal)
{
    // TODO: check if desired value is valid

    ULONG nRet, nErrCode;
    S_SetStatus_Live(CamID);        
    nRet = PDC_SetBitDepth(CamID, 1, nVal, &nErrCode); 
    S_check_error(nRet, nErrCode, "PDC_SetBitDepth");    
}

void S_SetExternalInMode(ULONG CamID, ULONG nExtInPortNo, ULONG nMode)
{
    // S_SetStatus_Live(CamID);

    ULONG nRet, nErrCode;
    nRet = PDC_SetExternalInMode(CamID, nExtInPortNo, nMode, &nErrCode);
    S_check_error(nRet, nErrCode, "PDC_SetExternalInMode");  

    /* 
    // Wait up to  180000 ms = 3 min while camera struggles!

    bool    isDone = false; 
    clock_t T0, T1;
    T0 = clock();

    while (clock() < (T0 + 180000))
    {
        T1 = clock();
        while(clock() < (T1 + 3000)) { ; }

        printf("\n\t ---- Status ---- \n");

        if (S_GetStatus(CamID) != PDC_STATUS_SAVE)
        {
            isDone = true;     
            break;
        } 
    }
    */
}

void S_SetTriggerMode(ULONG CamID, ULONG nMode, ULONG nNumFramesAfterTrigger, ULONG nNumFramesPerSingleRandomTrigger, ULONG nNumRecordingTimes)
{
    ULONG nRet, nErrCode;
    nRet = PDC_SetTriggerMode(
                CamID, 
                nMode, 
                nNumFramesAfterTrigger,             // Only for "manual" and "random manual" trigger modes. Includes trigger frame
                nNumFramesPerSingleRandomTrigger, 
                nNumRecordingTimes,                 // Only for "random center" and "random manual" trigger modes 
                &nErrCode);

    S_check_error(nRet, nErrCode, "PDC_SetTriggerMode");    
} 

void S_SetShadingMode(ULONG CamID)
{
    ULONG nRet, nErrCode;

    // nRet = PDC_SetShadingType(CamID,1,PDC_SHADING_TYPE_FINE,&nErrCode);
    // S_check_error(nRet, nErrCode, "PDC_SetShadingType");    
    nRet = PDC_SetShadingMode(CamID,1,PDC_SHADING_OFF,&nErrCode);
    nRet = PDC_SetShadingMode(CamID,1,PDC_SHADING_ON ,&nErrCode);
    S_check_error(nRet, nErrCode, "PDC_SetShadingMode");    
    
    /*
    // Optional: save shading data (can take minutes!)
    
    nRet = PDC_SetShadingMode(CamID, 1, PDC_SHADING_SAVE, &nErrCode);
    S_check_error(nRet, nErrCode, "PDC_SetShadingMode");    
    
    // Wait up to  180000 ms = 3 min while camera saves shading data
    bool    isDone = false;  
    clock_t T0 = clock();

    while (clock() < (T0 + 180000))
    {
        // Sleep(3);
        if (S_GetStatus(CamID) != PDC_STATUS_SAVE) {
            isDone = true;     
            break; 
        }
    }
    
    if (!isDone) {printf("\t!!! S_SetShadingMode Error: Timeout While Waiting for Camera to Save Shading Data");}
    */
}


//////////////////////////////////
// Get Current Settings 
//////////////////////////////////

ULONG S_GetStatus(ULONG CamID)
{
    ULONG nRet, nErrCode, nVal;

    nRet = PDC_GetStatus(CamID,
                         &nVal,
                         &nErrCode);

    S_check_error(nRet, nErrCode, "PDC_GetStatus");   

    return nVal;
}

char S_GetBitDepth(ULONG CamID)
{

    ULONG nRet, nErrCode;  
    char  nVal;

    nRet = PDC_GetBitDepth(CamID, 1, &nVal, &nErrCode); 
    S_check_error(nRet, nErrCode, "PDC_GetBitDepth");   

    return nVal; 
}

ULONG S_GetRecordRate(ULONG CamID)
{
    ULONG nRet, nErrCode, nVal;     

    nRet = PDC_GetRecordRate(CamID, 1, &nVal, &nErrCode);
    S_check_error(nRet, nErrCode, "PDC_GetRecordRate");

    return nVal;
}

ULONG S_GetShutterSpeedFps(ULONG CamID)
{
    ULONG nRet, nErrCode, nVal;     

    nRet = PDC_GetShutterSpeedFps(CamID, 1, &nVal, &nErrCode);
    S_check_error(nRet, nErrCode, "PDC_GetShutterSpeedFps");

    return nVal;
}

void S_GetResolution(ULONG CamID, ULONG* height, ULONG* width)
{
    ULONG nRet, nErrCode;     

    nRet = PDC_GetResolution(CamID, 1, width, height, &nErrCode);
    S_check_error(nRet, nErrCode, "PDC_GetResolution");
}

void S_GetMaxResolution(ULONG CamID, ULONG* height, ULONG* width)
{
    ULONG nRet, nErrCode;     

    nRet = PDC_GetMaxResolution(CamID, 1, width, height, &nErrCode);
    S_check_error(nRet, nErrCode, "PDC_GetMaxResolution");
}

ULONG S_GetExternalInMode(ULONG CamID, ULONG nExtInPortNum)
{
    S_SetStatus_Live(CamID);

    ULONG nRet, nErrCode, nVal;
        
    nRet = PDC_GetExternalInMode(   CamID,
                                    nExtInPortNum,
                                    &nVal,
                                    &nErrCode);
            
    S_check_error(nRet, nErrCode, "PDC_GetExternalInMode");    

    return nVal;
}

/*
ULONG S_GetTriggerMode(ULONG CamID)
{
    // nNumFramesAfterTrigger   Only for "manual" and "random manual" trigger modes. Includes trigger frame
    // nNumRecordingTimes       Only for "random center" and "random manual" trigger modes                  

    ULONG nRet, nErrCode;

    nRet = PDC_GetTriggerMode(
                CamID, 
                nMode, 
                nNumFramesAfterTrigger, 
                nNumFramesPerSingleRandomTrigger, 
                nNumRecordingTimes, 
                &nErrCode);

    S_check_error(nRet, nErrCode, "PDC_GetTriggerMode");    
} 

ULONG S_GetShadingMode(ULONG CamID)
{
    ULONG nRet, nErrCode, nStatus;

    nRet = PDC_GetShadingMode(CamID,
                              1, // nChildNo
                              PDC_SHADING_ON,
                              &nErrCode);

    S_check_error(nRet, nErrCode, "PDC_GetShadingMode");    
    
    nRet = PDC_GetShadingMode(CamID,
                              1, // nChildNo
                              PDC_SHADING_SAVE,     // Save shading data to camera 
                              &nErrCode);
    
    S_check_error(nRet, nErrCode, "PDC_GetShadingMode");    

    do {
        // Wait while camera saves shading data
        sleep(1);  // 1 sec
        nRet = PDC_GetStatus(CamID,
                             &nStatus,
                             &nErrCode);

        S_check_error(nRet, nErrCode, "PDC_GetStatus");

    }while( nStatus == PDC_STATUS_SAVE );
}
*/

//////////////////////////////////
// Get Valid Settings 
//////////////////////////////////

ULONG * S_GetRecordRateList(ULONG CamID, ULONG * nCount, bool bVerbose=false)
{
    /*  Rerturn: ULONG array pointer
        
        Caller responsible for de-allocating 
        the dynamically allocated mem seq 'Valids' 
       
        Call like this:
            
            bool   verbose  = true;
            ULONG  Count    = 0;
            ULONG* V        = S_Get<param>List(CamID, &Count, verbose);

            if (V) {
                // Do stuff;
                free(V);
            }
    */

    ULONG  nRet, nErrCode; 
    ULONG  ValidsList[PDC_MAX_LIST_NUMBER];  // Not returned. Allocated locally w/ auto-duration.
    ULONG* Valids;
    size_t validsLen;

    nRet = PDC_GetRecordRateList(CamID, 1, nCount, ValidsList, &nErrCode);
    S_check_error(nRet, nErrCode, "PDC_GetRecordRateList");
    
    validsLen = (*nCount)*sizeof(*nCount);
    Valids    = (ULONG*)malloc(validsLen);   // Dynamically allocated & returned (pointer). Caller must de-alloc.
    
    if (!Valids) {return NULL;}
    bVerbose && printf("\n\t--- Valid Record Rates ---\n\n");

    for (ULONG i=0; i < *nCount; i++) {
        Valids[i] = ValidsList[i];      
        bVerbose && printf("\t\t %3lu : %lu\n", i, ValidsList[i]);
    }

    return Valids;
}

ULONG * S_GetShutterSpeedFpsList(ULONG CamID, ULONG * nCount, bool bVerbose=false)
{
    /*  Rerturn: ULONG array pointer
        
        Caller responsible for de-allocating 
        the dynamically allocated mem seq 'Valids' 
       
        Call like this:
            
            bool   verbose  = true;
            ULONG  Count    = 0;
            ULONG* V        = S_Get<param>List(CamID, &Count, verbose);

            if (V) {
                // Do stuff;
                free(V);
            }
    */

    ULONG  nRet, nErrCode; 
    ULONG  ValidsList[PDC_MAX_LIST_NUMBER];  // Not returned. Allocated locally w/ auto-duration.
    ULONG* Valids;
    size_t validsLen;

    nRet = PDC_GetShutterSpeedFpsList(CamID, 1, nCount, ValidsList, &nErrCode);
    S_check_error(nRet, nErrCode, "PDC_GetShutterSpeedFpsList");
    
    validsLen = (*nCount)*sizeof(*nCount);
    Valids    = (ULONG*)malloc(validsLen);   // Dynamically allocated & returned (pointer). Caller must de-alloc.

    if (!Valids) {return NULL;}
    bVerbose  && printf("\n\t--- Valid Shutter Speeds (FPS) ---\n\n");

    for (ULONG i=0; i < *nCount; i++) {
        Valids[i] = ValidsList[i];      
        bVerbose && printf("\t\t %3lu : %lu\n", i, ValidsList[i]);
    }

    return Valids;
}

ULONG * S_GetResolutionList(ULONG CamID, ULONG * nCount, bool bVerbose=false)
{
    /*  Rerturn: ULONG array pointer
        
        Caller responsible for de-allocating 
        the dynamically allocated mem seq 'Valids' 
       
        Call like this:
            
            bool   verbose  = true;
            ULONG  Count    = 0;
            ULONG* V        = S_Get<param>List(CamID, &Count, verbose);

            if (V) {
                // Do stuff;
                free(V);
            }
    */

    ULONG  nRet, nErrCode; 
    ULONG  ValidsList[PDC_MAX_LIST_NUMBER];  // Not returned. Allocated locally w/ auto-duration.
    ULONG* Valids;
    size_t validsLen;

    nRet = PDC_GetResolutionList(CamID, 1, nCount, &ValidsList[0], &nErrCode);
    S_check_error(nRet, nErrCode, "PDC_GetResolutionList");
    
    validsLen = (*nCount)*sizeof(*nCount);
    Valids    = (ULONG*)malloc(2*validsLen);   // Dynamically allocated & returned (pointer). Caller must de-alloc.
    
    if (!Valids) {return NULL;}
    bVerbose && printf("\n\t--- Valid Resolutions ---\n\n");

    for (ULONG i=0; i < *nCount; i++) {
        Valids[2*i]   = (ValidsList[i] & 0xffff0000) >> 16;     // width  = upper 16 bits 
        Valids[2*i+1] = (ValidsList[i] & 0x0000ffff);           // height = lower 16 bits 

        bVerbose && printf("\t\t %3lu : %4lu x %4lu \n", i, Valids[2*i], Valids[2*i+1]);
    }

    return Valids;
}

ULONG * S_GetExternalInModeList(ULONG CamID, ULONG nExtInPortNum, ULONG * nCount, bool bVerbose)
{
    /*  Rerturn: ULONG array pointer
        
        Caller responsible for de-allocating 
        the dynamically allocated mem seq 'Valids' 
       
        Call like this:
            
            bool   verbose  = true;
            ULONG  Count    = 0;
            ULONG* V        = S_Get<param>List(CamID, &Count, verbose);

            if (V) {
                // Do stuff;
                free(V);
            }
    */

    ULONG  nRet, nErrCode; 
    ULONG  ValidsList[PDC_MAX_LIST_NUMBER];  // Not returned. Allocated locally w/ auto-duration.
    ULONG* Valids;
    size_t validsLen;

    nRet = PDC_GetExternalInModeList(CamID, 1, nCount, ValidsList, &nErrCode);
    S_check_error(nRet, nErrCode, "PDC_GetExternalInModeList");
    
    validsLen = (*nCount)*sizeof(*nCount);
    Valids    = (ULONG*)malloc(validsLen);   // Dynamically allocated & returned (pointer). Caller must de-alloc.

    if (!Valids) {return NULL;}
    bVerbose  && printf("\n\t--- Valid External In Modes for Port %lu ---\n\n", nExtInPortNum);

    for (ULONG i=0; i < *nCount; i++) {
        Valids[i] = ValidsList[i];      
        bVerbose && printf("\t\t %3lu : %lu\n", i, ValidsList[i]);
    }

    return Valids;
}   

//////////////////////////////////
// Record
//////////////////////////////////

void S_SetRecReady(ULONG CamID)
{
    /*  
        Will not ...

            * change trigger mode 
            * use ENDLESS recording 
            * handle halting of recording
            
        Will ...

            * change cam status to RECREADY or REC or ENDLESS
            * wait for an external trigger
        
        Is ...

            * memory limited. 
            * usable with "non-endless" trigger modes (e.g. MANUAL, RANDOM, etc.) 
    
        Nota Bene
        =========

            Although reasonable and tempting, don't assume that the 
            camera (in "RECREADY" or "ENDLESS" status) will start recording 
            *new* frames within any reasonable time frame (e.g. 2 seconds)
            after issuing an external or internal trigger. 

            After triggering (in/external), don't simply confirm a status of 
            "RECREADY" or "REC" or "ENDLESS" (loose disjunctive syllogism).
            Rather, confirm a status of "REC" -- exlusively.

            Reason: 
                
                For unknown reasons, after issuing a manual TriggerIn to start recroding, 
                the camera may fail to enter REC state (actual recording) in a timely manner (e.g. 2 seconds),
                and thus remain in RECREADY (or ENDLESS) state (not actually recording *new* frames).

                Unless we explicitly inquire about this, this situation will go undetected (b/c it's normal!).

                However, any frames we download afterwards will be *old* frames already in 
                the camera's memory from the previous run/experiment. This operation will 
                also execute normally and w/o any erorr (again, b/c everything is normal!).

                And ... assuming we're looking at new data, we'll end up analyzing *old* frames, and we'll never know!
    */    

    // printf("\n\t ---- S_SetRecReady ---- \n");

    ULONG  nRet, nErrCode;      
    nRet = PDC_SetRecReady(CamID, &nErrCode);
    S_check_error(nRet, nErrCode, "PDC_SetRecReady");   
    S_verify_recording_ready(CamID); 
}

void S_verify_recording_ready(ULONG CamID)
{
    // Confirm camera status change to          REC or ENDLESS or RECREADY 
    // Looser condition than actual recording:  REC or ENDLESS 

    ULONG       nStatus;  
    bool        isDone  = false;      
    clock_t     T0      = clock(); 

    while (clock() < (T0 + STATUS_CHECK_TIMEOUT))   
    {
        nStatus = S_GetStatus(CamID);

        if ((nStatus == PDC_STATUS_RECREADY) ||
            (nStatus == PDC_STATUS_REC))     // (nStatus == PDC_STATUS_ENDLESS)
        {
            isDone = true;
            break;
        }
    }
    
    if (!isDone) {
        printf("\n\t!!! S_verify_recording_ready Error: Timeout While Waiting for Camera to Enter REC, RECREADY, ENDLESS\n");
        S_check_status(CamID, "", "");
    }
}


void S_verify_recording(ULONG CamID)
{
    // Confirm actual recording state == REC or ENDLESS. (RECREADY is not enough!)

    ULONG       nStatus;  
    bool        isDone  = false;      
    clock_t     T0      = clock();    

    while (clock() < (T0 + STATUS_CHECK_TIMEOUT))   
    {
        nStatus = S_GetStatus(CamID);

        if ((nStatus == PDC_STATUS_ENDLESS) ||     // when trig mode == START  && status == "REC" or "RECREADY"  (using PDC_SetRecReady)
            (nStatus == PDC_STATUS_REC))           // when trig mode != START  && status == "ENDLESS"            (using PDC_SetEndless)
        {
            isDone = true;
            break;
        }
    }
    
    if (!isDone) {
        printf("\n\t!!! S_verify_recording Error: Timeout While Waiting for Camera to Enter REC or ENDLESS\n");
        S_check_status(CamID, "", "");
    }    
}

void S_start_recording(ULONG CamID)
{
    /* 
        Issues internal trigger to start recording if ...

            1) trig == "START"            && status == "REC" or "RECREADY"  (using PDC_SetRecReady)
            2) trig == "CENTER" or "END"  && status == "ENDLESS"            (using PDC_SetEndless)

        c.f. 'Nota Bene' under 'S_SetRecReady()'      

        Use PDC_SetStatus to terminate recording
    */

    S_SetRecReady(CamID);
    S_TriggerIn(CamID);
}


void S_TriggerIn(ULONG CamID)
{
    // printf("\n\t ---- S_TriggerIn ---- \n");

    ULONG   nRet, nErrCode;  

    nRet = PDC_TriggerIn(CamID, &nErrCode);                 // Start Recording
    S_check_error(nRet, nErrCode, "PDC_TriggerIn");

    S_verify_recording(CamID);

    /*
    bool    isDone  = false;      
    clock_t T0      = clock();    

    while (clock() < (T0 + STATUS_CHECK_TIMEOUT))   
    {
        nStatus = S_GetStatus(CamID);

        if ((nStatus != PDC_STATUS_RECREADY) &&    
            (nStatus != PDC_STATUS_REC))            
        {
            S_check_status(CamID, "", "");
            isDone = true;
            break;
        }
    }
    
    if (!isDone) {
        printf("\n\t!!! S_TriggerIn Error: Timeout While Waiting for Camera to Exit RECREADY or REC \n");
        S_check_status(CamID, "", "");
    }
    */
}

void S_start_recording_endless(ULONG CamID)        
{
    /* This function ...
        * starts    recording endlessly (limited by memory).
        * sets      trigger mode to END 
        * sets      device recording status to ENDLESS
        * issues    use an internal trigger (i.e. PDC_TriggerIn). Instead it could wait for an EXTERNAL trigger.
        * ignores   termination of recording or exiting recording mode 
    */

    /* Endless Trigger/Recording Process:
    
            0) PDC_SetStatus:       Ensure PDC_STATUS_LIVE
            1) PDC_SetTriggerMode:  Trigger modes that support endless triggering/recording: START, CENTER, END
            2) PDC_SetRecReady:     Ensures PDC_STATUS_RECREADY
            3) PDC_SetEndless:      Starts recording and causes PDC_STATUS_ENDLESS or PDC_STATUS_REC (prereq for internal/external trigger detection)
            4) PDC_TriggerIn        (i.e. internal trigger) or external trigger input
       
       Once recording has finished (forcibly or due to insufficient memory), device will NO longer have ENDLESS or REC status
       
       Endless trigger modes:
            * START:                Does NOT require PDC_SetEndless
            * CENTER:               Requires PDC_SetEndless. Offers less than half the largest number of recordable frames
            * END:                  Requires PDC_SetEndless. Offers largest number of recordable frames
    */
    
    ULONG nRet, nErrCode;  

    S_SetTriggerMode(CamID, PDC_TRIGGER_END, 0, 0, 0);      // Trig Mode = END
    S_SetRecReady(CamID);                                   // prep
    
    nRet = PDC_SetEndless(CamID, &nErrCode);                // status = ENDLESS
    S_check_error(nRet, nErrCode, "PDC_SetEndless");  
    S_verify_recording(CamID);    

    S_TriggerIn(CamID);
}

void S_stop_recording(ULONG CamID)
{
    // Halt recording forcibly by setting status to "LIVE"
    
    S_SetStatus_Live(CamID);
    // S_check_status(CamID, "LIVE", "S_stop_recording");

    bool    isDone  = false;
    clock_t T0      = clock();

    while (clock() < (T0 + STATUS_CHECK_TIMEOUT))
    {
        if (S_GetStatus(CamID) == PDC_STATUS_LIVE) {
            isDone = true;     
            break; 
        }
    }
    
    if (!isDone) {
        printf("\n\t!!! S_stop_recording Error: Timeout While Waiting for Camera to Enter PDC_STATUS_LIVE\n");
        S_check_status(CamID, "", "");
    }
}

char  S_GetMemBitDepth(ULONG CamID)
{
    S_SetStatus_Play(CamID);

    ULONG nRet, nErrCode;
    char  nVal;  

    nRet = PDC_GetMemBitDepth(CamID, 1, &nVal, &nErrCode);
    S_check_error(nRet, nErrCode, "PDC_GetMemBitDepth");

    return nVal;
}

ULONG S_GetMemRecordRate(ULONG CamID)
{
    S_SetStatus_Play(CamID);

    ULONG nRet, nErrCode, nVal;   

    nRet = PDC_GetMemRecordRate(CamID, 1, &nVal, &nErrCode);
    S_check_error(nRet, nErrCode, "PDC_GetMemRecordRate");

    return nVal;
}

ULONG S_GetMemShutterSpeedFps(ULONG CamID)
{
    S_SetStatus_Play(CamID);

    ULONG nRet, nErrCode, nVal;   

    nRet = PDC_GetMemShutterSpeedFps(CamID, 1, &nVal, &nErrCode);
    S_check_error(nRet, nErrCode, "PDC_GetMemShutterSpeedFps");

    return nVal;
}

ULONG S_GetNumofDropFrame(ULONG CamID)
{
    S_SetStatus_Play(CamID);

    ULONG nRet, nErrCode, nVal;  

    nRet = PDC_GetNumofDropFrame(CamID, 1, &nVal, &nErrCode);
    S_check_error(nRet, nErrCode, "PDC_GetNumofDropFrame");

    return nVal;
}

void S_GetMemFrameInfo(ULONG CamID, PDC_FRAME_INFO* FrameInfo)
{
    S_SetStatus_Play(CamID);

    ULONG nRet, nErrCode;
    PDC_FRAME_INFO info;

    nRet = PDC_GetMemFrameInfo(CamID,1,&info,&nErrCode);
    S_check_error(nRet, nErrCode, "PDC_GetMemFrameInfo");

    printf("\n\t ---- S_GetMemFrameInfo Params -----------------------\n");
    printf("\n\t -------- m_nStart          %lu", info.m_nStart);
    printf("\n\t -------- m_nEnd            %lu", info.m_nEnd);
    printf("\n\t -------- m_nTrigger        %lu", info.m_nTrigger);
    printf("\n\t -------- m_nRecordedFrames %lu", info.m_nRecordedFrames);
    printf("\n\t -----------------------------------------------------\n\n");
}

BYTE*  S_GetMemImageData(ULONG CamID, long nFrameNo, uint32_t nNumPixels = 0, ULONG nBitDepth = 8)
{
    S_SetStatus_Play(CamID);

    ULONG nRet, nErrCode;
    BYTE *pBuf;            // Memory seq pointer to store image 

    if (nNumPixels == 0) {
        ULONG nWidth, nHeight;  
        nRet = PDC_GetResolution(CamID, 
                                 1,     // nChildNo
                                 &nWidth,
                                 &nHeight,
                                 &nErrCode);

        S_check_error(nRet, nErrCode, "PDC_GetResolution");
        nNumPixels = nWidth * nHeight;
    }

    pBuf = (BYTE*)malloc(nNumPixels);    
    nRet = PDC_GetMemImageData(CamID,
                               1,              // nChildNo
                               nFrameNo,
                               nBitDepth,
                               pBuf,
                               &nErrCode);

    if (nRet == PDC_FAILED) {
        printf("PDC_GetMemImageData Error %lu\n", nErrCode);
        free(pBuf);
        // return;  // PDC dox don't use return here
    }

    return pBuf;
}

/* 
ULONG* S_GetMemResolution(ULONG CamID)
{
    S_SetStatus_Play(CamID);

    ULONG nRet, nErrCode, nHeight, nWidth;  

    nRet = PDC_GetMemResolution(CamID, 1, &nWidth, &nHeight, &nErrCode);
    S_check_error(nRet, nErrCode, "PDC_GetMemResolution");

    return nVal;
}

ULONG* S_GetMemResolutionROI(ULONG CamID)
{
    S_SetStatus_Play(CamID);
    ULONG nRet, nErrCode, nHeight, nWidth;  

    nRet = PDC_GetMemResolutionROI(CamID, 1, &nWidth, &nHeight, &nErrCode);
    S_check_error(nRet, nErrCode, "PDC_GetMemResolutionROI");

    return nVal;
}
*/

//////////////////////////////////
// Live
//////////////////////////////////

BYTE* S_GetLiveImageData(ULONG CamID, uint32_t nNumPixels = 0, ULONG nBitDepth = 8)
{
    // Assumes monochromatic image

    ULONG nRet, nErrCode;
    BYTE *pBuf;            // Memory seq pointer to store image 

    if (nNumPixels == 0) {
        ULONG nWidth, nHeight;  
        nRet = PDC_GetResolution(CamID,1,
                                 &nWidth,
                                 &nHeight,
                                 &nErrCode);

        S_check_error(nRet, nErrCode, "PDC_GetResolution");
        nNumPixels = nWidth * nHeight;
//        printf("S_GetLiveImageData nWidth  %lu, nHeight %lu, nNumPixels %u\n", nWidth, nHeight, nNumPixels);
    }

    pBuf = (BYTE*)malloc(nNumPixels);
    nRet = PDC_GetLiveImageData(CamID,1,
                                nBitDepth,      
                                pBuf,           
                                &nErrCode);
    
    if (nRet == PDC_FAILED) {
        printf("PDC_GetLiveImageData Error %lu\n", nErrCode);
        free(pBuf);
        // return;  // Photron uses 'return' here w/o a return value which is not allowed, esp b/c our fx returns BYTE*
    }

    // printf("S_GetLiveImageData pBuf %u\n", pBuf[500000]);
    // printf("S_GetLiveImageData pBuf %u\n", pBuf[250000]);
    // printf("S_GetLiveImageData pBuf %u\n", pBuf[125000]);

    return pBuf;
}
