########################################################################
##  SETTINGS
########################################################################

# Photron Settings

DEFAULT_IP          = '192.168.0.10'

# Basics
FRAME_RATE          = 100
SHUTTER_SPEED_FPS   = 101
RES                 = (1024,1024)
N_FRAMES            = 20            # only for live capture (no trigger)

SHADING             = "ON"

# Trigger
TRIGGER             = "START"
N_FRAMES_AFTER_TRIG = 50            # only for trig modes MANUAL & RANDOM_MANUAL 
N_FRAMES_RAND_TRIG  = 0             # only for a single randome trigger 
N_RECORDING_TIMES   = 0             # only for trig modes RANDOM_CENTER and RANDOM_MANUAL. Range: [1,10]

# External Input
EXT_IN_SETTINGS     = { 
                        "SYNC"      : {"OFF"      : 0, "OTHERS_POS" : 1},
                        "GEN"       : {"TRIG_POS" : 1, "READY_POS"  : 0},
                        "TRIGTTL"   : {"TRIG_POS" : 1                  }
                      }

# Image Quality
RECORD_PER          = 1000              # milliseconds. TODO: Convert to microseconds on cpp side
BITS_PER_PIXEL      = 12                # 8, 10, or 12

# Verbosity
DEBUG               = 0
VERBOSE             = 1

########################################################################
##  MAPS
########################################################################  

# Photron Settings Maps 

SHADE_MODE          = ("OFF", "ON", "SAVE", "LOAD", "UPDATE")
SHADE_CODE          = range(1,6)

TRIG_MODE           = ('START', 'CENTER', 'END', 'RANDOM', 'MANUAL', 'RANDOM_RESET', 'RANDOM_CENTER', 'RANDOM_MANUAL', 'TWOSTAGE_HALF', 'TWOSTAGE_QUARTER', 'TWOSTAGE_ONEEIGHTH')
TRIG_CODE           = (0, 16777216, 33554432, 50331648, 67108864, 83886080, 100663296, 117440512, 134217729, 134217730, 134217731)

SHADING_MODE        = dict(zip(SHADE_MODE, SHADE_CODE))
SHADING_CODE        = dict((v,k) for (k,v) in SHADING_MODE.items())

TRIGGER_MODE        = dict(zip(TRIG_MODE, TRIG_CODE))              
TRIGGER_CODE        = dict((v,k) for (k,v) in TRIGGER_MODE.items())

# MEMORY_PARAMS     = ("m_nStart", "m_nEnd", "m_nTrigger", "m_nTwoStageLowToHigh", "m_nTwoStageHighToLow", "m_nEvent", "m_nEventCount", "m_nRecordedFrames")
MEMORY_PARAMS       = ("mnStart", "mnEnd", "mnTrigger", "mnEvCnt", "mnRecFrames")

EXT_IN_PORT         = ("SYNC","GEN","TRIGTTL")
EXT_OUT_PORT        = ("GEN1","GEN2","GEN3", "TRIGTTL")

EXT_IN_PORT_NUM     = (1,2,3)
EXT_OUT_PORT_NUM    = (1,2,3,4)

EXT_IN_PORTS        = dict(zip(EXT_IN_PORT, EXT_IN_PORT_NUM))
EXT_IN_PORT_NUMS    = dict((v,k) for (k,v) in EXT_IN_PORTS.items())

EXT_OUT_PORTS       = dict(zip(EXT_OUT_PORT, EXT_OUT_PORT_NUM))
EXT_OUT_PORT_NUMS   = dict((v,k) for (k,v) in EXT_OUT_PORTS.items())


# PORTS               = {
                        # "IN"  : {PORTNUM:{VALIDS, CURRENT}} ,
                        # "OUT" : {}
                      # }

EXT_IN_MODE         = ('OFF','CAM_POS','CAM_NEG','OTHERS_POS','OTHERS_NEG','EVENT_POS','EVENT_NEG','TRIG_POS','TRIG_NEG','READY_POS','READY_NEG','SYNC_POS','SYNC_NEG','CAM','OTHERS')
EXT_IN_CODE         = range(1,16)

EXT_INPUT_MODE      = dict(zip(EXT_IN_MODE, EXT_IN_CODE))    
EXT_INPUT_CODE      = dict((v,k) for (k,v) in EXT_INPUT_MODE.items())

EXT_IN_SYNC_MODES   = EXT_IN_MODE[0:5]
EXT_IN_GEN_MODES    = EXT_IN_MODE[5:11]
EXT_IN_TRIGTTL_MODES= EXT_IN_MODE[7:9]

EXT_IN_PORT_STATUS  = {
                        "SYNC"      :  {"current" : None, "valids" :  EXT_IN_SYNC_MODES},
                        "GEN"       :  {"current" : None, "valids" :  EXT_IN_GEN_MODES},
                        "TRIGTTL"   :  {"current" : None, "valids" :  EXT_IN_TRIGTTL_MODES}
                      }


CAM_STATUS_MODE     = ("LIVE", "PLAYBACK", "RECREADY", "ENDLESS", "REC", "SAVE", "LOAD")
CAM_STATUS_CODE     = (0,1,2,4,8,16,32)

CAMERA_STATUS_MODE  = dict(zip(CAM_STATUS_MODE, CAM_STATUS_CODE))
CAMERA_STATUS_CODE  = dict((v,k) for (k,v) in CAMERA_STATUS_MODE.items())


##############################################