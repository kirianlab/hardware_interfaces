#!/bin/bash

# export SHELLOPTS && set -o igncr  # Cygwin Only

########################################
## Params
########################################

DLL_DIR=$(realpath "../../SDK/Dll/64bit(x64)")
LIB_DIR=$(realpath "../../SDK/Lib/64bit(x64)")
INC_DIR=$(realpath "../../SDK/Include")

########################################
## PDC Requirements
########################################

cp "${DLL_DIR}"/PDCLIB.dll . 
chmod 777 ./PDCLIB.dll

########################################
## Path Exports
########################################

export PATH=:"${DLL_DIR}":$PATH
export PATH=:"${INC_DIR}":$PATH
export PATH=:"${LIB_DIR}":$PATH

########################################
## Compile
########################################

echo -e "\n>>>> Compile >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n"

LDFLAGS="-L ${DLL_DIR}" \
python setup.py build_ext --inplace --compiler=msvc

[[ "$?" == 0 ]] && echo -e "\n\n*** Build Successful!"

echo -e "\n<<<< Compile <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n"
