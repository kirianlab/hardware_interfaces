# cython: language_level=3 
# cython: embedsignature=True

from setuptools import Extension, setup
# from distutils.core      import setup
# from distutils.extension import Extension
from Cython.Build        import cythonize

import os

LIB     = os.path.abspath("../../SDK/Lib/64bit(x64)/PDCLIB")
LIB_DIR = os.path.abspath("../../SDK/Lib/64bit(x64)/")
DLL_DIR = os.path.abspath("../../SDK/Dll/64bit(x64)/")
INC_DIR = os.path.abspath("../../SDK/Include/")

setup(ext_modules = cythonize([
  Extension(
    name         = "cyfastcam",
    sources      = ["./cyfastcam.pyx", "../src_c/fastcam.cpp"],
    language     = "c++",
    libraries    = [LIB],
    library_dirs = [".", "..", LIB_DIR],
    include_dirs = [".", "..", INC_DIR],
  ),
]))
