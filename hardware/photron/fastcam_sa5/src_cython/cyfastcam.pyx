# cython: language_level=3
# distutils: language=c++
""" Cython code to interface with fastcam.cpp (Photron Fastcam SA5).

This Cython module ``cyfastcam.pyx`` implements a python interface
for the corresponding C/C++ code ``../src_c/fastcam.cpp``.

Likewise, ``cyfastcam.pxd`` is Cython counterpart for C/C++
``../src_c/fastcam.h`` header file.

Note:
    * Cythonizing ``cyfastcam.pyx`` via ``./setup.py``
      yields ``./cyfastcam.cpp``. Do not change ``./cyfastcam.cpp``,
      as re-compiling ``cyfastcam.pyx`` will overwrite all changes in
      ``./cyfastcam.cpp``.
"""

##########
# cython
##########

cimport cyfastcam as cyf   ## cython header file (.pxd)

from libc.stdint cimport uint32_t
from libc.stdlib cimport malloc, free

##########
# python
##########

import os, sys, time, h5py, configparser
import datetime as dt
import numpy    as np

from tqdm        import tqdm
from collections import OrderedDict, namedtuple

##########
# configs
##########

########################################################################
##  SETTINGS
########################################################################

# Photron Settings

DEFAULT_IP          = '192.168.0.10'

# Basics
FRAME_RATE          = 100
SHUTTER_SPEED_FPS   = 101
RES                 = (1024,1024)
N_FRAMES            = 20            # only for live capture (no trigger)

SHADING             = "ON"

# Trigger
TRIGGER             = "START"
N_FRAMES_AFTER_TRIG = 50            # only for trig modes MANUAL & RANDOM_MANUAL
N_FRAMES_RAND_TRIG  = 0             # only for a single randome trigger
N_RECORDING_TIMES   = 0             # only for trig modes RANDOM_CENTER and RANDOM_MANUAL. Range: [1,10]

# External Input
EXT_IN_SETTINGS     = {
                        "SYNC"      : {"OFF"      : 0, "OTHERS_POS" : 1},
                        "GEN"       : {"TRIG_POS" : 1, "READY_POS"  : 0},
                        "TRIGTTL"   : {"TRIG_POS" : 1                  }
                      }

# Image Quality
RECORD_PER          = 1000              # milliseconds. TODO: Convert to microseconds on cpp side
BITS_PER_PIXEL      = 12                # 8, 10, or 12

# Verbosity
DEBUG               = 0
VERBOSE             = 1

########################################################################
##  MAPS
########################################################################

# Photron Settings Maps

SHADE_MODE          = ("OFF", "ON", "SAVE", "LOAD", "UPDATE")
SHADE_CODE          = range(1,6)

TRIG_MODE           = ('START', 'CENTER', 'END', 'RANDOM', 'MANUAL', 'RANDOM_RESET', 'RANDOM_CENTER', 'RANDOM_MANUAL', 'TWOSTAGE_HALF', 'TWOSTAGE_QUARTER', 'TWOSTAGE_ONEEIGHTH')
TRIG_CODE           = (0, 16777216, 33554432, 50331648, 67108864, 83886080, 100663296, 117440512, 134217729, 134217730, 134217731)

SHADING_MODE        = dict(zip(SHADE_MODE, SHADE_CODE))
SHADING_CODE        = dict((v,k) for (k,v) in SHADING_MODE.items())

TRIGGER_MODE        = dict(zip(TRIG_MODE, TRIG_CODE))
TRIGGER_CODE        = dict((v,k) for (k,v) in TRIGGER_MODE.items())

# MEMORY_PARAMS     = ("m_nStart", "m_nEnd", "m_nTrigger", "m_nTwoStageLowToHigh", "m_nTwoStageHighToLow", "m_nEvent", "m_nEventCount", "m_nRecordedFrames")
MEMORY_PARAMS       = ("mnStart", "mnEnd", "mnTrigger", "mnEvCnt", "mnRecFrames")

EXT_IN_PORT         = ("SYNC","GEN","TRIGTTL")
EXT_OUT_PORT        = ("GEN1","GEN2","GEN3", "TRIGTTL")

EXT_IN_PORT_NUM     = (1,2,3)
EXT_OUT_PORT_NUM    = (1,2,3,4)

EXT_IN_PORTS        = dict(zip(EXT_IN_PORT, EXT_IN_PORT_NUM))
EXT_IN_PORT_NUMS    = dict((v,k) for (k,v) in EXT_IN_PORTS.items())

EXT_OUT_PORTS       = dict(zip(EXT_OUT_PORT, EXT_OUT_PORT_NUM))
EXT_OUT_PORT_NUMS   = dict((v,k) for (k,v) in EXT_OUT_PORTS.items())


# PORTS               = {
                        # "IN"  : {PORTNUM:{VALIDS, CURRENT}} ,
                        # "OUT" : {}
                      # }

EXT_IN_MODE         = ('OFF','CAM_POS','CAM_NEG','OTHERS_POS','OTHERS_NEG','EVENT_POS','EVENT_NEG','TRIG_POS','TRIG_NEG','READY_POS','READY_NEG','SYNC_POS','SYNC_NEG','CAM','OTHERS')
EXT_IN_CODE         = range(1,16)

EXT_INPUT_MODE      = dict(zip(EXT_IN_MODE, EXT_IN_CODE))
EXT_INPUT_CODE      = dict((v,k) for (k,v) in EXT_INPUT_MODE.items())

EXT_IN_SYNC_MODES   = EXT_IN_MODE[0:5]
EXT_IN_GEN_MODES    = EXT_IN_MODE[5:11]
EXT_IN_TRIGTTL_MODES= EXT_IN_MODE[7:9]

EXT_IN_PORT_STATUS  = {
                        "SYNC"      :  {"current" : None, "valids" :  EXT_IN_SYNC_MODES},
                        "GEN"       :  {"current" : None, "valids" :  EXT_IN_GEN_MODES},
                        "TRIGTTL"   :  {"current" : None, "valids" :  EXT_IN_TRIGTTL_MODES}
                      }


CAM_STATUS_MODE     = ("LIVE", "PLAYBACK", "RECREADY", "ENDLESS", "REC", "SAVE", "LOAD")
CAM_STATUS_CODE     = (0,1,2,4,8,16,32)

CAMERA_STATUS_MODE  = dict(zip(CAM_STATUS_MODE, CAM_STATUS_CODE))
CAMERA_STATUS_CODE  = dict((v,k) for (k,v) in CAMERA_STATUS_MODE.items())


##############################################
##########
# str fmt
##########

TAB1 = 4*" "
TAB2 = 2*TAB1
NLN  = "\n"

##########
# misc
##########

def snooze(): time.sleep(0.5)

def snoozed(func):

    def snoozer(*args, **kwargs):
        ret = func(*args, **kwargs)
        snooze()
        return ret

    return snoozer

######################################################################
### Cython Fastcam Class
######################################################################


cdef class Fastcam:
    """ Cython Class for Photron Fastcam SA5 Camera."""

    cdef ULONG      _camID  # cdef class field has no default value
    cdef ULONG      _retVal, _errCode

    cdef ULONG      _m_recRate
    cdef ULONG      _m_shutterFPS
    cdef ULONG      _nBits       , _m_nBits
    cdef size_t     _width       , _m_width
    cdef size_t     _height      , _m_height
    cdef uint32_t   _nPixels     , _m_nPixels
    cdef long       _m_nStart    , _m_nRecFrames

    cdef public _num_recorded_frames
    cdef public _recording_info

    cdef public _recording_epoch_time_start
    cdef public _recording_epoch_time_stop

    cdef public valid_ext_in_modes
    cdef public cyf.PDC_FRAME_INFO mem_frame_info

    STATUS  = {
        cyf.PDC_STATUS_LIVE         : "live",
        cyf.PDC_STATUS_PLAYBACK     : "playback",
        cyf.PDC_STATUS_SAVE         : "save",
        cyf.PDC_STATUS_REC          : "rec",
        cyf.PDC_STATUS_RECREADY     : "recready",
        cyf.PDC_STATUS_ENDLESS      : "endless"
    }

    cyf.S_Init()

    def __cinit__(self, frame_rate=150000, bit_depth=8):

        try:
            self._camID = cyf.S_Connect()

        except Exception as e:
            print(repr(e), e)
            raise MemoryError("\tCam ID: " + self._camID)

        self.set_status_live()
        self.bit_depth  = bit_depth
        self.set_max_resolution()
        # Always set resolution before frame rate!
        # self.frame_rate = frame_rate
        self.set_status_live()

    def __init__(self): pass
    def __bool__(self): pass
    def __repr__(self): pass
    def __dealloc__(self):

        try:
            self.set_status_live()
            # print("\n**** Closing Fastcam (Automatic!) ****\n")
            cyf.S_CloseDevice(self._camID)
            time.sleep(0.5)

        except Exception as e:
            print(repr(e), e)
            raise MemoryError("\tCam ID: " + self._camID)

    def close_camera(self):

        try:
            self.set_status_live()
            # print("\n**** Closing Fastcam (Intentional!) ****\n")
            cyf.S_CloseDevice(self._camID)
            time.sleep(0.5)

        except Exception as e:
            print(repr(e), e)
            raise MemoryError("\tCam ID: " + self._camID)

    def __str__(self):

        h, w, _ = self.resolution
        self.set_status_live()

        msg = f"""
        --------------------------------------------------------------
        ---- Resolution    : {h} x {w}
        ---- Framerate     : {self.frame_rate}
        ---- Shutter       : {self.shutter_speed}
        ---- Bit           : {self.bit_depth}
        ---- Status        : {self.status}
        ---- SyncIn        : {self.get_external_in_mode()}
        ---- Frames in Mem : {self.mem_frame_info.get('m_nRecordedFrames')}
        --------------------------------------------------------------
        """

        self.set_status_live()

        return msg

    # Status #########################################################

    @property
    #@snoozed
    def status(self):
        val = self.STATUS.get(cyf.S_GetStatus(self._camID), "unknown")
        snooze()
        return val

    #@snoozed
    def set_status_live(self): cyf.S_SetStatus_Live(self._camID)

    #@snoozed
    def set_status_play(self): cyf.S_SetStatus_Play(self._camID)

    #@snoozed
    def reset_shading(self):   cyf.S_SetShadingMode( self._camID)

    # Frame Rate #####################################################

    @property
    def frame_rate(self): return cyf.S_GetRecordRate(self._camID)

    @frame_rate.setter
    def frame_rate(self, val):
        if val in self.valid_frame_rates:
            cyf.S_SetRecordRate(self._camID, <ULONG>val)
            snooze()
            self.set_min_shutter_speed_fps()
            self.set_max_resolution()
        else:
            print(ValueError("Invalid Requested Framerate: %d"%val))
            pass

    @property
    def valid_frame_rates(self):

        verbose           = False
        cdef ULONG  Count = 0;
        cdef ULONG* V     = cyf.S_GetRecordRateList(self._camID, &Count, <bint>verbose);

        if not V: raise MemoryError()
        valids = []

        try:
            for i in range(Count): valids.append(V[i])
        except  : raise MemoryError()
        finally : free(V)

        if verbose:
            print(f"{NLN}--- Valid Frame Rates: {Count} ---{NLN}")
            for i,v in enumerate(valids): print(f"{TAB1}{i:2d} : {v:d}")

        return valids

    # Resolution #####################################################

    @property
    def resolution(self):

        cdef ULONG h, w;
        self._retVal = cyf.PDC_GetResolution(self._camID, 1, &w, &h, &self._errCode)
        cyf.S_check_error(self._retVal, self._errCode, "PDC_GetResolution");

        self._height    = <size_t>h
        self._width     = <size_t>w
        self._nPixels   = <uint32_t>(w*h)

        return (self._height, self._width, self._nPixels)

    @resolution.setter
    def resolution(self, val):

        w, h = val
        cyf.S_SetResolution(self._camID, <ULONG>w, <ULONG>h)
        self.reset_shading()
        res = self.resolution

    @property
    #@snoozed
    def valid_resolutions(self):

        verbose             = False
        cdef ULONG  Count   = 0;
        cdef ULONG* V       = cyf.S_GetResolutionList(self._camID, &Count, <bint>verbose)

        if not V: raise MemoryError()
        valids = []

        try:
            for i in range(Count): valids.append((V[2*i], V[2*i+1]))
        except  : raise MemoryError()
        finally : free(V)

        if verbose:
            print(f"{NLN}--- Valid Resolutions: {Count} ---{NLN}")
            for i,v in enumerate(valids):
                print(f"{TAB1}{i:2d} : {v[0]:5d}  x {v[1]:5d}")

        return valids

    @property
    def resolution_max(self):

        cdef ULONG height, width
        cyf.S_GetMaxResolution(self._camID, &width, &height)

        return (height, width)

    def set_max_resolution(self):

        cyf.S_SetMaxResolution(self._camID)
        self.reset_shading()
        res = self.resolution

    # Shutter Speed FPS ##############################################

    @property
    def shutter_speed(self): return cyf.S_GetShutterSpeedFps(self._camID)

    @shutter_speed.setter
    #@snoozed
    def shutter_speed(self, val):
        cyf.S_SetShutterSpeedFps(self._camID, <ULONG>val)

    #@snoozed
    def set_min_shutter_speed_fps(self): cyf.S_SetMinShutterSpeedFps(self._camID)

    @property
    #@snoozed
    def valid_shutter_speeds(self):

        verbose             = False
        cdef ULONG  Count   = 0;
        cdef ULONG * V      = cyf.S_GetShutterSpeedFpsList(self._camID, &Count, <bint>verbose)

        if not V: raise MemoryError()
        valids = []

        try:
            for i in range(Count): valids.append(V[i])
        except  : raise MemoryError()
        finally : free(V)

        if verbose:
            print(f"{NLN}--- Valid Shutter Speeds (FPS): {Count} ---{NLN}")
            for i,v in enumerate(valids): print(f"{TAB1}{i:2d} : {v:d}")

        return valids

    # Bit Depth ######################################################

    @property
    #@snoozed
    def bit_depth(self):
        self._nBits = cyf.S_GetBitDepth(self._camID)
        return self._nBits

    @bit_depth.setter
    #@snoozed
    def bit_depth(self, val):
        cyf.S_SetBitDepth(self._camID, <char>val)
        b = self.bit_depth

    # External Inputs ################################################

    #@snoozed
    def get_external_in_mode(self, port="sync", verbose=True):

        mode=0

        try:
            portN = EXT_IN_PORTS[port.upper()]
            modeN = cyf.S_GetExternalInMode(self._camID, <ULONG>portN)
            mode  = EXT_INPUT_CODE[modeN]

        except Exception as e:
            print(repr(e))

        return mode

    #@snoozed
    def get_external_in_mode_list(self, port_num=1, verbose=True):

        cdef ULONG  Count   = 0;
        cdef ULONG * V;
        try:
            V = cyf.S_GetExternalInModeList(self._camID, <ULONG>port_num, &Count, <bint>verbose)
            if V:
                self.valid_ext_in_modes = {}
                self.valid_ext_in_modes[port_num] = []

                while i < Count:
                    self.valid_ext_in_modes[port_num].append(V[i])
                    i+=1

                free(V)

        except Exception as e:
            print(repr(e))

    def set_external_in_mode(self, port="sync", mode="others_pos"):

        portN = EXT_IN_PORTS.get(  port.upper(), False)
        modeN = EXT_INPUT_MODE.get(mode.upper(), False)

        # print(f"Set External In Port Num : {portN}")
        # print(f"Set External In Mode Num : {modeN}")

        if portN and modeN:

            try:
                self.set_status_live()
                self._retVal = cyf.PDC_SetExternalInMode(self._camID, <ULONG>portN, <ULONG>modeN, &self._errCode);
                cyf.S_check_error(self._retVal, self._errCode, "PDC_SetExternalInMode");
                snooze()
                self.set_status_live()

                # cyf.S_SetExternalInMode(self._camID, <ULONG>portN, <ULONG>modeN)
                # print(f"    ---- Ext In: {port} --> {self.get_external_in_mode(port)}")

            except Exception as e:
                print(repr(e))

        else: print("Invalid External Input Port Number")

    # Record #########################################################

    def trigger(self):                 cyf.S_TriggerIn(self._camID)
    def set_recready(self):            cyf.S_SetRecReady(self._camID)
    def start_recording(self):         cyf.S_start_recording(self._camID)
    def start_recording_endless(self): cyf.S_start_recording_endless(self._camID)
    def stop_recording(self) :         cyf.S_stop_recording( self._camID)

    #@snoozed
    def get_mem_frame_info(self, verbose=False):

        self.set_status_play();

        cdef ULONG nRet, nErrCode;      # cdef cyf.PDC_FRAME_INFO mem_frame_info;
        nRet = cyf.PDC_GetMemFrameInfo( # cyf.S_GetMemFrameInfo(self._camID, &info)
                    self._camID, 1,
                    &self.mem_frame_info,
                    &nErrCode);

        self._m_nRecFrames  = <long>(self.mem_frame_info.get("m_nRecordedFrames"))
        self._m_nStart      = <long>(self.mem_frame_info.get("m_nStart"))

        if verbose: print(f"""
            Fastcam Memory -----------
            Start Frame  : {self._m_nStart}
            Total Frames : {self._m_nRecFrames}
            -------------------------- """
        )

    def get_recording_info(self, verbose=False):

        self.set_status_play()

        self.get_mem_frame_info()
        self.Cy_get_mem_resolution()

        self._m_nBits       = <ULONG>cyf.S_GetMemBitDepth(        self._camID)
        self._m_recRate     = <ULONG>cyf.S_GetMemRecordRate(      self._camID)
        self._m_shutterFPS  = <ULONG>cyf.S_GetMemShutterSpeedFps( self._camID)

        self._recording_info = OrderedDict(
            framerate_fps    = self._m_recRate,
            shutter_fps      = self._m_shutterFPS,
            width            = self._m_width,
            height           = self._m_height,
            pixels           = self._m_nPixels,
            bits             = self._m_nBits,
            frames           = self._num_recorded_frames,
            epoch_time_start = self._recording_epoch_time_start,
            epoch_time_stop  = self._recording_epoch_time_stop,
        )

    cdef Cy_get_mem_resolution(self):

        self.set_status_play()

        cdef ULONG w,h
        self._retVal = cyf.PDC_GetMemResolution(self._camID, 1, &w, &h, &self._errCode)
        cyf.S_check_error(self._retVal, self._errCode, "PDC_GetMemResolution");

        self._m_height    = <size_t>h
        self._m_width     = <size_t>w
        self._m_nPixels   = <uint32_t>(w*h)

        return (self._m_height, self._m_width, self._m_nPixels)

    def get_recorded_frames(self, n=200, verbose=0):

        """
            Retrieves recorded frames from Fastcam's internal memory
            Returns np.ndarray((n, height, width), dtype=np.uint32)

            Assumes monochromatic image

            For python/numpy array indices use types
                'Py_ssize_t' & 'size_t'
                (signed & unsigned ounterparts)

            unsigned int = np.uint32 = 4 Bytes = 32 bits = 0 to 2*65535

            imp numpy int types:
                intc (32 or 64), uint16, uint32, uint64

            Camera status must be PDC_STATUS_PLAYBACK to obtain saved data (image, conditions)
        """

        self.set_status_play()
        self.get_recording_info()

        verbose  = <bint>verbose
        n        = <size_t>min(n, self._m_nRecFrames)
        frames   = np.zeros((n, self._m_height, self._m_width), dtype=np.uint8)  # TODO: bit depth

        cdef BYTE[:, :, :]  stack_memview = frames
        cdef BYTE           *pCyBuf # mem seq pointer to store image
        pCyBuf              = <BYTE*>malloc(self._m_nPixels);
        cdef size_t         i, j, k

        print("\nTransfer Recording ...\n")

        for k in tqdm(range(n)):
            if verbose: print("\t", f"{k:3d} of {n:3d}")

            cyf.PDC_GetMemImageData(    # or cyf.S_GetMemImageData
                    self._camID, 1,
                    <long>(k + self._m_nStart),
                    self._m_nBits,
                    pCyBuf,
                    &self._errCode)

            if pCyBuf is NULL:
                free(pCyBuf)
                raise MemoryError()

            for i in range(self._m_height):
                for j in range(self._m_width):
                    stack_memview[k,i,j] = pCyBuf[j+i*self._m_width]

        free(pCyBuf)
        self._num_recorded_frames = len(frames)

        return frames

    def record_interval(self, duration=3):

        print("\nRecording ... \n")

        while (self.status != "recready"):
            self.set_recready()
            snooze()

        while (self.status not in ["rec", "endless"]):
            self._recording_epoch_time_start = dt.datetime.now().timestamp()
            self.trigger()
            snooze()

        for i in tqdm(range(int(duration/0.5))):
            time.sleep(0.5)

        self._recording_epoch_time_stop = dt.datetime.now().timestamp()
        self.stop_recording()

    def record_nframes(self, n=200):

        duration = 3 + int(n/self.frame_rate)
        self.record_interval(duration)

        frames = self.get_recorded_frames(n=n)
        self.get_recording_info()   #TODO: Necessary? Already called by 'self.get_recorded_frames()'

        return frames

    # Live ###########################################################

    cdef BYTE* Cy_get_live_frame(self):

        cdef BYTE *pBuf
        pBuf = <BYTE*>malloc(self._nPixels);  # TODO
        self._retVal = cyf.PDC_GetLiveImageData(self._camID, 1, <ULONG>self._nBits, pBuf, &self._errCode);

        if self._retVal == cyf.PDC_FAILED:
            print("PDC_GetLiveImageData Error: ", self._errCode);
            free(pBuf);  # TODO
            # return;   # Photron uses 'return' here w/o a return value which is not allowed, esp b/c our fx returns unsigned char*

        return pBuf;

    def get_live_frame(self):

        """
            Returns np.ndarray((height, width), dtype=np.uint32)

            Assumes monochromatic image

            For python/numpy array indices use types
                'Py_ssize_t' & 'size_t'
                (signed & unsigned ounterparts)

            unsigned int = np.uint32 = 4 Bytes = 32 bits = 0 to 2*65535

            imp numpy int types:
                intc (32 or 64), uint16, uint32, uint64
        """

        cdef BYTE* pCyBuf           # Memory seq pointer to store image
        pCyBuf = <BYTE*>malloc(<uint32_t>self._nPixels);
        cyf.PDC_GetLiveImageData(self._camID, 1, <ULONG>self._nBits, pCyBuf, &self._errCode)

        """ Also possible (and tested!)

                 pCyBuf = cyf.S_GetLiveImageData(self._camID, <uint32_t>self._nPixels, self._nBits)
                 pCyBuf = self.Cy_get_live_frame()
        """

        if pCyBuf is NULL:
            free(pCyBuf)
            raise MemoryError()

        cdef size_t h   = self._height
        cdef size_t w   = self._width
        frame           = np.zeros((h,w), dtype=np.uint8)   # cdef npc.ndarray frame = np.zeros((h,w), dtype=np.uint8)

        cdef BYTE[:, :] frame_memview = frame
        cdef size_t i, j

        for i in range(h):
            for j in range(w):
                frame_memview[i,j] = pCyBuf[j+i*w]

        free(pCyBuf)

        return frame

