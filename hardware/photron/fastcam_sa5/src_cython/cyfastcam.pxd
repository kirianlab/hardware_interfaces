# cython: language_level=3
# distutils: libraries = PDCLIB
# distutils: include_dirs = .
# distutils: library_dirs = .

"""Cython header file for cyfastcam.pyx, counterpart of fastcam.h"""

from libc.stdint cimport uint32_t, int64_t, uint64_t

ctypedef unsigned long ULONG
ctypedef unsigned char BYTE

cdef extern from "./fastcam.h":

    ctypedef struct PDC_FRAME_INFO:
        long m_nStart;
        long m_nEnd;
        long m_nTrigger;
        long m_nTwoStageLowToHigh;
        long m_nTwoStageHighToLow;
        long m_nEvent[10];
        long m_nEventCount;
        long m_nRecordedFrames;

    ULONG   PDC_FAILED
    ULONG   PDC_SUCCEEDED
    ULONG   PDC_EXT_IN_OTHERSSYNC_POSI
    ULONG   PDC_EXT_IN_NONE

    ULONG   PDC_STATUS_LIVE, \
            PDC_STATUS_PLAYBACK, \
            PDC_STATUS_SAVE, \
            PDC_STATUS_REC, \
            PDC_STATUS_RECREADY, \
            PDC_STATUS_ENDLESS


    ########################################
    ## Standalone
    ########################################

    ULONG   PDC_GetResolution(    ULONG, ULONG, ULONG* , ULONG*, ULONG*)
    ULONG   PDC_GetResolutionList(ULONG, ULONG, ULONG* , ULONG*, ULONG*)
    ULONG   PDC_GetMemResolution( ULONG, ULONG, ULONG* , ULONG*, ULONG*)
    ULONG   PDC_GetLiveImageData( ULONG, ULONG, ULONG  , void* , ULONG*)

    ULONG   PDC_SetExternalInMode(ULONG, ULONG, ULONG, ULONG*)

    ULONG   PDC_GetMemImageData(  ULONG, ULONG, long, ULONG, void*, ULONG*)
    ULONG   PDC_GetMemFrameInfo(  ULONG, ULONG, PDC_FRAME_INFO*  , ULONG*)


    ########################################
    ## Run Seq
    ########################################

    void    S_Init();
    ULONG   S_Connect();
    void    S_Run_Simplest(ULONG CamID);
    void    S_Run(ULONG CamID);
    void    S_CloseDevice(ULONG CamID);


    ########################################
    ## Routines
    ########################################

    void S_SetRecReady(             ULONG CamID);
    void S_TriggerIn(               ULONG CamID);
    void S_start_recording(         ULONG CamID);
    void S_start_recording_endless( ULONG CamID);
    void S_verify_recording(        ULONG CamID);
    void S_verify_recording_ready(  ULONG CamID);
    void S_stop_recording(          ULONG CamID);



    ########################################
    ## Getters
    ########################################

    #### Main       ------------------------

    void            S_print_PDC_error_codes();
    void            S_main_getters( ULONG CamID);
    void            S_check_error(  ULONG nRet, ULONG nErrorCode, const char* PDC_fx);
    ULONG           S_GetStatus(    ULONG CamID);

    #### Ports      ------------------------

    ULONG           S_GetExternalInMode(ULONG CamID, ULONG nExtInPortNum);
    ## ULONG        S_GetTriggerMode(ULONG CamID);
    ## ULONG        S_GetShadingMode(ULONG CamID);

    #### Live       ------------------------

    char            S_GetBitDepth(              ULONG CamID);
    BYTE*           S_GetLiveImageData(         ULONG CamID, uint32_t uNumPixels, ULONG nBitDepth);

    ULONG           S_GetRecordRate(            ULONG CamID);
    ULONG           S_GetShutterSpeedFps(       ULONG CamID);
    void            S_GetResolution(            ULONG CamID, ULONG* height, ULONG* width);
    void            S_GetMaxResolution(         ULONG CamID, ULONG* height, ULONG* width);

    #### Valids     ------------------------

    ULONG*          S_GetRecordRateList(        ULONG CamID, ULONG* nCount, bint bVerbose);
    ULONG*          S_GetShutterSpeedFpsList(   ULONG CamID, ULONG* nCount, bint bVerbose);
    ULONG*          S_GetResolutionList(        ULONG CamID, ULONG* nCount, bint bVerbose);
    ULONG*          S_GetShadingModeList(       ULONG CamID, ULONG* nCount, bint bVerbose);
    ULONG*          S_GetTriggerModeList(       ULONG CamID, ULONG* nCount, bint bVerbose);
    ULONG*          S_GetExternalInModeList(    ULONG CamID, ULONG nExtInPortNum, ULONG* nCount, bint bVerbose);

    #### Playback   ------------------------

    void            S_GetMemFrameInfo(          ULONG CamID, PDC_FRAME_INFO* FrameInfo);
    BYTE*           S_GetMemImageData(          ULONG CamID, long nFrameNo, uint32_t nNumPixels, ULONG nBitDepth);
    char            S_GetMemBitDepth(           ULONG CamID);

    ULONG           S_GetMemRecordRate(         ULONG CamID);
    ULONG           S_GetMemShutterSpeedFps(    ULONG CamID);
    ULONG           S_GetNumofDropFrame(        ULONG CamID);

    ULONG*          S_GetMemResolution(         ULONG CamID);
    ULONG*          S_GetMemResolutionROI(      ULONG CamID);


    ########################################
    ## Setters
    ########################################

    void S_main_setters(            ULONG CamID);

    void S_SetStatus(               ULONG CamID, ULONG status);
    void S_SetStatus_Live(          ULONG CamID);
    void S_SetStatus_Play(          ULONG CamID);

    void S_SetRecordRate(           ULONG CamID, ULONG nRate);
    void S_SetResolution(           ULONG CamID, ULONG nWidth, ULONG nHeight);
    void S_SetMaxResolution(        ULONG CamID);
    void S_SetShutterSpeedFps(      ULONG CamID, ULONG nFps);
    void S_SetMinShutterSpeedFps(   ULONG CamID);
    void S_SetBitDepth(             ULONG CamID, char nDepth);

    void S_SetTriggerMode(          ULONG CamID, ULONG nMode, ULONG nNumFramesAfterTrigger, ULONG nNumFramesPerSingleRandomTrigger, ULONG nNumRecordingTimes);
    void S_SetShadingMode(          ULONG CamID);
    void S_SetBurstTransfer(        ULONG CamID);
    void S_SetExternalInMode(       ULONG CamID, ULONG nExtInPortNo, ULONG nMode);

