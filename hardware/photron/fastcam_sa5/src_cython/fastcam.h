////////////////////////////////////////
// Includes
////////////////////////////////////////

//// Photron    ------------------------

#pragma once

#define WIN32_LEAN_AND_MEAN
#define STATUS_CHECK_TIMEOUT 30000   // milli-sec

#include <stdio.h>
#include <windows.h>
#include <locale.h>

//// Sahba      ------------------------

#include <stdint.h>
#include <ctime>

//// Typedefs   ------------------------

typedef unsigned long ULONG;
typedef unsigned char BYTE;

#include "PDCLIB.h"

////////////////////////////////////////
// Run Seq
////////////////////////////////////////

void    S_Init(void);
ULONG   S_Connect(void);
void    S_Run_Simplest(ULONG CamID);
unsigned long S_Run(ULONG CamID);
void    S_CloseDevice(ULONG CamID);

////////////////////////////////////////
// Routines
////////////////////////////////////////

void S_SetRecReady(ULONG CamID);
void S_TriggerIn(ULONG CamID);
void S_start_recording(ULONG CamID);
void S_start_recording_endless(ULONG CamID);
void S_verify_recording(ULONG CamID);
void S_verify_recording_ready(ULONG CamID);
void S_stop_recording(ULONG CamID);

////////////////////////////////////////
// Getters
////////////////////////////////////////

//// Main       ------------------------

void S_main_getters(ULONG CamID);
void S_check_error(ULONG nRet, ULONG nErrorCode, const char* PDC_fx);
void S_check_status(ULONG CamID, const char* PDC_status, const char* S_fx);
void S_print_PDC_error_codes(void);

ULONG           S_GetStatus(ULONG CamID);

//// Ports      ------------------------

ULONG           S_GetExternalInMode(ULONG CamID, ULONG nExtInPortNum);
// ULONG        S_GetTriggerMode(ULONG CamID);
// ULONG        S_GetShadingMode(ULONG CamID);

//// Live       ------------------------

char            S_GetBitDepth(ULONG CamID);
BYTE*           S_GetLiveImageData(ULONG CamID, uint32_t uNumPixels, ULONG nBitDepth);

ULONG           S_GetRecordRate(ULONG CamID);
ULONG           S_GetShutterSpeedFps(ULONG CamID);
void            S_GetResolution(   ULONG CamID, ULONG* height, ULONG* width);
void            S_GetMaxResolution(ULONG CamID, ULONG* height, ULONG* width);

//// Valids     ------------------------

ULONG*          S_GetRecordRateList(        ULONG CamID, ULONG* nCount, bool bVerbose);
ULONG*          S_GetShutterSpeedFpsList(   ULONG CamID, ULONG* nCount, bool bVerbose);
ULONG*          S_GetResolutionList(        ULONG CamID, ULONG* nCount, bool bVerbose);
ULONG*          S_GetShadingModeList(       ULONG CamID, ULONG* nCount, bool bVerbose);
ULONG*          S_GetTriggerModeList(       ULONG CamID, ULONG* nCount, bool bVerbose);
ULONG*          S_GetExternalInModeList(    ULONG CamID, ULONG nExtInPortNum, ULONG* nCount, bool bVerbose);

//// Playback   ------------------------

void            S_GetMemFrameInfo(ULONG CamID, PDC_FRAME_INFO* FrameInfo);
BYTE*           S_GetMemImageData(ULONG CamID, long nFrameNo, uint32_t nNumPixels, ULONG nBitDepth);
char            S_GetMemBitDepth(ULONG CamID);

ULONG           S_GetMemRecordRate(ULONG CamID);
ULONG           S_GetMemShutterSpeedFps(ULONG CamID);
ULONG           S_GetNumofDropFrame(ULONG CamID);

ULONG*          S_GetMemResolution(ULONG CamID);
ULONG*          S_GetMemResolutionROI(ULONG CamID);



////////////////////////////////////////
// Setters
////////////////////////////////////////

void S_main_setters(ULONG CamID);

void S_SetStatus(ULONG CamID, ULONG status);
void S_SetStatus_Live(ULONG CamID);
void S_SetStatus_Play(ULONG CamID);

void S_SetRecordRate(ULONG CamID, ULONG nRate);
void S_SetResolution(ULONG CamID, ULONG nWidth, ULONG nHeight);
void S_SetMaxResolution(ULONG CamID);
void S_SetShutterSpeedFps(ULONG CamID, ULONG nFps);
void S_SetMinShutterSpeedFps(ULONG CamID);
void S_SetBitDepth(ULONG CamID, char nDepth);

void S_SetTriggerMode(ULONG CamID, ULONG nMode, ULONG nNumFramesAfterTrigger, ULONG nNumFramesPerSingleRandomTrigger, ULONG nNumRecordingTimes);
void S_SetShadingMode(ULONG CamID);
void S_SetBurstTransfer(ULONG CamID);
void S_SetExternalInMode(ULONG CamID, ULONG nExtInPortNo, ULONG nMode);

