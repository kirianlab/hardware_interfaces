#pragma once

#define WIN32_LEAN_AND_MEAN
#define STATUS_CHECK_TIMEOUT 30000   // milli-sec

#include <stdio.h>
#include <windows.h>
#include <locale.h>

#include <stdint.h>

typedef unsigned long ULONG;
typedef unsigned char BYTE;

#include "PDCLIB.h"

void check_error(unsigned long, unsigned long, const char*);
void test();
