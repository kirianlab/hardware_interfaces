import os, sys, time, h5py, configparser, datetime as dt, numpy as np

from tqdm import tqdm
from collections import OrderedDict, namedtuple
from .constants import EXT_IN_PORTS, EXT_INPUT_MODE, EXT_INPUT_CODE

# from odysseus.modules.make_dummy_data import make_dummy_image, make_dummy_movie

#################
### str format
#################

TAB1 = 4 * " "
TAB2 = 2 * TAB1
NLN = "\n"

################################################################################
### Fastcam Dummy Class (Python, not Cython)
################################################################################


class FastcamDummy:
    """Python Dummy Class for Photron Fastcam SA5 Camera."""

    def __init__(self, frame_rate=150000, bit_depth=8):
        self.init_dummy_attrs()

        print("\n**** Welcome to Odysseus  (Dummy Photron) ****\n")

        self.set_status_live()
        self.bit_depth = bit_depth
        self.frame_rate = frame_rate
        self.set_status_live()

    def init_dummy_attrs(self):
        self._status = "live"  # unique to dummy
        self._frame_rate = 0
        self._height = 512
        self._width = 512
        self._nPixels = self._height * self._width
        self._resolution = (self._height, self._width)
        self._shutter_speed = 0
        self._nBits = 0
        self._num_recorded_frames = 0

    def __bool__(self):
        pass

    def __repr__(self):
        pass

    def __dealloc__(self):
        print("\n**** Closing Fastcam (Automatic!) ****\n")

    def close_camera(self):
        print("\n**** Closing Fastcam (Intentional!) ****\n")

    def __str__(self):
        h, w, _ = self.resolution
        self.set_status_live()

        msg = f"""
        --------------------------------------------------------------
        ---- Resolution    : {h} x {w}
        ---- Framerate     : {self.frame_rate}
        ---- Shutter       : {self.shutter_speed}
        ---- Bit           : {self.bit_depth}
        ---- Status        : {self.status}
        ---- SyncIn        : {self.get_external_in_mode(port="sync")}
        --------------------------------------------------------------
        """

        self.set_status_live()
        return msg

    # Status #########################################################

    @property
    # @snoozed
    def status(self):
        return self._status

    @status.setter
    def status(self, val):
        self._status = val

    # @snoozed
    def set_status_live(self):
        self.status = "live"

    # @snoozed
    def set_status_play(self):
        self.status = "play"

    # @snoozed
    def reset_shading(self):
        print("    Reset Shading")

    # Frame Rate #####################################################

    @property
    def frame_rate(self):
        return self._frame_rate

    @frame_rate.setter
    def frame_rate(self, val):
        self._frame_rate = val

    @property
    def valid_frame_rates(self):
        return np.arange(1000, 10000, 2000)

    # Resolution #####################################################

    @property
    def resolution(self):
        # idx = np.random.choice(np.arange(len(self.valid_resolutions)), size=1)
        # self._height, self._width = self.valid_resolutions[idx[0]]
        self._nPixels = self._height * self._width

        return (self._height, self._width, self._nPixels)

    @resolution.setter
    def resolution(self, val):
        self._width, self._height = val
        self.reset_shading()
        res = self.resolution
        return res

    @property
    def valid_resolutions(self):
        return [(512, 512), (512, 128), (64, 128)]

    @property
    def resolution_max(self):
        return self.valid_resolutions[0]

    def set_max_resolution(self):
        self._width, self.height = self.resolution_max
        self.reset_shading()
        res = self.resolution

    # Shutter Speed FPS ##############################################

    @property
    def shutter_speed(self):
        return self._shutter_speed

    @shutter_speed.setter
    # @snoozed
    def shutter_speed(self, val):
        self._shutter_speed = val

    # @snoozed
    def set_min_shutter_speed_fps(self):
        self._shutter_speed = min(self.valid_shutter_speeds)

    @property
    # @snoozed
    def valid_shutter_speeds(self):
        return np.arange(1000, 10000, 2000)

    # Bit Depth ######################################################

    @property
    # @snoozed
    def bit_depth(self):
        return self._nBits

    @bit_depth.setter
    # @snoozed
    def bit_depth(self, val):
        self._nBits = val

    # External Inputs ################################################

    # @snoozed
    def get_external_in_mode(self, port="sync", verbose=True):
        # TODO: fix in original
        portN = EXT_IN_PORTS[port.upper()]
        modeN = np.random.choice(list(EXT_INPUT_CODE.keys()), size=1)[0]
        mode = EXT_INPUT_CODE[modeN]
        return mode

    # @snoozed
    def get_external_in_mode_list(self, port_num=1, verbose=True):
        return list(EXT_INPUT_MODE[EXT_IN_PORTS[port.upper()]].keys())

    def set_external_in_mode(self, port="sync", mode="others_pos"):
        portN = EXT_IN_PORTS.get(port.upper(), False)
        modeN = EXT_INPUT_MODE.get(mode.upper(), False)

        print("\t ", f"Set Ext In Port {port} ({portN}) to Mode {mode} ({modeN})")

        if portN and modeN:
            self.set_status_live()
            print(f"    ---- Get Ext In: {port} --> {self.get_external_in_mode(port)}")
        else:
            print("Invalid Port or Mode")

    # Record #########################################################

    def trigger(self):
        self.start_recording()

    def set_recready(self):
        self._status = "recready"

    def start_recording(self):
        self._status = "rec"

    def start_recording_endless(self):
        self._status = "endless"

    def stop_recording(self):
        self.set_status_live

    # @snoozed
    def get_mem_frame_info(self, verbose=False):
        self.set_status_play()
        self._m_nRecFrames = np.random.choice([500, 1000, 3000], size=1)[0]
        self._m_nStart = 0

        print(
            f"""
            Fastcam Memory -----------
            Start Frame  : {self._m_nStart}
            Total Frames : {self._m_nRecFrames}
            -------------------------- """
        )

    def get_recording_info(self, verbose=False):
        self.set_status_play()
        self.get_mem_frame_info()
        self.Cy_get_mem_resolution()

        self._m_nBits = self.bit_depth
        self._m_recRate = self.frame_rate
        self._m_shutterFPS = self.shutter_speed

        self._recording_info = OrderedDict(
            framerate_fps=self._m_recRate,
            shutter_fps=self._m_shutterFPS,
            width=self._m_width,
            height=self._m_height,
            pixels=self._m_nPixels,
            bits=self._m_nBits,
            frames=self._num_recorded_frames,
            epoch_time_start=self._recording_epoch_time_start,
            epoch_time_stop=self._recording_epoch_time_stop,
        )

    def Cy_get_mem_resolution(self):
        self.set_status_play()

        self._m_height, self._m_width, self._m_nPixels = (
            self._height,
            self._width,
            self._nPixels,
        )
        return (self._m_height, self._m_width, self._m_nPixels)

    def get_recorded_frames(self, n=200, verbose=0):
        self.set_status_play()
        self.get_recording_info()

        h, w, n = self._m_height, self._m_width, min(n, self._m_nRecFrames)
        frames = np.zeros((n, h, w), dtype=np.uint8)  # TODO: bit depth
        stack_memview = frames[:, :, :]

        print("\nTransfer Recording ...\n")

        for k in tqdm(range(n)):
            pCyBuf = np.random.randint(1, 256, size=h * w, dtype=np.uint8)
            for i in range(h):
                for j in range(w):
                    stack_memview[k, i, j] = pCyBuf[j + i * w]

        self._num_recorded_frames = len(frames)

        return frames

    def record_interval(self, duration=3):
        print("\nRecording ... ")
        while self.status != "recready":
            self.set_recready()

        while self.status not in ["rec", "endless"]:
            self._recording_epoch_time_start = dt.datetime.now().timestamp()
            self.trigger()

        for i in tqdm(range(int(duration / 0.5))):
            time.sleep(0.5)

        self._recording_epoch_time_stop = dt.datetime.now().timestamp()
        self.stop_recording()

    def record_nframes(self, n=200):
        duration = 3 + int(n / self.frame_rate)
        self.record_interval(duration)

        frames = self.get_recorded_frames(n=n)
        self.get_recording_info()

        return frames

    # Live ###########################################################

    def Cy_get_live_frame(self):
        pass

    def get_live_frame(self):
        h, w = self._height, self._width
        pCyBuf = np.random.randint(1, 256, size=h * w, dtype=np.uint8)
        frame = np.zeros((h, w), dtype=np.uint8)
        frame_memview = frame[:, :]
        for i in range(h):
            for j in range(w):
                frame_memview[i, j] = pCyBuf[j + i * w]

        return frame


def main():
    cam = FastcamDummy()
    print(cam)
    return cam


if __name__ == "__main__":
    cam = main()
