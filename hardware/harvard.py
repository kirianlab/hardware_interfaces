r"""
Author: Jude Tombo
Editors: Roberto Alvarez, Richard Kirian
"""
import os
import time
import serial
import sys
import numpy as np
from .utils.serial_communication_tools import identify_devices, access_config


class Pump:

    # FIXME: Can we add a method is_infusing() that checks if the pump is actually infusing?

    _diams = (None, None)

    def __init__(self,
                 com_port: str=None,
                 serial_number: str=None,
                 config_file: str=None,
                 selected_mode: str="independent"):
        r"""
        Python interface for the Harvard Apparatus pump.

        In order of priority, the connection is established from:

        * com_port
        * serial_number
        * config_file

        If none are given, the class will raise an error and no 
        object will be created. All arguments are optional, but at
        least one of the connection methods must be provided for the
        class to create an object and connect to the pump.
        The Harvard Apparatus pump has three possible modes:

        * independent   : each pump has different paramters (default)
        * reciprocating : same parameters, one infuses & one withdraws
        * twin          : same parameters, same action (infuse/withrdaw)

        Args:
            com_port (str, optional): COM port to communicate through.
            serial_number (str, optional): Serial number of the pump.
            config_file (str, optional): Configuration file pump.
            selected_mode (str, optional): The mode to put the pump in.

        Attributes:
            pump_mode (str): Mode of the pump.
            comport (str): COM port where communications are sent.
        """
        mode = {"independent": "condition Independent/\r",
                "reciprocating": "condition Reciprocating/\r",
                "twin": "condition Twin/\r"}
        self.pump_mode = selected_mode
        if self.pump_mode not in mode.keys():
            raise ValueError("selected_mode can only be 'independent', 'reciprocating', or 'twin'")
        if com_port is None:
            if serial_number is None:
                if config_file is None:
                    raise ValueError("A COM port, serial number, or config file is required.")
                else:
                    self.comport = self._from_config(config_file)
            else:
                self.comport = self._from_serial(serial_number)
        else:
            self.comport = com_port
        self.connection = serial.Serial(port = self.comport,
                                        baudrate = 9600,
                                        parity = serial.PARITY_NONE,
                                        bytesize = 8,
                                        stopbits = serial.STOPBITS_ONE,
                                        xonxoff = False,
                                        timeout = 5)  # [ms]
        self._send_command(mode[self.pump_mode])

    def _from_serial(self, serial_number: str):
        r"""
        Private function to initiate class from serial number.

        Args:
            serial_number (str): Serial number of pump to connect.

        Returns:
            com (str): COM port for device with serial number serial_number.
        """
        df = identify_devices()
        df = df.loc[df["serial_number"] == serial_number]
        com = df["com_port"].values[0]
        if sys.platform == "linux":
            com = os.path.join("/dev/", com)
        return com

    def _from_config(self, config_file: str):
        # FIXME: Implement utils.access_config()!
        # FIXME: A config file can have multiple configs for different devices; need to implement a search for the
        # FIXME: relevant part of the config (i.e. the part with "harvard" in the device type.).
        r"""
        Private function to initiate class from config file.

        Args:
            config_file (str): Configuration file.

        Returns:
            com (str): COM port for device with serial number serial_number.
        """
        config_params = access_config(config_file)
        serial_number = config_params["serial_number"]
        com = self._from_serial(serial_number)
        return com

    def _send_command(self, command: str):
        r"""
        Private function to send commands to the pump slow enough that we do not damage the pump's oboard RAM.

        Args:
            command (str): RS232 command to send to pump.

        Returns:
            None
        """
        time.sleep(0.1)
        self.connection.write(bytes(command, encoding="utf-8"))

    def _command(self, which_pump: str, command: str):
        r"""
        Private function that generates the code needed to send correct commands.

        Args:
            which_pump (str): Which pump to query ('p1', 'p2', or 'p1p2').
            command (str): RS232 command.

        Returns:
            None
        """
        pump_options = {"p1": command + " a\r",
                        "p2": command + " b\r",
                        "p1p2": command + " ab\r"}
        if which_pump in pump_options.keys():
            if self.pump_mode == "independent":
                self._send_command(pump_options[which_pump])
            else:
                self._send_command(command + "\r")
        else:
            raise ValueError("Valid options are 'p1', 'p2', or 'p1p2'")

    def _set_error(self, err_val: str):
        r"""
        Private function to handle command errors when pump is not in independent mode.

        Args:
            err_val (str): Erroneous command.

        Returns:
            None
        """
        msg = err_val + " can only be None in independent mode."
        raise ValueError(msg)

    def _get_pump_response(self):
        r"""
        Private function that converts the pump response string to a float.

        Returns:
            value (float): Value of parameter.
            units (str): Units of value.
        """
        response = self.connection.read(100)
        response_str = response.decode()
        response_lst = response_str.split(" ")
        print(response_lst)
        value = response_lst[1]
        units = response_lst[2]
        return float(value), units

    def set_syringe_volume(self, volume_p1: float=None, volume_p2: float=None):
        r"""
        Set volume [uL] of syringes.  Will not set if it is None.

        Both arguments are optional, but one must be given for the object to communicate with the pump.

        Args:
            volume_p1 (float, optional): Volume of syringe in p1 pump.
            volume_p2 (float, optional): Volume of syringe in p2 pump.

        Returns:
            None
        """
        if self.pump_mode == "independent":
            if volume_p1 is not None:
                self._send_command("svolume a {} ul/\r".format(volume_p1))
            if volume_p2 is not None:
                self._send_command("svolume b {} ul/\r".format(volume_p2))
        else:
            if volume_p1 is None:
                self._set_error("volume_p1")
            else:
                self._send_command("svolume {} ul/\r".format(volume_p1))

    def set_syringe_diameter(self, diameter_p1: float=None, diameter_p2: float=None):
        r"""
        Set diameter [mm] of syringes. Will not set if it is None.

        Both arguments are optional, but one must be
        given for the object to communicate with the pump.

        Args:
            diameter_p1 (float, optional): Diameter of syringe in p1 pump.
            diameter_p2 (float, optional): Diameter of syringe in p2 pump.

        Returns:
            None
        """
        self._diams = (diameter_p1, diameter_p2)
        if self.pump_mode == "independent":
            if diameter_p1 is not None:
                self._send_command("diameter a {} ul/\r".format(diameter_p1))
            if diameter_p2 is not None:
                self._send_command("diameter b {} ul/\r".format(diameter_p2))
        else:
            if diameter_p1 is None:
                self._set_error("diameter_p1")
            else:
                self._send_command("diameter {} ul/\r".format(diameter_p1))

    def set_target_volume(self, target_volume_p1: float=None, target_volume_p2: float=None):
        r"""
        Set target volume [uL] of syringes.
        Will not set if it is None.

        Both arguments are optional, but one must be
        given for the object to communicate with the pump.

        Args:
            target_volume_p1 (float, optional): Target volume of syringe in p1 pump.
            target_volume_p2 (float, optional): Target volume of syringe in p2 pump.

        Returns:
            None
        """
        if self.pump_mode == "independent":
            if target_volume_p1 is not None:
                self._send_command("tvolume a {} u\r".format(target_volume_p1))
            if target_volume_p2 is not None:
                self._send_command("tvolume b {} u\r".format(target_volume_p2))
        else:
            if target_p1 is None:
                self._set_error("target_p1")
            else:
                self._send_command("tvolume {} u\r".format(target_volume_p1))

    def set_infuse_rate(self, infuse_p1: float=None, infuse_p2: float=None):
        r"""
        Set infuse rate [uL/min] of syringes.
        Will not set if it is None.

        Both arguments are optional, but one must be
        given for the object to communicate with the pump.

        Args:
            infuse_p1 (float, optional): Infuse rate of syringe in p1 pump.
            infuse_p2 (float, optional): Infuse rate of syringe in p2 pump.

        Returns:
            None
        """
        if self.pump_mode == "independent":
            if infuse_p1 is not None:
                self._send_command("irate a {} u/m\r".format(infuse_p1))
            if infuse_p2 is not None:
                self._send_command("irate b {} u/m\r".format(infuse_p2))
        else:
            if infuse_p1 is None:
                self._set_error("infuse_p1")
            self._send_command("irate {} u/m\r".format(infuse_p1))

    def set_withdraw_rate(self, withdraw_p1: float=None, withdraw_p2: float=None):
        r""" 
        Set withdraw rate [uL/min] of syringes.
        Will not set if it is None.

        Both arguments are optional, but one must be
        given for the object to communicate with the pump.

        Args:
            withdraw_p1 (float, optional): Withdraw rate of syringe in p1 pump.
            withdraw_p2 (float, optional): Withdraw rate of syringe in p2 pump.

        Returns:
            None
        """
        if self.pump_mode == "independent":
            if withdraw_p1 is not None:
                self._send_command("wrate a {} u/m\r".format(withdraw_p1))
            if withdraw_p2 is not None:
                self._send_command("wrate b {} u/m\r".format(withdraw_p2))
        else:
            if withdraw_p1 is None:
                self._set_error("withdraw_p1")
            else:
                self._send_command("wrate {} u/m\r".format(withdraw_p1))

    def set_force(self, force_p1: int=None, force_p2: int=None):
        r"""
        Set force of syringes as percentage of pump maximum (70 lbs).
        Valid range is 1 to 100. Will not set if it is None.

        Both arguments are optional, but one must be
        given for the object to communicate with the pump.

        Args:
            force_p1 (float, optional): Force to apply to syringe in p1 pump.
            force_p2 (float, optional): Force to apply to syringe in p2 pump.

        Returns:
            None
        """
        if force_p1 or force_p2 not in range(1, 101):
            raise ValueError("Valid range is 1 - 100.")
        else:
            if self.pump_mode == "independent":
                if force_p1 is not None:
                    self._send_command("force a {}\r".format(force_p1))
                if force_p2 is not None:
                    self._send_command("force b {}\r".format(force_p2))
            else:
                if force_p1 is None:
                    self._set_error("force_p1")
                else:
                    self._send_command("force {}\r".format(force_p1))

    def set_parameters(self, p1_params_dict: dict=None, p2_params_dict: dict=None):
        r"""
        Set syringe parameters. Must pass a dictionary for each pump with the keys:

        * syringe_diameter: float
        * syringe_volume: float
        * target_volume: float
        * infuse_rate: float
        * withdraw_rate: float
        * force: float

        The force must be percentage of pump maximum (70 lbs). Valid range is 1 to 100.

        Args:
            p1_params_dict (dict): Paramters to apply to pump p2.
            p2_params_dict (dict): Paramters to apply to pump p2.

        Returns:
            None
        """
        keyvals = ["syringe_diameter", "syringe_volume", "target_volume", "infuse_rate", "withdraw_rate", "force"]
        if p1_params_dict is None:
            p1_params_dict = {k: None for k in keyvals}
        if p2_params_dict is None:
            p2_params_dict = {k: None for k in keyvals}
        self.set_syringe_diameter(p1_params_dict["syringe_diameter"], p2_params_dict["syringe_diameter"])
        self.set_syringe_volume(p1_params_dict["syringe_volume"], p2_params_dict["syringe_volume"])
        self.set_target_volume(p1_params_dict["target_volume"], p2_params_dict["target_volume"])
        self.set_infuse_rate(p1_params_dict["infuse_rate"], p2_params_dict["infuse_rate"])
        self.set_withdraw_rate(p1_params_dict["withdraw_rate"], p2_params_dict["withdraw_rate"])
        # self.set_force(p1_params_dict["force"], p2_params_dict["force"])

    def get_syringe_volume(self, which_pump: str):
        r"""
        Get syringe volume [uL].

        Args:
            which_pump (str): Which pump to query ('p1', 'p2', or 'p1p2').

        Returns:
            value (float): Syringe volume for syringe in pump which_pump.
        """
        self._command(which_pump, "svolume")
        value, units = self._get_pump_response()
        return value

    def get_syringe_diameter(self, which_pump: str):
        r"""
        Get syringe diameter [mm].

        Args:
            which_pump (str): Which pump to query ('p1', 'p2', or 'p1p2').

        Returns:
            value (float): Syringe diameter for syringe in pump which_pump.
        """
        if which_pump == 'p1':
            return self._diams[0]
        if which_pump == 'p2':
            return self._diams[1]
        return None
        # self._command(which_pump, "diameter")
        # value, units = self._get_pump_response()
        # return value

    def get_target_volume(self, which_pump: str):  # FIXME: return the values as numbers
        r"""
        Get target volume [uL].

        Args:
            which_pump (str): Which pump to query ('p1', 'p2', or 'p1p2').

        Returns:
            value (float): Target volume for syringe in pump which_pump.
        """
        self._command(which_pump, "tvolume")
        value, units = self._get_pump_response()
        return value


    def get_infuse_rate(self, which_pump: str):  # FIXME: return the values as numbers
        r"""
        Get infuse rate [uL/min].

        Args:
            which_pump (str): Which pump to query ('p1', 'p2', or 'p1p2').

        Returns:
            value (float): Infuse rate value for syringe in pump which_pump.
        """
        self._command(which_pump, "irate")
        value, units = self._get_pump_response()
        return value

    def get_withdraw_rate(self, which_pump: str):  # FIXME: return the values as numbers
        r"""
        Get withdraw rate [uL/min].

        Args:
            which_pump (str): Which pump to query ('p1', 'p2', or 'p1p2').

        Returns:
            value (float): Withdraw rate value for syringe in pump which_pump.
        """
        self._command(which_pump, "wrate")
        value, units = self._get_pump_response()
        return value

    def get_force(self, which_pump: str):  # FIXME: return the values as numbers
        r"""
        Get pump force [N].

        Args:
            which_pump (str): Which pump to query ('p1', 'p2', or 'p1p2').

        Returns:
            pressure (float): Force pump is applying to syringe.
        """
        MAX_FORCE = 70.0
        self._command(which_pump, "force")
        response = self.connection.read(100)
        val = "".join(map(chr, response))
        # Extracting the force percentage number from the pump
        i = 0
        forceP = ""
        while i < len(val):
            if val[i] != ":" and val[i].isdigit():
                forceP += val[i]
            i += 1
        force_percentage = int(forceP) / 100
        force_lbs = MAX_FORCE * force_percentage
        force = force_lbs * 4.448220  # convert [lbs] to [N]
        return force

    def get_pressure(self, which_pump: str):  # FIXME: return the values as numbers
        r"""exi
        Get pump pressure [N/mm^2].

        Args:
            which_pump (str): Which pump to query ('p1', 'p2', or 'p1p2').

        Returns:
            pressure (float): Pressure pump is applying to syringe.
        """
        # FIXME: This method is failing
        return 0
        # force = self.get_force(which_pump)
        # diameter = self.get_syringe_diameter(which_pump)
        # # Setting the pump zero infusion rate max as 15 ul
        # # self._send_command("diameter ab 4\r")
        # # response = self.connection.read(100).decode()
        # # Extracts the number from the string
        # # nbr2 = [int(s) for s in response.split() if s.isdigit()]
        # area = np.pi * diameter ** 2
        # pressure = force / area
        # return pressure

    def get_current_parameters(self, which_pump: str):
        r"""
        Get current pump parameters.

        Args:
            which_pump (str): Which pump to communicate with ('p1', 'p2', or 'p1p2').

        Returns:
            params (dict): Current pump parameters for which_pump.
        """
        params = {"syringe_diameter": self.get_syringe_diameter(which_pump),
                  "syringe_volume": self.get_syringe_volume(which_pump),
                  "target_volume": self.get_target_volume(which_pump),
                  "infuse_rate": self.get_infuse_rate(which_pump),
                  "withdraw_rate": self.get_withdraw_rate(which_pump),
                  "force": self.get_force(which_pump),
                  "pressure": self.get_pressure(which_pump)}
        return params

    def start_infuse(self, which_pump: str):
        r"""
        Start the pump in infuse mode.

        Args:
            which_pump (str): Which pump to communicate with ('p1', 'p2', or 'p1p2').

        Returns:
            None
        """
        self._command(which_pump, "irun")

    def start_withdraw(self, which_pump: str):
        r"""
        Start the pump in withdraw mode.

        Args:
            which_pump (str): Which pump to communicate with ('p1', 'p2', or 'p1p2').

        Returns:
            None
        """
        self._command(which_pump, "wrun")

    def stop_pump(self, which_pump: str):
        r"""
        Stop the pump.

        Args:
            which_pump (str): Which pump to communicate with ('p1', 'p2', or 'p1p2').

        Returns:
            None
        """
        self._command(which_pump, "stop")

    def __del__(self):
        self.stop_pump('p1')
        self.stop_pump('p2')


Pump33DDS = Pump
