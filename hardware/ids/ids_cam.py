# core
import ctypes
import h5py
import numpy as np
import sys

# graphis
import cv2
import pyqtgraph as pg
from pyqtgraph.Qt import QtCore, QtGui
import pyqtgraph.ptime as ptime

# sdk
sys.path.append("../hardware_interfaces_sdk/ids")
import ueye


class IDS_Cam:

    def __init__(self, 
                 cam_num: int=0, 
                 n_bits: int=8,
                 rgb_channels: int=1):
        r"""
        Python wrapper for IDS cameras (most Thorlabs cameras).

        Args:
            cam_num (int): 0-254.
                           0 => First available camera, 
                           1-254 => camera choice
                           FIX ME: needs to utilize serial number instead
            n_bits (int): 8 or 24.
                          8 bits per pixel => monochrome, 
                          24 bits per pixel => color mode
            rgb_channels (int): 1 or 3.
                                1 channel => monochrome,
                                3 channels => RGB
            h5_name (str): Name of the h5 file to save data to. 
        """
        # Camera Variables
        self.hcam = ueye.HIDS(cam_num)
        self.s_info = ueye.SENSORINFO()
        self.c_info = ueye.CAMINFO()
        self.pc_image_memory = ueye.c_mem_p()
        self.mem_id = ueye.int()
        self.rect_aoi = ueye.IS_RECT()
        self.pitch = ueye.INT()
        self.n_bits_per_image = ueye.INT(n_bits)
        self.channels = rgb_channels
        self.m_n_colormode = ueye.INT()       # Y8/RGB16/RGB24/REG32
        self.bytes_per_pixel = int(self.n_bits_per_image / 8)
        self.aoi_seq_params = ueye.AOI_SEQUENCE_PARAMS()
        print("Camera initializing... \n")

        # Starts the driver and establishes the connection to the camera
        if ueye.is_InitCamera(self.hcam, None) != ueye.IS_SUCCESS:
            print("is_InitCamera ERROR")
        # Reads out the data hard-coded in the non-volatile camera memory and writes it to the data structure that c_info points to
        if ueye.is_GetCameraInfo(self.hcam, self.c_info) != ueye.IS_SUCCESS:
            print("is_GetCameraInfo ERROR")
        # You can query additional information about the sensor type used in the camera
        if ueye.is_GetSensorInfo(self.hcam, self.s_info) != ueye.IS_SUCCESS:
            print("is_GetSensorInfo ERROR")

        if ueye.is_ResetToDefault(self.hcam) != ueye.IS_SUCCESS:
            print("is_ResetToDefault ERROR")

        # Set display to Bitmap mode (Device Independent Bitmap, DIB)
        status = ueye.is_SetDisplayMode(self.hcam, ueye.IS_SET_DM_DIB)

        # Set the right color mode
        if int.from_bytes(self.s_info.nColorMode.value, byteorder='big') == ueye.IS_COLORMODE_BAYER:
            # setup the color depth to the current windows setting
            ueye.is_GetColorDepth(self.hcam, self.n_bits_per_image, self.m_n_colormode)
            self.bytes_per_pixel = int(self.n_bits_per_image / 8)
            print("IS_COLORMODE_BAYER")

        elif int.from_bytes(self.s_info.nColorMode.value, byteorder='big') == ueye.IS_COLORMODE_CBYCRY:
            # for color camera models use RGB32 mode
            self.m_n_colormode = ueye.IS_CM_BGRA8_PACKED
            self.n_bits_per_image = ueye.INT(32)
            self.bytes_per_pixel = int(self.n_bits_per_image / 8)
            print("IS_COLORMODE_CBYCRY")

        elif int.from_bytes(self.s_info.nColorMode.value, byteorder='big') == ueye.IS_COLORMODE_MONOCHROME:
            # for color camera models use RGB32 mode
            self.m_n_colormode = ueye.IS_CM_MONO8
            self.n_bits_per_image = ueye.INT(8)
            self.bytes_per_pixel = int(self.n_bits_per_image / 8)
            print("IS_COLORMODE_MONOCHROME")

        else:
            # for monochrome camera models use Y8 mode
            self.m_n_colormode = ueye.IS_CM_MONO8
            self.n_bits_per_image = ueye.INT(8)
            self.bytes_per_pixel = int(self.n_bits_per_image / 8)
            print("else")

        print(f"""\t m_n_colormode:\t\t{self.m_n_colormode}\n
                  \tn_bits_per_image:\t\t{self.n_bits_per_image}\n
                  \t bytes_per_pixel:\t\t{self.bytes_per_pixel}""")

        print("TESTING!: ", self.rect_aoi.s32Width.value)

        # Can be used to set the size and position of an "area of interest"(AOI) within an image
        if ueye.is_AOI(self.hcam, ueye.IS_AOI_IMAGE_GET_AOI, self.rect_aoi, ueye.sizeof(self.rect_aoi)) != ueye.IS_SUCCESS:
            print("is_AOI ERROR")

        self.width = self.rect_aoi.s32Width
        self.height = self.rect_aoi.s32Height

        # Prints out some information about the camera and the sensor
        print("Camera model:\t\t", self.s_info.strSensorName.decode('utf-8'))
        print("Camera serial no.:\t", self.c_info.SerNo.decode('utf-8'))
        print("Maximum image width:\t", self.width)
        print("Maximum image height:\t", self.height)

        # Allocates an image memory for an image having its dimensions defined by width and height and its color depth defined by n_bits_per_image
        if ueye.is_AllocImageMem(self.hcam, self.width, self.height, self.n_bits_per_image, self.pc_image_memory, self.mem_id) != ueye.IS_SUCCESS:
            print("is_AllocImageMem ERROR")
        else:
            # Makes the specified image memory the active memory
            status = ueye.is_SetImageMem(self.hcam, self.pc_image_memory, self.mem_id)
            if status != ueye.IS_SUCCESS:
                print("is_SetImageMem ERROR")
            else:
                # Set the desired color mode
                status = ueye.is_SetColorMode(self.hcam, self.m_n_colormode)

        print('Camera ready for use!')

        self.init_cam()

    def init_cam(self):
        if ueye.is_CaptureVideo(self.hcam, ueye.IS_DONT_WAIT) != ueye.IS_SUCCESS:
            print("is_CaptureVideo ERROR")
        # Enables the queue mode for existing image memory sequences
        status = ueye.is_InquireImageMem(self.hcam, self.pc_image_memory, self.mem_id, self.width, self.height, self.n_bits_per_image, self.pitch)
        if status != ueye.IS_SUCCESS:
            print("is_InquireImageMem ERROR")

    def close_camera(self):
        # Releases an image memory that was allocated using is_AllocImageMem() and removes it from the driver management
        ueye.is_FreeImageMem(self.hcam, self.pc_image_memory, self.mem_id)
        # Disables the hcam camera handle and releases the data structures and memory areas taken up by the uEye camera
        ueye.is_ExitCamera(self.hcam)

    def set_triggered_mode(self):
        # set the trigger to wait for rising edge of electrical signal
        if ueye.is_SetExternalTrigger(self.hcam, ueye.IS_SET_TRIGGER_LO_HI) != ueye.IS_SUCCESS:
            raise ValueError("is_CaptureVideo ERROR")
        # Enables the queue mode for existing image memory sequences
        status = ueye.is_InquireImageMem(self.hcam, self.pc_image_memory, self.mem_id, self.width, self.height, self.n_bits_per_image, self.pitch)
        if ueye.is_CaptureVideo(self.hcam, ueye.IS_DONT_WAIT) != ueye.IS_SUCCESS:
            print("is_FreezeVideo ERROR")
        if status != ueye.IS_SUCCESS:
            raise ValueError("is_InquireImageMem ERROR")

    def get_live_frame(self):
        # extract the data from camera memory
        array = ueye.get_data(self.pc_image_memory, self.width, self.height, self.n_bits_per_image, self.pitch, copy=False)
        # reshape it in a numpy array
        frame = np.reshape(array,(self.height.value, self.width.value, self.bytes_per_pixel))
        return frame

    def rec_nframes(self, nframes: int):
        if nframes == 0:
            print('Number of frames must be greater than zero!')
            pass

        status = ueye.is_InquireImageMem(self.hcam, self.pc_image_memory, self.mem_id, self.width, self.height, self.n_bits_per_image, self.pitch)

        # initialize camera
        status = ueye.is_CaptureVideo(self.hcam, ueye.IS_DONT_WAIT)
        
        data = np.zeros((nframes, self.height.value, self.width.value))
        if status != ueye.IS_SUCCESS:
            print("is_InquireImageMem ERROR")

        for n in range(nframes):
            status = ueye.is_FreezeVideo(self.hcam, ueye.IS_WAIT)
            data[n, :, :] = self.get_live_frame()[:, :, 0]

        return data

    def rec_nframes_triggered(self, nframes:int):
        if nframes == 0:
            print('Number of frames must be greater than zero!')
            pass

        # initialize camera
        # status = ueye.is_CaptureVideo(self.hcam, ueye.IS_DONT_WAIT)
        status = ueye.is_SetExternalTrigger(self.hcam, ueye.IS_SET_TRIGGER_LO_HI)
        if status != ueye.IS_SUCCESS:
            print("is_SetExternalTrigger ERROR")
        status = ueye.is_InquireImageMem(self.hcam, self.pc_image_memory, self.mem_id, self.width, self.height, self.n_bits_per_image, self.pitch)
        
        data = np.zeros((nframes, self.height.value, self.width.value))
        if status != ueye.IS_SUCCESS:
            print("is_InquireImageMem ERROR")

        for n in range(nframes):
            vid_freeze_status = ueye.is_FreezeVideo(self.hcam, ueye.IS_WAIT)
            data[n, :, :] = self.get_live_frame()[:, :, 0]

        return data

    def set_framerate(self, fps):
        r"""
        Arguments
        ---------
            fps: desired framerate

        Returns
        -------
            new_fps: new framerate
            status: return code (0 means success, 1 means failure)
        """
        new_fps = ctypes.c_double()
        status = ueye.is_SetFrameRate(self.hcam, fps, ctypes.byref(new_fps))
        return new_fps.vlaue, status

    def set_exposure(self, val: float):
        r"""
        Arguments
        ---------
            val (float): desired exposure value in ms
        """
        expose_val = ctypes.c_double(val)
        status = ueye.is_Exposure(self.hcam, ueye.IS_EXPOSURE_CMD_SET_EXPOSURE, 
                            expose_val, ueye.sizeof(expose_val))
        if status != ueye.IS_SUCCESS:
            self._errors(status)
        if status == ueye.IS_SUCCESS:
            self.exposure = expose_val

    def _errors(self, status):
        #FIXME: Update this to include all error codes
        #FIXME: Put this into an ids_config file and read the file here
        #FIXME: Instead of the general output, return the error descriptions from the thorlabs docs
        err_dict = {-1: "IS_NO_SUCCESS",
                     0: "IS_SUCCESS",
                     1: "IS_INVALID_CAMERA_HANDLE",
                     2: "IS_REQUEST_FAILED",
                     3: "IS_CANT_OPEN_DEVICE",
                     11: "IS_CANT_OPEN_REGISTRY",
                     12: "IS_CANT_READ_REGISTRY",
                     15: "IS_NO_IMAGE_MEM_ALLOCATED",
                     16: "IS_CANT_CLEANUP_MEMORY",
                     17: "IS_CANT_CLEANUP_MEMORY",
                     18: "IS_FUNCTION_NOT_SUPPORTED_YET",
                     32: "IS_INVALID_CAPTURE_MODE", 
                     49: "IS_INVALID_MEMORY_POINTER", 
                     50: "IS_FILE_WRITE_OPEN_ERROR",
                     51: "IS_FILEREAD_OPEN_ERROR", 
                     52: "IS_FILE_READ_INVALID_BMP_ID",
                     53: "IS_FILE_READ_INVALID_BMP_SIZE",
                     108: "IS_NO_ACTIVE_IMG_MEM",
                     112: "IS_SEQUENCE_LIST_EMPTY"
                        }
        try:
            print(err_dict[status])
        except KeyError:
            print("Error value not yet in dictionary... look up manually.")


class IDS_Cam_Live:

    def __init__(self):
        self.cam = IDS_Cam()

    def opencv_live_mode(self):
        # Continuous image display
        while(True):
            # resize the image by a half
            frame = cv2.resize(self.cam.get_live_frame(),(0,0),fx=0.5, fy=0.5)
            # and finally display it
            cv2.imshow(f"Live view: Camera {self.cam.s_info.strSensorName.decode('utf-8')}", frame)

            # Press q if you want to end the loop
            if cv2.waitKey(1) & 0xFF == ord("q"):
                break

        # destroys the OpenCv windows
        cv2.destroyAllWindows()

    def pyqt_live_mode(self):
        app = QtGui.QApplication([])

        ## Create window with GraphicsView widget
        win = pg.GraphicsLayoutWidget()
        win.show()  ## show widget alone in its own window
        win.setWindowTitle('pyqtgraph example: ImageItem')
        view = win.addViewBox()

        ## lock the aspect ratio so pixels are always square
        view.setAspectLocked(True)

        ## Create image item
        self._img = pg.ImageItem(border='w')
        view.addItem(self._img)

        ## Set initial view bounds
        view.setRange(QtCore.QRectF(0, 0, 600, 600))

        ## Create random image
        self._data = self.cam.get_live_frame()
        self._i = 0

        self._updateTime = ptime.time()
        self._fps = 0


        def updateData():
            # global img, data, i, updateTime, fps
            img = self._img
            data = self._data
            i = self._i
            updateTime = self._updateTime
            fps = self._fps

            ## Display the data
            img.setImage(self.cam.get_live_frame())
            i = (i+1) % data.shape[0]

            QtCore.QTimer.singleShot(1, updateData)
            now = ptime.time()
            fps2 = 1.0 / (now-updateTime)
            updateTime = now
            fps = fps * 0.9 + fps2 * 0.1

        while True:
            updateData()
            pg.mkQApp().processEvents()

    def opencv_live_triggered_mode(self):
        self.cam.set_triggered_mode()
        self.opencv_live_mode()

    def pyqt_live_triggered_mode(self):
        self.cam.set_triggered_mode()
        self.pyqt_live_mode()


class h5Tools:

    def __init__(self, h5_name):
        self.h5_name = h5_name

    def h5_outpath(self, outpath, verbose=True):
        r"""
        Creates the file path for the h5 save location

        Arguments
        ---------
            outpath (str): File path to the save location
            verbose (bool): If True, prints save location. Else, prints nothing.
        """
        try:
            os.mkdirs(outpath)
        except FileExistsError:
            pass
        if verbose is True:
            print("Files will be saved here: \n \t\t {}".format(outpath))

    def write_data_as_h5(self, *args, group_name="default", verbose=True):
        r"""
        Writes a data group to a specified h5 file. 

        Arguments
        ----------
            *args (tuple): inputs must follow this format ("data_name", data). 
                            The *args allows for multiple entries in a single group. 
                            This function must be called once for each h5 group.
                                Example: 
                                    write_data_as_h5(("camera_frames", frames), 
                                                     ("time_stamp", time_stamp_data),
                                                       group_name="raw_frames")
                                In the example, we see that two data subgroups are made within
                                the "raw_frames" group.
            group_name (str): Name of data group. Can be recursive, 
                                Example:
                                    group_name="config/pixel_size"
                                In the example, the pixel_size group is made within the config
                                group. This allows nested groups for data to be saved into.
            verbose (bool): If True, prints group name. Else, prints nothing.
        """
        try:
            "Append to already made h5"
            h5 = h5py.File(h5_name, 'a')
        except ValueError:
            "Create the h5 first, then append the data"
            h5 = h5py.File(h5_name, 'w')
        try:
            "Create group if it does not exist"
            group = h5.create_group(self.group_name)
        except ValueError:
            "Appends to group if it already exists"
            group = h5["{}".format(group_name)]

        for arg in args:
            "Save the dataset"
            group.create_dataset(arg[0], data=arg[1])

        h5.close()
        if verbose is True:
            print("Created the {} group!".format(group_name))
            print("Closed {}!".format(self.h5_name))
