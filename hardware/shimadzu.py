# Using the code available here:
#   https://github.com/paulscherrerinstitute/shimadzu_pump
# First install the dependencies:
#   conda install requests pcaspy
#   conda install -c paulscherrerinstitute shimadzu_pump
import os
import time
import threading
import json
import numpy as np
import pyqtgraph as pg
from shimadzu_pump import shimadzu_driver
from .utils import time_tools
from .utils import zmq_tools
from .utils import loggers


class HPLCDevice(shimadzu_driver.ShimadzuCbm20):
    r"""Interface to Shimadzu HPLC pump.  You need to install the following:

    conda install -c paulscherrerinstitute shimadzu_pump

    This class is a thin wrapper around the above module.
    """
    name = "LC-20AD"
    debug_threshold = 0
    sensor_name = None
    last_time = 0
    buffering = False
    thread = None
    current_data = None

    def _dbgmsg(self, *args, level=1, **kwargs):
        if level <= self.debug_threshold:
            print("HPLC:", *args, **kwargs)

    def __init__(self, config=None, ip_address=None, username=None, password=None):
        ip_address = config.get("ip_address", None)
        self.ip_address = ip_address
        username = config.get("username", None)
        password = config.get("password", None)
        polling_frequency = config.get("polling_frequency", 10)
        if ip_address is None:
            raise ValueError(
                "You must provide an IP address for the Shimadzu HPLC pump."
            )
        super().__init__(ip_address)
        self.login(username, password)
        self.set("pressure_unit", 1)
        self.buffer_length = int(12 * 3600 * polling_frequency)
        self.buffer_interval = 1 / polling_frequency
        self.logging = config['logging']    # whether to log data or not
        self.log_directory = (
            config["log_file_path"] + "/shimadzu_" + self.name + "_" + ip_address + "/"
        )
        os.makedirs(self.log_directory, exist_ok=True)
        self.status_file_path = self.log_directory + "status.json"
        self.log_file_path = None
        self.datalog = None
        self.setup_datalog()
        self.start_buffering()
        self._dbgmsg("Shimadzu initialized")

    def setup_datalog(self):
        filepath = os.path.dirname(self.status_file_path) + "/data.log"
        data_names = ("epoch_time", "pressure", "flow_setpoint", "is_pumping")
        data_formats = (r"%f", r"%f", r"%f", r"%d")
        self.datalog = loggers.DataLogger(
            filepath, data_names=data_names, data_formats=data_formats, interval=10
        )

    def log_datapoint(self):
        if self.logging:
            a = self.current_data
            self.datalog.log_data(time.time(), a["pressure"], a["flow"], bool(a["pumping"]))

    def get_status_dict(self):
        r"""Return dictionary with the status of all important aspects of the device."""
        a = self.get_all()
        if a["pressure_unit"] == 1:
            a["pressure_units"] = "PSI"
        else:
            a["pressure_units"] = None
        a["flow_rate"] = a["flow"] * 1e3
        a["flow_rate_units"] = "ul/min"
        a = time_tools.append_timestamps_to_dict(a)
        a["name"] = self.name
        return a

    def is_pumping(self):
        a = self.get_all()
        return bool(a["pumping"])

    def set_flow_rate(self, flow):
        r"""Set volumetric flow rate in ul/min units."""
        self.set("flow", flow * 1e-3)

    def get_flow_setpoint(self):
        r"""Get the flow rate setpoint in ul/min."""
        a = self.get_status_dict()
        return time.time(), a["flow"] * 1e3  # TODO

    def get_pressure(self):
        r"""Get the pressure reading in PSI units."""
        a = self.get_status_dict()
        return time.time(), a["pressure"]*100

    def set_max_pressure(self, val):
        r"""Set the maximum pressure in PSI units. """
        if self.get('max_pressure') != val:
            # print(f"! Attempting to set max pressure to {val}. Currently: {self.get('max_pressure')}")
            self.set('max_pressure', val)
            # time.sleep(0.1)

    def set_min_pressure(self, val):
        r"""Set the minimum pressure in PSI units."""
        if self.get('min_pressure') != val:
            # print(f"! Attempting to set min pressure to {val}. Currently: {self.get('min_pressure')}")
            self.set('min_pressure', val)
            # time.sleep(0.1)

    def get_max_pressure(self):
        r"""Get the maximum pressure in PSI units."""
        return time.time(), self.get('max_pressure')

    def get_min_pressure(self):
        r"""Get the minimum pressure in PSI units."""
        return time.time(), self.get('min_pressure')

    def start_pump(self):
        r"""Start the pump."""
        self.start()

    def stop_pump(self):
        r"""Stop the pump."""
        self.stop()

    def clear_error(self):
        r"""Clear the error."""
        self.set("clear_error", 1)

    def start_buffering(self):
        self._dbgmsg("start_buffering")
        self.thread = threading.Thread(target=self.buffer_thread)
        self.buffering = True
        self.thread.start()

    def stop_buffering(self):
        self._dbgmsg("stop_buffering")
        self.buffering = False
        if self.thread is not None:
            time.sleep(2 * self.buffer_interval)
            self.thread.join()

    def buffer_thread(self):
        self._dbgmsg("buffer_thread", level=2)
        status_time = np.floor(time.time())
        while self.buffering:
            t = time.time()
            if (t - self.last_time) > self.buffer_interval:
                self.last_time = t
                data = self.get_status_dict()
                self.current_data = data
                self.log_datapoint()
                if np.abs(status_time - t) > 1:  # Update status file at 1 Hz
                    status_time = np.floor(t)
                    self.write_status_json_text()
            time.sleep(
                max(np.abs(self.buffer_interval - (time.time() - self.last_time)), 0)
            )

    def write_status_json_text(self):
        filepath = self.status_file_path
        with open(filepath, "w") as f:
            json.dump(self.current_data, f, indent=4)

    def disconnect(self):
        r"""Disconnect from the device."""
        self.stop_buffering()
        self.logout()

    def __del__(self):
        self.disconnect()


class HPLCDeviceDummy:
    name = "Unknown"
    max_pressure = 4000
    min_pressure = 0

    def __init__(self, config=None, ip_address=None, status_file=None, name="LC-20AD"):
        self._flow_rate = 0
        self._is_pumping = False

    def get_all(self):
        a = {}
        a['pumping'] = self._is_pumping
        a["pressure_units"] = "PSI"
        a["flow_rate"] = self._flow_rate
        a["flow_rate_units"] = "ul/min"
        a["name"] = "Dummy HPLC"
        return a

    def get_status_dict(self):
        r"""Return dictionary with the status of all important aspects of the device."""
        a = time_tools.append_timestamps_to_dict(self.get_all())
        return a

    def set_flow_rate(self, flow):
        self._flow_rate = flow

    def is_pumping(self):
        a = self.get_all()
        return bool(a["pumping"])

    def get_flow_setpoint(self):
        return time.time(), 0  # TODO:

    def get_pressure(self):
        return time.time(), 100  # TODO

    def set_max_pressure(self, val):
        self.max_pressure = val

    def set_min_pressure(self, val):
        self.min_pressure = val

    def get_max_pressure(self):
        return time.time(), self.max_pressure

    def get_min_pressure(self):
        return time.time(), self.min_pressure

    def disconnect(self):
        pass

    def start_pump(self):
        self._is_pumping = True

    def stop_pump(self):
        self._is_pumping = False

    def clear_error(self):
        pass

# For backward compatibility
# hplc = HPLCDevice


def make_config(zmq_port=5559, dummy=False):
    r"""Create a standard configuration dictionary for use with Shimadzu HPLC Device, Client and Server classes.

    Arguments:
        zmq_port (int): The port to be used for server/client instances.
    """
    default_config = {
        "device_class": HPLCDevice,  # This is the class that should be instantiated for this device.
        #"ip_address": "192.168.200.99",
        "ip_address": "10.139.1.94",
        "username": "raklab",
        "password": "sky2Blue",
        "polling_frequency": 10,  # Specific to this type of device
        "log_file_path": "D:/data/microjets/logs",  # Where to keep log data
        "logging": True,  # Write log files?
        "zmq_client_address": "tcp://localhost:PORT",  # Make sure the port is unique to this device.
        "zmq_server_address": "tcp://*:PORT",  # Make sure this port is matching the above.
        "zmq_daemon_pid_file": "/tmp/zmq_daemon_PORT.pid",  # This is for creating a daemon server, if desired.
    }
    if dummy:
        default_config["device_class"] = HPLCDeviceDummy
    if zmq_port is not None:
        default_config["zmq_client_address"] = default_config[
            "zmq_client_address"
        ].replace("PORT", zmq_port.__str__())
        default_config["zmq_server_address"] = default_config[
            "zmq_server_address"
        ].replace("PORT", zmq_port.__str__())
        default_config["zmq_daemon_pid_file"] = default_config[
            "zmq_daemon_pid_file"
        ].replace("PORT", zmq_port.__str__())
    return default_config


class HPLCClient(zmq_tools.GenericZMQClient):
    r"""ZMQ client for Shimadzu HPLC.  See GenericZMQClient superclass for more details and keyword
    arguments."""

    def __init__(self, config=None, **kwargs):
        r"""
        Arguments:
            config (dict): Configuration parameters.  Use make_config() to get a starting point.
            start_server (bool): Set to True if you want to start a server upon instantiation of the client.
        """
        if config is None:
            raise ValueError(
                'Your config dictionary is None.  Use "make_config" and modify as appropriate.'
            )
        super().__init__(config=config, **kwargs)

    def set_flow(self, flow):
        r"""See HPLCDevice documentation."""
        return self.get(method="set_flow_rate", args=[flow])

    def get_flow_setpoint(self):
        r"""See HPLCDevice documentation."""
        return self.get(method="get_flow_setpoint")

    def get_pressure(self):
        r"""See HPLCDevice documentation."""
        return self.get(method="get_pressure")

    def get_min_pressure(self):
        r"""See HPLCDevice documentation."""
        return self.get(method="get_min_pressure")

    def get_max_pressure(self):
        r"""See HPLCDevice documentation."""
        return self.get(method="get_max_pressure")

    def set_min_pressure(self, val):
        r"""See HPLCDevice documentation."""
        return self.get(method="set_min_pressure", args=[val])

    def set_max_pressure(self, val):
        r"""See HPLCDevice documentation."""
        return self.get(method="set_max_pressure", args=[val])

    def get_status_dict(self):
        r"""See HPLCDevice documentation."""
        return self.get(method="get_status_dict")

    def start_pump(self):
        r"""See HPLCDevice documentation."""
        return self.get(method="start_pump")

    def stop_pump(self):
        r"""See HPLCDevice documentation."""
        return self.get(method="stop_pump")

    def is_pumping(self):
        r"""See HPLCDevice documentation."""
        return self.get(method="is_pumping")

    def clear_error(self):
        r"""See HPLCDevice documentation."""
        return self.get(method="clear_error")

    def start(self):
        r"""See HPLCDevice documentation."""
        return self.get(method="start")


class HPLCServer(zmq_tools.GenericZMQServer):
    r"""ZMQ server for Shimadzu HPLC.  See GenericZMQServer superclass for more details and keyword
    arguments."""

    def __init__(self, config=None, **kwargs):
        r"""
        Arguments:
            config (dict): Configuration parameters.  Use make_config() to get a starting point.
            threaded (bool): Start the server in a thread to avoid blocking.
        """
        if config is None:
            raise ValueError(
                'Your config dictionary is None.  Use "make_config" and modify as appropriate.'
            )
        super().__init__(config=config, **kwargs)


class HPLCWidget(pg.Qt.QtWidgets.QWidget):
    r"""A for the Shimadzu HPLC device class.  Creates a ShimadzuClient, which creates a ShimadzuServer, which
    creates a Shimadzu instance."""

    def __init__(self, config):
        r"""
        Arguments:
            config (dict): Configurations.  TODO: Document.
        """
        self._dbgmsg("Initializing.")
        app = pg.mkQApp()
        app.aboutToQuit.connect(self.quit)
        self.app = app
        super().__init__()
        self.config = config
        self.buffer_idx = 0
        self.buffer_length = int(1e5)
        self.buffer_update_interval = 0.1
        self.plot_update_interval = 0.1
        self.ring_buffer_setpoint = np.zeros(self.buffer_length)
        self.ring_buffer_pressure = np.zeros(self.buffer_length)
        self.ring_buffer_time = np.zeros(self.buffer_length)
        self.is_initialized = False
        self.is_paused = False
        self.do_update_plot = True
        self.client = HPLCClient(config=config, start_server=True)
        self._setup_interface()

    def _dbgmsg(self, *args, **kwargs):
        print("ShimadzuWidget:", *args, **kwargs)

    def _setup_interface(self):
        main_layout = pg.Qt.QtWidgets.QVBoxLayout()
        # Setup plot window
        plt_layout = (
            pg.Qt.QtWidgets.QHBoxLayout()
        )  # QGridLayout() for grid layout, or QSlider() for slider window thing
        plot = pg.PlotWidget()
        # plot.addLegend()
        plot.setLabel("bottom", "Relative Time (s)")
        plot.setLabel("left", "Pressure (PSI)")
        plot.setTitle("Shimadzu HPLC Pump")
        pg.setConfigOptions(antialias=True)
        curve_pressure = plot.plot(
            name="Pressure (PSI) x0.01",
            pen='g',
            symbol="o",
            symbolPen=None,
            symbolBrush="g",
            symbolSize=3,
        )
        # curve_setpoint = plot.plot(name="Flow SP (ul/min)", pen="r")
        plt_layout.addWidget(plot)
        main_layout.addItem(plt_layout)
        # Setup device status widget (row of widgets)
        btn_layout = pg.Qt.QtWidgets.QHBoxLayout()
        # Flow setpoint status
        btn_layout.addWidget(pg.Qt.QtWidgets.QLabel("Flow setpoint:"))
        self.setpoint_label = pg.Qt.QtWidgets.QLabel("")
        btn_layout.addWidget(self.setpoint_label)
        # Pressure status
        btn_layout.addWidget(pg.Qt.QtWidgets.QLabel("Pressure:"))
        self.pressure_label = pg.Qt.QtWidgets.QLabel("")
        btn_layout.addWidget(self.pressure_label)
        main_layout.addItem(btn_layout)
        # Pump status
        btn_layout.addWidget(pg.Qt.QtWidgets.QLabel("Pumping:"))
        self.pump_label = pg.Qt.QtWidgets.QLabel("")
        btn_layout.addWidget(self.pump_label)
        main_layout.addItem(btn_layout)
        # Setup control widget (row of widgets)
        ctl_layout = pg.Qt.QtWidgets.QHBoxLayout()
        ctl_layout.addWidget(pg.Qt.QtWidgets.QLabel("Min Pressure:"))
        ctl_layout.addSpacing(5)
        self.minp_lineedit = pg.Qt.QtWidgets.QLineEdit()
        _, sp = self.client.get_min_pressure()
        self.minp_lineedit.setText("%g" % sp)
        self.minp_lineedit.returnPressed.connect(self._set_min_pressure)
        ctl_layout.addWidget(self.minp_lineedit)
        ctl_layout.addSpacing(20)
        ctl_layout.addWidget(pg.Qt.QtWidgets.QLabel("Max Pressure:"))
        ctl_layout.addSpacing(5)
        self.maxp_lineedit = pg.Qt.QtWidgets.QLineEdit()
        _, sp = self.client.get_max_pressure()
        self.maxp_lineedit.setText("%g" % sp)
        self.maxp_lineedit.returnPressed.connect(self._set_max_pressure)
        ctl_layout.addWidget(self.maxp_lineedit)
        ctl_layout.addSpacing(20)
        self.ce_btn = pg.Qt.QtWidgets.QPushButton("Clear Error")
        self.ce_btn.clicked.connect(self._clear_error)
        ctl_layout.addWidget(self.ce_btn)
        main_layout.addItem(ctl_layout)
        ctl_layout = pg.Qt.QtWidgets.QHBoxLayout()
        self.pumping_btn = pg.Qt.QtWidgets.QPushButton("Start Pump")
        self.pumping_btn.clicked.connect(self._toggle_pump)
        ctl_layout.addWidget(self.pumping_btn)
        ctl_layout.addSpacing(20)
        self.set_flow_setpoint_btn = pg.Qt.QtWidgets.QPushButton("Flow setpoint:")
        self.set_flow_setpoint_btn.clicked.connect(self._set_flow)
        ctl_layout.addWidget(self.set_flow_setpoint_btn)
        ctl_layout.addSpacing(20)
        self.flow_setpoint_lineedit = pg.Qt.QtWidgets.QLineEdit()
        _, sp = self.client.get_flow_setpoint()
        self.flow_setpoint_lineedit.setText("%g" % sp)
        self.flow_setpoint_lineedit.returnPressed.connect(self._set_flow)
        ctl_layout.addWidget(self.flow_setpoint_lineedit)
        main_layout.addItem(ctl_layout)
        # Setup stop flow widget
        ctl_layout = pg.Qt.QtWidgets.QHBoxLayout()
        self.stop_flow_btn = pg.Qt.QtWidgets.QPushButton("STOP FLOW NOW!")
        self.stop_flow_btn.clicked.connect(self._stop_pump)
        ctl_layout.addWidget(self.stop_flow_btn)
        main_layout.addItem(ctl_layout)
        # Timers for auto-updating things
        buffer_update_timer = pg.QtCore.QTimer()
        buffer_update_timer.timeout.connect(self._update_buffer)
        plot_update_timer = pg.QtCore.QTimer()
        plot_update_timer.timeout.connect(self._update_plot)
        self.setLayout(main_layout)
        self.layout = main_layout
        self.buffer_update_timer = buffer_update_timer
        self.plot_update_timer = plot_update_timer
        self.curve_pressure = curve_pressure
        # self.curve_setpoint = curve_setpoint
        self.plot = plot

    def start(self, app_exec=False):
        r"""Start the GUI, make it visible."""
        self._dbgmsg("GUI: Starting application.")
        self.show()
        self.buffer_update_timer.start(int(self.buffer_update_interval * 1e3))
        self.plot_update_timer.start(int(self.plot_update_interval * 1e3))
        if app_exec:
            self.app.exec_()

    def _set_flow(self):
        try:
            self._dbgmsg("GUI: setting flow rate...")
            flow = float(self.flow_setpoint_lineedit.text())
            self._dbgmsg("GUI: set flow rate to", flow)
        except:
            self._dbgmsg("Flow setpoint is not a float value.")
            return
        self.client.set_flow(flow)
        # self.client.start_pump()

    def _set_min_pressure(self):
        try:
            self._dbgmsg("GUI: setting min pressure...")
            flow = float(self.minp_lineedit.text())
            self._dbgmsg("GUI: set min pressure to", flow)
        except:
            self._dbgmsg("Min pressure is not a float value.")
            return
        self.client.set_min_pressure(flow)
        # self.client.start_pump()

    def _set_max_pressure(self):
        try:
            self._dbgmsg("GUI: setting max pressure...")
            flow = float(self.maxp_lineedit.text())
            self._dbgmsg("GUI: set max pressure to", flow)
        except:
            self._dbgmsg("Max pressure is not a float value.")
            return
        self.client.set_max_pressure(flow)

    def _clear_error(self):
        self.client.clear_error()

    def _stop_pump(self):
        self.client.stop_pump()

    def _start_pump(self):
        self.client.start_pump()

    def _toggle_pump(self):
        if self.client.is_pumping():
            self._stop_pump()
        else:
            self._start_pump()

    def _update_buffer(self):
        r"""Fetch data and update the internal memory buffer."""
        i = self.buffer_idx % self.buffer_length
        t, setpoint = self.client.get(method="get_flow_setpoint")
        t, pressure = self.client.get(method="get_pressure")
        if setpoint is None or pressure is None:
            self._dbgmsg("GUI: Requested data is None.")
            return
        self.ring_buffer_time[i] = t
        self.ring_buffer_setpoint[i] = setpoint
        self.ring_buffer_pressure[i] = pressure
        self.buffer_idx += 1
        self.setpoint_label.setText("%3.2f ul/min" % setpoint)
        self.pressure_label.setText("%3.2f PSI" % pressure)
        if self.client.is_pumping():
            self.pump_label.setText("YES")
            self.pumping_btn.setText("Stop Pump")
        else:
            self.pump_label.setText("NO")
            self.pumping_btn.setText("Start Pump")

    def _update_plot(self):
        r"""Update the plot."""
        if self.buffer_idx < 10:
            return
        t = self.ring_buffer_time
        s = self.ring_buffer_setpoint
        p = self.ring_buffer_pressure
        if (
            self.buffer_idx < self.buffer_length
        ):  # Don't show garbage data values, or zeros.
            t = t[: self.buffer_idx]
            s = s[: self.buffer_idx]
            p = p[: self.buffer_idx]
        self.curve_pressure.setData(t - np.max(t), p * 0.01)
        # self.curve_setpoint.setData(t - np.max(t), s)
        if not self.is_initialized:
            self.plot.enableAutoRange(
                "x", False
            )  # Stop auto-scaling after the first data set is plotted.
            self.plot.setXRange(-60, 0)
            self.plot.setLabel("bottom", "Relative Time (s)")
            self.is_initialized = True

    def quit(self):
        if self.client.server is not None:
            self.client.server.stop()
