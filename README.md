# hardware_interfaces

Various pieces of code that help control hardware with software.

See the documentation here: https://kirianlab.gitlab.io/hardware_interfaces/index.html
