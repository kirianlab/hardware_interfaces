#!/bin/bash

[ -d build ] && rm -r build
[ -d source/api ] && rm -r source/api
[ -d source/auto_examples ] && rm -r source/auto_examples
sphinx-apidoc --output-dir source/api --module-first ../hardware
# Fix the stupid default title of API page
tail -n+3 source/api/modules.rst > tmp.rst
echo 'API Reference' > source/api/modules.rst
echo '=============' >> source/api/modules.rst
cat tmp.rst >> source/api/modules.rst
rm tmp.rst &> /dev/null
make clean
# make doctest
make html
perl -p -i -e 's{<head>\n}{<head>\n  <meta name="robots" content="noindex, nofollow" />\n}' build/html/*.html
