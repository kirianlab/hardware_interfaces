import sys
sys.path.append('../../hardware')
from PyQt5.QtWidgets import QApplication, QWidget, QGridLayout, QMainWindow, QPushButton, QLineEdit
from hardware.mdrive import MDrive, aerosol_injector_x_serial, aerosol_injector_y_serial


class MDriveWidget(QMainWindow):
    def __init__(self):
        super().__init__()
        self.hbox = QGridLayout()
        self.main_widget = QWidget()
        self.main_widget.setLayout(self.hbox)
        self.setCentralWidget(self.main_widget)
        self.show()
        
        self.ym_button = QPushButton('-y')
        self.ym_button.clicked.connect(self.ymm)
        self.hbox.addWidget(self.ym_button, 2, 1)
        self.ydist_edit = QLineEdit()
        self.ydist_edit.setText('1')
        self.hbox.addWidget(self.ydist_edit, 2, 2)
        self.yp_button = QPushButton('+y')
        self.yp_button.clicked.connect(self.ymp)
        self.hbox.addWidget(self.yp_button, 2, 3)
        self.ym =  MDrive(serial_number=aerosol_injector_y_serial)
        
        self.xm_button = QPushButton('-x')
        self.xm_button.clicked.connect(self.xmm)
        self.hbox.addWidget(self.xm_button, 1, 1)
        self.xdist_edit = QLineEdit()
        self.xdist_edit.setText('1')
        self.hbox.addWidget(self.xdist_edit, 1, 2)
        self.xp_button = QPushButton('+x')
        self.xp_button.clicked.connect(self.xmp)
        self.hbox.addWidget(self.xp_button, 1, 3)
        self.xm =  MDrive(serial_number=aerosol_injector_x_serial)

    def xmm(self):
        dist = float(self.xdist_edit.text())
        self.xm.translate_mm(mm=-dist)
    def xmp(self):
        dist = float(self.xdist_edit.text())
        self.xm.translate_mm(mm=+dist)
    def ymm(self):
        dist = float(self.ydist_edit.text())
        self.ym.translate_mm(mm=-dist)
    def ymp(self):
        dist = float(self.ydist_edit.text())
        self.ym.translate_mm(mm=+dist)



app = QApplication(sys.argv)


w = MDriveWidget()

app.exec_()


#ym = MDrive(serial_number=aerosol_injector_y_serial)
#ym.translate_mm(mm=1, max_move=3)
#xm = MDrive(serial_number=aerosol_injector_x_serial)

