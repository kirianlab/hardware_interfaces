Need to write an example script for general testing station usage

1) Talk to sensors & HPLC/HA pump
2) Talk to Photron camera
3) Run live analysis in background

General steps:
1) change gas/liq flow rates
2) record a movie (one data point)
3) update the live analysis
4) repeat from step 1 with new flow/liq parameters