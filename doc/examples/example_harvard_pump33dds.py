from hardware.harvard import Pump33DDS


pump = Pump33DDS(serial_number='P100465')
pump.set_syringe_diameter(4.5, 1)  # mm
pump.set_syringe_volume(1000, 0)  # ul
pump.set_target_volume(100)  # ul
pump.set_infuse_rate(20, 0)  # ul/min
pump.start_infuse('p1')
print('Infusing')
