import sys, time
from hardware import sensirion
config = sensirion.make_config(zmq_port=5555)
config['serial_number'] = 'FTSTT5NA'  # SLI-0430, microjet test station high-flow
# config['serial_number'] = 'FTXEF0AHA'  # SLG64_0075, microjet test station low-flow
if 1:  # Make a GUI.  Plots flow rate, starts a client and server.  This Qt Widget can be embedded in other widgets.
    widget = sensirion.SensirionWidget(config=config)
    widget.start()
    sys.exit()
if 1:  # Make a client and get some flow readings.  It will start a server if one is not already running.
    client = sensirion.SensirionClient(config=config, start_server=True)
    time.sleep(3)
    for i in range(10):
        print(client.get_flow_rate())
    client.server.stop()
    sys.exit()
if 1:  # Make a device instance and communicate directly.  Most likely you should not do this.  Use a client instead.
    device = sensirion.SensirionDevice(config=config, debug=0)
    time.sleep(3)
    for i in range(10):
        print(device.get_flow_rate())
    sys.exit()
