# Steps for calibrating liquid flow:
# 1) Get a container to capture water
# 2) Weigh the empty container
# 3) Connect liquid line from pump, through flow meters, and into container
# 4) Ensure no evaporation is possible
# 5) Run this script to capture liquid in container and record flow
# 6) Weigh the water.  The density is 1 g/cm^3.

from time import time
import numpy as np
from hardware import sensirion, harvard
from queue import Queue
from threading import Thread

def myprint(*args, **kwargs):
    print(*args, **kwargs, flush=True)


myprint('Establishing connection to SLG64...')
t = time()
slg = sensirion.Sensirion(serial_number='FTXEF0AHA')
myprint('Done (%0.3g seconds)' % (time()-t))

myprint('Establishing connection to SLI...')
t = time()
sli = sensirion.Sensirion(serial_number='FTSTT5NA')
myprint('Done (%0.3g seconds)' % (time()-t))

myprint('Establishing connection to harvard...')
t = time()
pump = harvard.Pump(serial_number='P100465')
myprint('Done (%0.3g seconds)' % (time()-t))


class GetFlow(Thread):

    flow = None

    def __init__(self, queue, flowmeter):
        Thread.__init__(self)
        self.queue = queue
        self.flowmeter = flowmeter

    def run(self):
        self.flow = self.flowmeter.reading
        self.queue.task_done()


queue = Queue()
n = 10
slg_readings = np.zeros(n)
sli_readings = np.zeros(n)
myprint('Gather %d readings...' % (n,))
worker = GetFlow(queue, slg)
# worker.daemon = True
t = time()
for i in range(n):
    if True:
        worker.start()
        sli_reading = sli.reading
        queue.join()
        slg_reading = worker.flow
    else:
        slg_reading = slg.reading
        sli_reading = sli.reading
    print(slg_reading, sli_reading, 'ul/min', flush=True)

myprint('Done (%0.3g seconds/reading).' % ((time()-t)/n))
