from hardware import mdrive

x_axis = mdrive.MDrive(serial_number=mdrive.aerosol_injector_x_serial)
y_axis = mdrive.MDrive(serial_number=mdrive.aerosol_injector_y_serial)

y_axis.set_acceleration('slow')
print(x_axis.get_velocity())
