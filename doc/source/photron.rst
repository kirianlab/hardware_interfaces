Photron FastCam SA5 Notes
=========================

In order to use the Photron camera interface you must first download the
photron software development kit (SDK).  The latest version should be available
at the Photron website (possibly `here <https://photron.com/photron-support>`_ ).

Once files are downloaded, copy the directory named "SDK" into the directory
`hardware/photron`.

Next you need to build the CyFastcam module using the script `hardware/photron/`.
