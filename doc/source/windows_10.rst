Working with Windows 10
=======================

Symbolic Links
--------------

Symbolic links annoyingly do not work with Windows 10 git bash in the normal way. To add a symbolic link, see below.


To make a symbolic link from the ``../hardware_interfaces/utils`` (U) folder to the ``../hardware_intefaces/sensirion/``
(S) folder, navigate to (S) and run this:

``MSYS=winsymlinks:nativestrict ln -s ../utils/ .``

Note that this only works with relative paths. You need to be in the directory you'd like the symbolic link to live and
choose the relative path from there. Otherwise, Windows will throw a fit. It is unknown how well this translates to
other systems via git.
