Hardware Interfaces Home
========================

You've found the doc's for the Kirian Lab Hardware Interfaces package.

The source code is found at the GitLab repository: https://gitlab.com/kirianlab/hardware_interfaces

Quick Start
-----------

You can clone the repository as follows:

.. code-block:: bash

    git clone git@gitlab.com:kirianlab/hardware_interfaces.git

Make sure the repository is in your python path:

.. code-block:: bash

    export PYTHONPATH=/path/to/hardware_interfaces:$PYTHONPATH

Read a liquid flow rate:

.. code-block:: python

    from hardware import sensirion
    liquid_sensors = sensirion.get_sensors()
    liquid = liquid_sensors[0]
    print(liquid.reading)

Acknowledgements
----------------

The hardware package is developed by the `Kirian Lab <https://www.physics.asu.edu/content/richard-kirian>`_
with contributions from Roberto Alvarez, Kosta Karpos, Jude Tombo, Sahba Zaare, and Rick Kirian.
It's development is supported by National Science Foundation awards
`1231306 <https://www.nsf.gov/awardsearch/showAward?AWD_ID=1231306>`__,
`1943448 <https://www.nsf.gov/awardsearch/showAward?AWD_ID=1943448>`__,
and `1565180 <https://www.nsf.gov/awardsearch/showAward?AWD_ID=1565180>`__.

Contents
--------

.. toctree::
   :maxdepth: 1

   installation
   hardware_notes
   windows_10
   developer
   api/modules

Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
