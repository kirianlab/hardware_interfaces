Bronkhorst Gas Flow Sensors/Controllers
=======================================

Communicating with the Bronkhorst Mass Flow Sensors
---------------------------------------------------

Devices
-------

Current devices on the testing station:

- EL-Flow ???

The serial information for these devices will be located in the config file (coming soon).


Usage
-----

Open iPython and run the following ``%run bronkhorst.py``. This will initialize the ``Bronkhorst`` class under the
variable name ``bronk``.



Basic usage examples are as follows:

Example 1: Reading a Flow Rate
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    In [1]: bronk.reading
    Out [1]: 23.52


Example 2: Determine the Units
------------------------------

.. code-block::

    In [1]: bronk.units
    Out [1]: 'mg/min'


Example 3: Get Sensor Range Information
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    In [1]: bronk.sensor_range
    Out [1]: [0,100]

Please see the source code for more of the functionality.

General Questions to Answer
---------------------------

1. What's going on with the serial number?!?

Notes
-----

**NOTE THAT IS A MAJOR HACK HERE. IT NEEDS TO BE ADDRESSED BEFORE ADDING MORE BRONKHORST DEVICES TO THE STATION. SEE
THE MAIN FUNCTION FOR THE DETAILS.**

Symbolic links annoyingly do not work with Windows 10 git bash in the normal way. To add a symbolic link, see below.

Example: 
To make a symbolic link from the `../hardware_interfaces/utils` (U) folder to the `../hardware_intefaces/sensirion/` (S) folder, navigate to (S) and run this:

`MSYS=winsymlinks:nativestrict ln -s ../utils/ .`

Note that this only works with relative paths. You need to be in the directory you'd like the symbolic link to live and choose the relative path from there. Otherwise, Windows will throw a fit. It is unknown how well this translates to other systems via git.
