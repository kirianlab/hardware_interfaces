Installation Notes
==================

So far, there is nothing to "install".  However, note that we will not include Software Development Kits (SDKs) for
hardware such as the Photron fastcam, in which case we need to document those installation procedures here.
