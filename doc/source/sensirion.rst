Sensirion Liquid Flow Sensors
=============================

Devices
-------

Current devices on the testing station:

- SLG64-075
- SLI-0430

The serial information for these devices will be located in the config file (coming soon).


Usage
-----

Open iPython and run the following `%run sensirion.py`. This will initialize the `Sensirion` class under the variable name `sens`.



Basic usage examples are as follows:

Example 0: Create a Flowmeter Instance
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: python

    sens = Senserion(serial_number='FTSTT5NA')


Example 1: Reading a Flow Rate
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    In [1]: sens.reading
    Out [1]: 23.52


Example 2: Determine the Units
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    In [1]: sens.units
    Out [1]: 'microliter/minute'


Example 3: Get Sensor Range Information
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    In [1]: sens.sensor_range
    Out [1]:
    Sensirion Calibration 0
    {'lo': 0,
     'hi': 80,
     'units': 'microliter/minute',
     'calib': 0,
     'name': 'SLI-0430',
     'sn': 170700101}


General Questions to Answer
---------------------------

1. `sensirion.get("sensor_serial_num")[0]` shows that the serial number of the device is `170700101` for the `SLI-0430`,
while Windows says it's `"FTSTT5NA"`. What's the difference? Where does the `170700101` come from?

Notes
-----

Symbolic links annoyingly do not work with Windows 10 git bash in the normal way. To add a symbolic link, see below.

Example: 
To make a symbolic link from the `../hardware_interfaces/utils` (U) folder to the `../hardware_intefaces/sensirion/` (S) folder, navigate to (S) and run this:

`MSYS=winsymlinks:nativestrict ln -s ../utils/ .`

Note that this only works with relative paths. You need to be in the directory you'd like the symbolic link to live and choose the relative path from there. Otherwise, Windows will throw a fit. It is unknown how well this translates to other systems via git.
