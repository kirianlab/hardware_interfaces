#import sys
#sys.path.append("/home/labuser/hplc/hardware_interfaces/hardware")
from hardware import shimadzu
config = shimadzu.make_config(dummy=False)
#config['log_file_path'] = "C:\\Users\\uwelab\\projects\\data"
config['log_file_path'] = "/home/labuser/hplc/data"
config['polling_frequency'] = 10
print(config)
widget = shimadzu.HPLCWidget(config=config)
widget.start(app_exec=True)

