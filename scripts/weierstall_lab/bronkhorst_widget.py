from hardware import bronkhorst as bronkhorst
com_port = 'COM3'
config = bronkhorst.make_config(dummy=False)
config['com_port'] = com_port
config['log_file_path'] = "C:\\Users\\uwelab\\projects\\data"
widget = bronkhorst.BronkhorstWidget(config=config)
widget.start(app_exec=True)