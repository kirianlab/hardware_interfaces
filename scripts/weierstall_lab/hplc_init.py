"""
Goal: use Shimadzu for LCP in Weierstall Lab

ToDo ------------------------------------------------
- units
    - flow rate: uL/min (default) --> mL/min
    - pressrue: correct scale
    - properly set flow rate and pressure units in parent class
    - propagate flow/pressure units to GUI and log
- operation
    - set MAX pressure, not flowrate
    - plot pressure (correct scale)
    - log only if `is_pumping` (avoid non-pumping 0s)
    - SET pressure (not flowrate) from terminal & GUI
"""

PRESSURE_UNITS = {
    1 : 'PSI',
}

UNIT_SCALE = dict(  ## defines scale factors that convert the default 'factory' units to a new desired unit
    flowrate = dict( code=None, unit='mL/min', scale=1.0),
    pressure = dict( code=1   , unit='PSI'   , scale=1.0)
)

import sys; sys.path.append('C:\\Users\\uwelab\\projects\\hardware_interfaces\\')
import time, numpy as np, pyqtgraph as pg
from hardware import shimadzu
from hardware.shimadzu import HPLCDevice, HPLCWidget, HPLCClient, time_tools

def print_dict(d, title=""):
    if title: print(f"\n{title} {'-'*30}\n")
    for k,v in (d.items()): print(f'{k:>30} : {v}')
    print()

def print_hplc_status(hplc):
    """Prints setpoint and current value for pressure and flow rate"""
    a = hplc.get_status_dict()
    a['pumping'] = hplc.is_pumping()
    d = {k : a[k] for k in \
            ['pumping','pressure', 'max_pressure', 'min_pressure', 'pressure_units', 'timestamp_iso', 'flow_rate', 'flow_rate_units', 'event']}
    print_dict(d, "Status: Pressure & Flow Rate")

## CONFIG #####################################################

config = shimadzu.make_config(dummy=False)
config['log_file_path'] = "C:\\Users\\uwelab\\projects\\data"
config['polling_frequency'] = 1
config['logging'] = False

print_dict({k : config[k] for k in ['logging','log_file_path', 'polling_frequency']}, "config")

## Client / Server  ###########################################

class HPLCClientForLCP( HPLCClient ):
    r"""Overloads some methods in `HPLCClient`
        (mainly to set/get max pressure) for LCP applications.
    """

    def __init__(self, config, **kwargs):
        config['device_class'] = __class__
        super().__init__(config=config, **kwargs)
    def get_flow(self): return self.get(method="get_flow")
    # def get_flow_rate(self): return self.get(method="get_flow")
    # def get_flow_setpoint(self): return self.get(method="get_flow")

    def get_max_pressure_setpoint(self): return self.get(method="get_max_pressure_setpoint")
    def get_min_pressure_setpoint(self): return self.get(method="get_min_pressure_setpoint")

    def set_max_pressure_setpoint(self): return self.get(method="set_max_pressure_setpoint")
    def set_min_pressure_setpoint(self): return self.get(method="set_min_pressure_setpoint")


## GUI  #######################################################

class HPLCWidgetForLCP( pg.Qt.QtWidgets.QWidget ):
    r"""Overloads some methods in `HPLCWidget`
        (mainly to set & plot max pressure) for LCP applications.
    """

    unit_pressure = UNIT_SCALE['pressure']['unit']
    unit_flowrate = UNIT_SCALE['flowrate']['unit']

    def __init__(self, **kwargs):

        self._dbgmsg("Initializing.")

        app = pg.mkQApp()
        app.aboutToQuit.connect(self.quit)
        self.app = app

        super().__init__()

        self.config = config
        self.buffer_idx = 0
        self.buffer_length = int(1e5)
        self.buffer_update_interval = 0.1
        self.plot_update_interval = 0.1

        self.ring_buffer_time     = np.zeros(self.buffer_length)
        self.ring_buffer_setpoint = np.zeros(self.buffer_length)
        self.ring_buffer_pressure = np.zeros(self.buffer_length)

        ## Only for LCP
        self.ring_buffer_max_pressure_setpoint = np.zeros(self.buffer_length)
        self.ring_buffer_min_pressure_setpoint = np.zeros(self.buffer_length)

        self.is_initialized = False
        self.is_paused = False
        self.do_update_plot = True

        # self.client = HPLCClient(config=config, start_server=True)
        self.client = HPLCClientForLCP(config=config, start_server=True)

        self._setup_interface()

    def _dbgmsg(self, *args, **kwargs): print("ShimadzuWidget:", *args, **kwargs)
    def start(self, app_exec=False):
        r"""Identical to that of original `HPLCWidget`"""
        self._dbgmsg("GUI: Starting application.")
        self.show()
        self.buffer_update_timer.start(int(self.buffer_update_interval * 1e3))
        self.plot_update_timer.start(int(self.plot_update_interval * 1e3))
        if app_exec:
            self.app.exec_()
    def _stop_pump(self):  self.client.stop_pump()
    def _start_pump(self): self.client.start_pump()
    def _toggle_pump(self):
        if self.client.is_pumping(): self._stop_pump()
        else: self._start_pump()
    def quit(self):
        if self.client.server is not None: self.client.server.stop()

    def _setup_interface(self):
        main_layout = pg.Qt.QtWidgets.QVBoxLayout()
        # Setup plot window
        plt_layout = (
            pg.Qt.QtWidgets.QHBoxLayout()
        )  # QGridLayout() for grid layout, or QSlider() for slider window thing
        plot = pg.PlotWidget()
        plot.addLegend()
        plot.setLabel("bottom", "Relative Time (s)")
        plot.setTitle("Shimadzu HPLC Pump")
        pg.setConfigOptions(antialias=True)

        # curve_pressure = self.plot.plot(name=f"Pressure (PSI) x 0.01", pen=None, symbol="o", symbolPen=None, symbolBrush="g")
        # curve_setpoint = plot.plot(name="Flow SP (ul/min)", pen="r")

        curve_pressure = plot.plot(name=f"Pressure ({self.unit_pressure})", pen=None, symbol="o", symbolPen=None, symbolBrush="g")
        curve_setpoint = plot.plot(name=f"Flow ({self.unit_flowrate})", pen="r")

        curve_min_pressure_setpoint = plot.plot(name=f"Min Pressure Setpoint ({self.unit_pressure})", pen="c")
        curve_max_pressure_setpoint = plot.plot(name=f"Max Pressure Setpoint ({self.unit_pressure})", pen="m")

        plt_layout.addWidget(plot)
        main_layout.addItem(plt_layout)

        # Setup device status widget (row of widgets)
        btn_layout = pg.Qt.QtWidgets.QHBoxLayout()

        # Flow setpoint status
        btn_layout.addWidget(pg.Qt.QtWidgets.QLabel("Flow:"))
        self.setpoint_label = pg.Qt.QtWidgets.QLabel("")    ## actually, this is the current flow rate.
        btn_layout.addWidget(self.setpoint_label)

        # Pressure status
        btn_layout.addWidget(pg.Qt.QtWidgets.QLabel("Pressure:"))
        self.pressure_label = pg.Qt.QtWidgets.QLabel("")
        btn_layout.addWidget(self.pressure_label)

        # Min/Max Pressure setpoint status (for LCP)
        btn_layout.addWidget(pg.Qt.QtWidgets.QLabel("Min Pressure Setpoint:"))
        self.min_pressure_setpoint_label = pg.Qt.QtWidgets.QLabel("")
        btn_layout.addWidget(self.min_pressure_setpoint_label)

        btn_layout.addWidget(pg.Qt.QtWidgets.QLabel("Max Pressure Setpoint:"))
        self.max_pressure_setpoint_label = pg.Qt.QtWidgets.QLabel("")
        btn_layout.addWidget(self.max_pressure_setpoint_label)

        main_layout.addItem(btn_layout)

        # Pump status
        btn_layout.addWidget(pg.Qt.QtWidgets.QLabel("Pumping?"))
        self.pump_label = pg.Qt.QtWidgets.QLabel("")
        btn_layout.addWidget(self.pump_label)

        main_layout.addItem(btn_layout)

        # Setup control widget (row of widgets)

        ## Start pump button
        ctl_layout = pg.Qt.QtWidgets.QHBoxLayout()
        self.pumping_btn = pg.Qt.QtWidgets.QPushButton("Start Pump")
        self.pumping_btn.clicked.connect(self._toggle_pump)
        ctl_layout.addWidget(self.pumping_btn)

        ctl_layout.addSpacing(20)

        ## Set flow setpoint button (Won't use for LCP!)
        # self.set_flow_setpoint_btn = pg.Qt.QtWidgets.QPushButton("Flow setpoint:")
        # self.set_flow_setpoint_btn.clicked.connect(self._set_flow)
        # ctl_layout.addWidget(self.set_flow_setpoint_btn)

        ## Set max pressure (instead of flow setpoint, for LCP only!)
        self.set_flow_setpoint_btn = None
        self.set_max_pressure_setpoint_btn = pg.Qt.QtWidgets.QPushButton("Max Pressure Setpoint")
        self.set_max_pressure_setpoint_btn.clicked.connect(self._set_max_pressure_setpoint)

        ctl_layout.addWidget(self.set_max_pressure_setpoint_btn)
        ctl_layout.addSpacing(20)

        ## flow setpoint entry box (Won't use for LCP!)
        # self.flow_setpoint_lineedit = pg.Qt.QtWidgets.QLineEdit()
        # _, sp = self.client.get_flow_setpoint()
        # self.flow_setpoint_lineedit.setText("%g" % sp)
        # self.flow_setpoint_lineedit.returnPressed.connect(self._set_max_pressure_setpoint)
        # ctl_layout.addWidget(self.flow_setpoint_lineedit)

        ## max pressure setpoint entry box (instead of flow setpoint entry box, only for LCP!)
        self.max_pressure_setpoint_lineedit = pg.Qt.QtWidgets.QLineEdit()
        val = self.client.get_max_pressure_setpoint()
        print(val)
        if val is not None: _, sp = val
        else: sp = 'None'
        self.max_pressure_setpoint_lineedit.setText(sp)
        self.max_pressure_setpoint_lineedit.returnPressed.connect(self._set_max_pressure_setpoint)
        ctl_layout.addWidget(self.max_pressure_setpoint_lineedit)

        main_layout.addItem(ctl_layout)

        # Setup stop flow widget
        ctl_layout = pg.Qt.QtWidgets.QHBoxLayout()
        self.stop_flow_btn = pg.Qt.QtWidgets.QPushButton("STOP FLOW NOW!")
        self.stop_flow_btn.clicked.connect(self._stop_pump)
        ctl_layout.addWidget(self.stop_flow_btn)
        main_layout.addItem(ctl_layout)

        # Timers for auto-updating things
        buffer_update_timer = pg.QtCore.QTimer()
        buffer_update_timer.timeout.connect(self._update_buffer)
        plot_update_timer = pg.QtCore.QTimer()
        plot_update_timer.timeout.connect(self._update_plot)

        self.setLayout(main_layout)
        self.layout = main_layout
        self.buffer_update_timer = buffer_update_timer
        self.plot_update_timer   = plot_update_timer

        self.curve_pressure = curve_pressure
        self.curve_setpoint = curve_setpoint

        self.curve_min_pressure_setpoint = curve_min_pressure_setpoint
        self.curve_max_pressure_setpoint = curve_max_pressure_setpoint

        self.plot = plot

    def _set_flow(self): self._dbgmsg("Request Ignored! We don't set flow rate setpoint for LCP!")

    def _set_max_pressure_setpoint(self):
        r""" NEW! Only for LCP. Mirrors the structure of `self._set_flow` in original parent HPLCWidget"""
        try:
            self._dbgmsg("GUI: setting max pressure setpoint ...")
            val = float(self.max_pressure_setpoint_lineedit.text())
            self._dbgmsg("GUI: set max pressure setpoint to", val)
        except:
            self._dbgmsg("Max pressure setpoint is not a float value.")
            return
        self.client.set_max_pressure_setpoint(val)
        # self.client.start_pump()

    def _update_buffer(self):
        r"""Fetch data and update the internal memory buffer."""
        i = self.buffer_idx % self.buffer_length

        t, setpoint = self.client.get(method="get_flow_setpoint")   ## actually, this is the current flow rate
        t, pressure = self.client.get(method="get_pressure")


        if setpoint is None or pressure is None:
            self._dbgmsg("GUI: Requested data (Flow rate setpoint or current pressure) is None.")
            return

        t, min_pressure_setpoint = self.client.get(method="get_min_pressure_setpoint")
        t, max_pressure_setpoint = self.client.get(method="get_max_pressure_setpoint")

        if min_pressure_setpoint is None or max_pressure_setpoint is None:
            self._dbgmsg("GUI: Requested data (min/max Pressure Setpoint or current flow rate) is None.")
            return

        self.ring_buffer_time[i] = t
        self.ring_buffer_setpoint[i] = setpoint   ## actually, this is the current flow rate
        self.ring_buffer_pressure[i] = pressure

        self.ring_buffer_min_pressure_setpoint[i] = min_pressure_setpoint
        self.ring_buffer_max_pressure_setpoint[i] = max_pressure_setpoint

        self.buffer_idx += 1

        ## update current values of pressure and flowrate
        self.setpoint_label.setText(f"{setpoint:3.2f} {self.unit_flowrate}")  ## actually, this is the current flow rate
        self.pressure_label.setText(f"{pressure:3.2f} {self.unit_pressure}")

        ## update min/max pressure setpoints (only for LCP)
        self.min_pressure_setpoint_label.setText(f"{min_pressure_setpoint:3.2f} {self.unit_pressure}")
        self.max_pressure_setpoint_label.setText(f"{max_pressure_setpoint:3.2f} {self.unit_pressure}")

        ## update 'pumping' status
        if self.client.is_pumping():
            self.pump_label.setText("YES")
            self.pumping_btn.setText("Stop Pump")
        else:
            self.pump_label.setText("NO")
            self.pumping_btn.setText("Start Pump")

    def _update_plot(self):
        r"""Update the plot."""
        idx = self.buffer_idx
        if idx < 10: return

        t = self.ring_buffer_time
        s = self.ring_buffer_setpoint     ## actually, this is the current flow rate
        p = self.ring_buffer_pressure

        p_min = self.ring_buffer_min_pressure_setpoint
        p_max = self.ring_buffer_max_pressure_setpoint

        if ( idx < self.buffer_length):  # Don't show garbage data values, or zeros.
            t = t[:idx]
            s = s[:idx]
            p = p[:idx]

            p_min = p_min[:idx]
            p_max = p_max[:idx]

        # self.curve_pressure.setData(t - np.max(t), p * 0.01)
        self.curve_pressure.setData(t - np.max(t), p)
        self.curve_setpoint.setData(t - np.max(t), s)

        self.curve_min_pressure_setpoint.setData(t - np.max(t), p_min)
        self.curve_max_pressure_setpoint.setData(t - np.max(t), p_max)

        if not self.is_initialized:
            self.plot.enableAutoRange(
                "x", False
            )  # Stop auto-scaling after the first data set is plotted.
            self.plot.setXRange(-60, 0)
            self.plot.setLabel("bottom", "Relative Time (s)")
            self.is_initialized = True

## HPLC #######################################################

class HPLCDeviceForLCP( HPLCDevice ):
    r"""Overloads some methods in `HPLCDevice`
        (mainly to change units) for LCP applications.
    """
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.set('pressure_unit',UNIT_SCALE['pressure']['code'])

    def get_status_dict(self):
        r"""Return dictionary with the status of all important aspects of the device."""
        a = self.get_all()

        ## Pressure ---------------------------------

        _param = UNIT_SCALE['pressure']
        a["pressure_unit"]  = _param['code']
        a["pressure_units"] = _param['unit']  # same: PRESSURE_UNITS[self.get('pressure_unit')]
        a["pressure"]       = _param['scale'] * a["pressure"]
        a["max_pressure"]   = _param['scale'] * a["max_pressure"]
        a["min_pressure"]   = _param['scale'] * a["min_pressure"]

        ## Flow Rate --------------------------------

        _param = UNIT_SCALE['flowrate']
        a["flow"]            = _param['scale'] * a["flow"]
        a["flow_rate"]       = a["flow"]
        a["flow_rate_units"] = _param['unit']

        a = time_tools.append_timestamps_to_dict(a)
        a["name"] = self.name

        return a

    def set_flow_rate(self, flow):
        r"""Set volumetric liquid flow rate in mL/min."""
        self.set("flow", flow)

    def get_flow(self): self.get_flow_setpoint()

    def get_flow_rate(self): self.get_flow_setpoint()

    def get_flow_setpoint(self):
        r"""Get the flow rate setpoint"""
        a = self.get_status_dict()
        # return a['timestamp_iso'], a['flow_rate'], a['flow_rate_units']
        return time.time(), a["flow"]   # TODO

    def set_max_pressure_setpoint(self, val):
        while self.get('max_pressure') != val:
            print(f"! Attempting to set max pressure to {val}. Currently: {self.get('max_pressure')}")
            self.set('max_pressure', val)
            time.sleep(0.1)

    def set_min_pressure_setpoint(self, val):
        while self.get('min_pressure') != val:
            print(f"! Attempting to set min pressure to {val}. Currently: {self.get('min_pressure')}")
            self.set('min_pressure', val)
            time.sleep(0.1)

    def get_max_pressure_setpoint(self): return time.time(), self.get('max_pressure') # self.get_status_dict()["max_pressure"]

    def get_min_pressure_setpoint(self): return time.time(), self.get('min_pressure') # self.get_status_dict()["min_pressure"]


def no_gui():
    hplc = HPLCDeviceForLCP(config=config)

    hplc.set_max_pressure_setpoint(234)
    hplc.set_min_pressure_setpoint(15)
    hplc.set_flow_rate(0.5)

    hplc.set('clear_error',1)  # TODO may be useful for beamtimes.
    hplc.start_pump()

    print_hplc_status(hplc)
    hplc.stop_buffering()
    print_hplc_status(hplc)

    # hplc.stop_pump()
    hplc.disconnect()

    del hplc
    return

def with_gui():
    widget = HPLCWidgetForLCP(config=config)
    widget.start(app_exec=True)


if __name__ == "__main__":
    # no_gui()
    with_gui()
    pass