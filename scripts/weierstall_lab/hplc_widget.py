from hardware import shimadzu
from hplc_init import HPLCWidgetForLCP
config = shimadzu.make_config(dummy=False)
config['log_file_path'] = "C:\\Users\\uwelab\\projects\\data"
config['polling_frequency'] = 10
config['debug'] = 1
print(config)
# widget = shimadzu.HPLCWidget(config=config)
widget = HPLCWidgetForLCP(config=config)
widget.start(app_exec=True)

