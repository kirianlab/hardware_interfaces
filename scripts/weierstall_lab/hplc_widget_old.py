from hardware import shimadzu
config = shimadzu.make_config(dummy=False)
config['log_file_path'] = "C:\\Users\\uwelab\\projects\\data"
widget = shimadzu.HPLCWidget(config=config)
widget.start(app_exec=True)