from hardware import sensirion
config = sensirion.make_config(dummy=False)
config['serial_number'] = '2222-00369'
config['log_file_path'] = "C:\\Users\\uwelab\\projects\\data"
widget = sensirion.SensirionWidget(config=config)
widget.start(app_exec=True)