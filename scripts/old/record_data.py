import os
import time
from hardware import bronkhorst
# from hardware import sensirion
from hardware import shimadzu
import numpy as np

# print('Setup Sensirion liquid flowmeter')
# sensirion_config = sensirion.make_config()
# sensirion_client = sensirion.SensirionClient(config=sensirion_config)

print('Setup Bronkhorst gas flow controller')
bronkhorst_config = bronkhorst.make_config()  # Assuming defaults are reasonable...
bronkhorst_client = bronkhorst.BronkhorstClient(config=bronkhorst_config)
def set_gas(flow):
    bronkhorst_client.set_flow_rate(flow)

print('Setup Shimadzu HPLC liquid pump')
shimadzu_config = shimadzu.make_config()
shimadzu_client = shimadzu.HPLCClient(config=shimadzu_config)
def set_liquid(flow):  # This is ul/min
    shimadzu_client.set_flow(flow)
def stop_pump():
    shimadzu_client.stop_pump()
def start_pump():
    shimadzu_client.start_pump()

print('Setup Odysseus record function')
record_file = 'D:/data/microjets/logs/odysseus/record_now.msg'
os.makedirs(os.path.dirname(record_file), exist_ok=True)
def record():
    f = open(record_file, 'w')
    f.close()
    while os.path.isfile(record_file):
        time.sleep(0.2)


def record_range(liquid_flows, gas_flows, equilibrate_time:int=30, sweep_gas=True):
    print("\n\n Starting parameter sweep:", flush=True)
    count = 0
    for l in liquid_flows:
        # set the liquid flow rate
        set_liquid(l)
        print(f'Equilibrating liquid... waiting {equilibrate_time} seconds', flush=True)
        time.sleep(equilibrate_time)
        for g in gas_flows:
            # set the gas flow rate
            set_gas(g)
            time.sleep(20)  # give gas a few seconds to adjust
            print('liquid =', l, ', gas = ', g, flush=True)
            print('recording video...', flush=True)
            record()
    print(f"\n\n record_range completed for: \n\t liquid_flows: {liquid_flows} \n\t gas_flows: {gas_flows}")