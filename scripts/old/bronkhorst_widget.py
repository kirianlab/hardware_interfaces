from hardware import bronkhorst
com_port = 'COM3'
config = bronkhorst.make_config(dummy=False)
config['com_port'] = com_port
widget = bronkhorst.BronkhorstWidget(config=config)
widget.start(app_exec=True)