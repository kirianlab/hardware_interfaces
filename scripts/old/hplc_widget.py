from hardware import shimadzu
config = shimadzu.make_config(dummy=False)
widget = shimadzu.HPLCWidget(config=config)
widget.start(app_exec=True)