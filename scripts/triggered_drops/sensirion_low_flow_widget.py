from hardware import sensirion
config = sensirion.make_config(zmq_port=5565, dummy=False)
config['log_file_path'] = 'C:/Users/raklab/data/triggered_drops/logs'
config['serial_number'] = 'FT772308A'
widget = sensirion.SensirionWidget(config=config)
widget.start(app_exec=True)
