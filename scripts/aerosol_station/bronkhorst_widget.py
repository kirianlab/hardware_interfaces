from hardware import bronkhorst as bronkhorst
com_port = 'COM8'
config = bronkhorst.make_config(dummy=False)
config['com_port'] = com_port
config['log_file_path'] = r'C:/Users/raklab/Desktop/aerosol/logs'
widget = bronkhorst.BronkhorstWidget(config=config)
widget.start(app_exec=True)