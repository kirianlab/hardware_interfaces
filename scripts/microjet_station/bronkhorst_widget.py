import sys
from hardware import bronkhorst
if len(sys.argv) > 1:
    com_port = sys.argv[1]
else:
    com_port = 'COM4'
config = bronkhorst.make_config(dummy=False)
config['com_port'] = com_port
widget = bronkhorst.BronkhorstWidget(config=config)
widget.start(app_exec=True)
