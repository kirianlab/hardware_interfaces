from hardware import sensirion
config = sensirion.make_config(zmq_port=5565, dummy=False)
config['serial_number'] = 'FTXEF0AHA'
widget = sensirion.SensirionWidget(config=config)
widget.start(app_exec=True)