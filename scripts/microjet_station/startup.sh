#!/bin/bash
eval "$('/c/Users/raklab/miniconda3/Scripts/conda.exe' 'shell.bash' 'hook')"
conda activate hardware_interfaces
export PYTHONPATH='/d/software/odysseus/:/d/software/odysseus/submodules/hardware_interfaces'
#export PATH=$PATH:/d/software/odysseus/submodules/hardware_interfaces/hardware
which python
#python ../identify_devices.py
python bronkhorst_widget.py   &   # Find port
python hplc_widget.py &
python sensirion_low_flow_widget.py &
python sensirion_high_flow_widget.py &
