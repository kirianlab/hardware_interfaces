from hardware import sensirion
config = sensirion.make_config(dummy=False)
config['serial_number'] = 'FTSTT5NA'
widget = sensirion.SensirionWidget(config=config)
widget.start(app_exec=True)