import sys
sys.path.append('..')
import pandas as pd
pd.options.display.max_columns = 999
pd.options.display.max_rows = 999
from hardware.utils.serial_communication_tools import identify_devices
print(identify_devices())
